import { CapacitorConfig } from "@capacitor/cli";

const config: CapacitorConfig = {
    appId: "com.example.app",
    appName: "HomeLab",
    webDir: "out", // build || out || .next
    bundledWebRuntime: false,
    // server: {
    //     // url: "http://192.168.x.xxx:3000",
    //     url: "http://localhost:3000",
    //     url: "10.0.2.2:8000", // https://stackoverflow.com/questions/55785581/socketexception-os-error-connection-refused-errno-111-in-flutter-using-djan
    //     cleartext: true,
    // },
};

export default config;
