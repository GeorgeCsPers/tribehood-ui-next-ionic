/** @type {import('next-sitemap').IConfig} */
// https://www.techomoro.com/generate-sitemap-for-static-and-dynamic-pages-in-a-next-js-app/
const siteUrl = process.env.NEXT_PUBLIC_URL || "https://tribehood.co.uk/";
module.exports = {
    siteUrl,
    exclude: ["/404"],
    generateRobotsTxt: true, // (optional)
    robotsTxtOptions: {
        policies: [
            {
                userAgent: "*",
                disallow: ["/404"],
            },
            {
                userAgent: "*",
                disallow: ["/users"],
            },
            {
                userAgent: "*",
                disallow: ["/users/my-account"],
            },
            {
                userAgent: "*",
                disallow: ["/users/my-profile"],
            },
            {
                userAgent: "*",
                disallow: ["/categories"],
            },
            { userAgent: "*", allow: "/" },
        ],
        additionalSitemaps: [
            `${siteUrl}sitemap.xml`,
            `${siteUrl}server-sitemap.xml`,
        ],
    },
};
