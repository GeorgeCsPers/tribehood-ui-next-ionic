Setup and Run Ionic iOS and Android

Inspiration: // https://www.youtube.com/watch?v=xQKtDgJXrlM

| npm i -D @capacitor/cli
| npm i @capacitor/core @capacitor/android @capacitor/ios @capacitor/preferences

| npx cap init

| Add to the package scripts "export": "next build && next export"
| npm run export

| edit capacitor.config, change webDir from "build" to "out"
| npx cap add ios
| npx cap add android
| npx cap open ios
| npx cap open android

| npx cap sync android
| npx cap sync ios

| npm i serve
| "start": "serve out"

PM2

pm2 start npm --name "HomeLab Client" -- run startStatic -- --port 20000 -- --no-autorestart
