import { Preferences } from "@capacitor/preferences";

class StorageService {
    setItem = async (key: string, value: string) => {
        await Preferences.set({
            key: key,
            value: value,
        });
    };

    getItem = async (key: string) => {
        const { value } = await Preferences.get({ key: key });
        return JSON.parse(value);
    };

    removeItem = async (key: string) => {
        await Preferences.remove({ key: key });
    };
}

export default new StorageService();
