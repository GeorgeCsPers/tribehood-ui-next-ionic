import axios from "axios";
import authHeader from "./auth-header";

const API_URL = process.env.NEXT_PUBLIC_API_URL;
// const API_URL = "https://univ-s1.goldilab.com/api/";

class UserService {
    getPublicContent() {
        return axios.get(API_URL + "all");
    }

    getUserBoard() {
        return axios.get(API_URL + "user", { headers: authHeader() });
    }

    getModeratorBoard() {
        return axios.get(API_URL + "mod", { headers: authHeader() });
    }

    getAdminBoard() {
        return axios.get(API_URL + "admin", { headers: authHeader() });
    }
}

export default new UserService();
