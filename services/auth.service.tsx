import axios from "axios";
import StorageService from "./storage.service";
import { isExpired } from "react-jwt";
import { getData } from "./data.service";

const API_URL = process.env.NEXT_PUBLIC_API_URL;
// const API_URL = "https://univ-s1.goldilab.com/api/";
// https://levelup.gitconnected.com/ionic-react-login-authentication-with-restful-api-79352883b9f3
// https://www.bezkoder.com/react-jwt-auth/
// https://github.com/bezkoder/react-jwt-auth/blob/master/src/App.js
// https://www.bezkoder.com/react-typescript-login-example/

class AuthService {
    async login(body: any) {
        return await axios
            .post(API_URL + "users/login", body)
            .then(async (res) => {
                if (await res.data.accessToken) {
                    // localStorage.setItem("user", JSON.stringify(response.data));
                    await StorageService.setItem(
                        "user",
                        JSON.stringify(await res.data)
                    );

                    await getData(`users/${res.data.id}`).then(
                        async (userDetails) => {
                            console.log("AuthService userDetails");
                            console.log(await userDetails);

                            await StorageService.setItem(
                                "userDetails",
                                JSON.stringify(await userDetails)
                            );
                        }
                    );
                    // window.location.reload();
                    return await res.data;
                }
            });
        // .catch(async (error) => {
        //     console.log("login error");
        //     console.log(error);
        // });
    }

    // async register(body: any) {
    //     return await axios
    //         .post(API_URL + "users/register", body)
    //         .then((response) => {
    //             if (response.data.accessToken) {
    //                 // localStorage.setItem("user", JSON.stringify(response.data));
    //                 StorageService.setItem(
    //                     "user",
    //                     JSON.stringify(response.data)
    //                 );
    //                 window.location.reload();
    //             }

    //             return response.data;
    //         });
    // }

    async logout() {
        console.log("logout");
        // localStorage.removeItem("user");
        await StorageService.removeItem("user");
        await StorageService.removeItem("userDetails");
        window.location.reload();
    }

    async register(body: any) {
        return await axios
            .post(API_URL + "users/register", body)
            .then((res) => {
                console.log(res);
            });
        // .catch(async (error) => {
        //     console.log("register error");
        //     console.log(error);
        // });
    }

    async getCurrentUser() {
        try {
            const user = await StorageService.getItem("userDetails");
            console.log("getCurrentUser");
            console.log(user);
            if ((await user) != null) {
                return user;
            }
            // return null;
        } catch (reason) {
            console.log(reason);
            return null;
        }
        // const user = await StorageService.getItem("userDetails");
        // console.log("getCurrentUser");
        // console.log(user);
        // return user;
    }

    async getCurrentToken() {
        const user = await StorageService.getItem("user");
        console.log("getCurrentToken");
        console.log(user);
        return user;
    }

    async checkRole() {
        const user = await StorageService.getItem("user");
        if (user && user?.accessToken) {
            return user.role[0];
        }
    }

    async checkToken() {
        let isAuth = false;
        const user = await StorageService.getItem("user");

        // console.log("checkToken");
        // console.log(user);
        // console.log(user?.accessToken);

        if (user && user?.accessToken) {
            const isExp = isExpired(user.accessToken);

            console.log("checkToken isExpired");
            console.log(isExp);

            if (!isExp) {
                isAuth = true;
            } else {
                isAuth = false;
                StorageService.removeItem("user");
                window.location.reload();
            }
        }
        // console.log(isAuth);
        return isAuth;
    }
}

export default new AuthService();
