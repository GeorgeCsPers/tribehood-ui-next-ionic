import axios from "axios";
import { getSession, signOut, useSession } from "next-auth/react";
import { getToken } from "next-auth/jwt";
import authService from "./auth.service";
// import { getStorageItem } from "./storage.service";
// import { hasToken, getActiveToken } from "./checkUser";
// import { useErrorBoundary } from "react-error-boundary";
// import SnackbarComp from "../components/common/snackbars";

const API_URL = process.env.NEXT_PUBLIC_API_URL;
// const API_URL = "https://univ-s1.goldilab.com/api/";

// export default async function getPublicData(
export const getPublicData = async (path: string, search = "", filter = "") => {
    const config = {
        headers: {
            "Cache-Control": "public, s-maxage=10, stale-while-revalidate=59",
            "Content-Type": "application/json",
        },
    };

    console.log("getPublicData path");
    console.log(API_URL + path + search + filter);

    let resData: any = [];
    // const { showBoundary } = useErrorBoundary();
    await axios
        .get(API_URL + path + search + filter, config)
        .then(async (res) => {
            console.log("getData");
            console.log(res.data);
            resData = await res.data;
        });
    // .catch(async (err) => {
    //     console.log("GET data error");
    //     console.log(err.response);
    //     console.log(err.message);
    //     console.log(err);

    //     resData = await err;
    //     // showBoundary("test error");

    //     // if (err.response.status == 401) {
    //     //     signOut();
    //     // }
    // });

    // if (resData.status == 401) {
    //     axios.post("/api/auth/logout", "");
    // } else {
    return resData;
    // }
};

export const getData = async (
    // user: any,
    path: string,
    search = "",
    filter = ""
) => {
    const token = await authService.getCurrentToken();
    const config = {
        headers: {
            "Cache-Control": "public, s-maxage=10, stale-while-revalidate=59",
            "Content-Type": "application/json",
            Authorization: `Bearer ${token?.accessToken}`,
        },
    };

    // console.log("GET path");
    // console.log(API_URL + path + search + filter);

    let resData: any = [];
    await axios
        .get(API_URL + path + search + filter, config)
        .then(async (res) => {
            console.log("getData");
            console.log(res.data);

            resData = await res.data;
        });
    // .catch(async (err) => {
    //     console.log("GET data error");
    //     console.log(err.response);
    //     console.log(err.message);
    //     console.log(err);

    //     resData = await err;

    //     // if (err.response.status == 401) {
    //     //     signOut();
    //     // }
    // });

    // if (resData.status == 401) {
    //     axios.post("/api/auth/logout", "");
    // } else {
    return resData;
    // }
};

export const getSingleData = async (path: string, id: any) => {
    const token = await authService.getCurrentToken();
    const config = {
        headers: {
            "Cache-Control": "public, s-maxage=10, stale-while-revalidate=59",
            "Content-Type": "application/json",
            Authorization: `Bearer ${token?.accessToken}`,
        },
    };

    console.log("GET single path");
    console.log(API_URL + path + "/" + id);

    let resData: any = [];
    await axios.get(API_URL + path + "/" + id, config).then(async (res) => {
        console.log("getSingleData");
        console.log(res.data);

        resData = await res.data;
    });
    // .catch(async (err) => {
    //     console.log("GET SINGLE data error");
    //     console.log(err.message);
    //     console.log(err);

    //     resData = await err;
    // });

    return resData;
};

export const postData = async (path: string, data: any) => {
    const token = await authService.getCurrentToken();
    const config = {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token?.accessToken}`,
        },
    };

    // console.log("POST path");
    // console.log(API_URL + path);
    // const { showBoundary } = useErrorBoundary();

    let resData: any = [];
    await axios.post(API_URL + path, data, config).then(async (res) => {
        console.log(res);
        console.log(res.data);

        resData = await res.data;
    });
    // .catch(async (err) => {
    //     console.log("POST data error");
    //     console.log(err.message);
    //     console.log(err);

    //     resData = await err;
    //     // showBoundary(err);
    //     // useSnackbar()
    //     //     // close modal
    // });

    return resData;
};

export const postDataFile = async (path: string, data: any) => {
    const token = await authService.getCurrentToken();
    const config = {
        headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token?.accessToken}`,
        },
    };

    // console.log("POST path");
    // console.log(API_URL + path);

    let resData: any = [];
    await axios.post(API_URL + path, data, config).then(async (res) => {
        console.log(res);
        console.log(res.data);

        resData = await res.data;
    });
    // .catch(async (err) => {
    //     console.log("POST data error");
    //     console.log(err.message);
    //     console.log(err);

    //     resData = await err;
    //     // useSnackbar()
    //     //     // close modal
    // });

    return resData;
};

export const putData = async (path: string, id: string, data: any) => {
    const token = await authService.getCurrentToken();
    const config = {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token?.accessToken}`,
        },
    };

    // console.log("PUT path");
    // console.log(API_URL + path + "?id=" + id);

    let resData: any = [];
    await axios
        .put(API_URL + path + "/" + id, data, config)
        .then(async (res) => {
            console.log(res);
            console.log(res.data);

            resData = await res.data;
        });
    // .catch(async (err) => {
    //     console.log("PUT data error");
    //     console.log(err.message);
    //     console.log(err);

    //     resData = await err;
    //     // useSnackbar()
    //     //     // close modal
    // });

    return resData;
};

export const putPublicData = async (path: string, id: string, data: any) => {
    const config = {
        headers: {
            "Content-Type": "application/json",
        },
    };

    // console.log("PUT path");
    // console.log(API_URL + path + "?id=" + id);

    let resData: any = [];
    await axios
        .put(API_URL + path + "/" + id, data, config)
        .then(async (res) => {
            console.log(res);
            console.log(res.data);

            resData = await res.data;
        });
    // .catch(async (err) => {
    //     console.log("PUT data error");
    //     console.log(err.message);
    //     console.log(err);

    //     resData = await err;
    //     // useSnackbar()
    //     //     // close modal
    // });

    return resData;
};

export const putDataFile = async (path: string, id: string, data: any) => {
    const token = await authService.getCurrentToken();
    const config = {
        headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token?.accessToken}`,
        },
    };

    // console.log("PUT path");
    // console.log(API_URL + path + "?id=" + id);

    let resData: any = [];
    await axios
        .put(API_URL + path + "/" + id, data, config)
        .then(async (res) => {
            console.log(res);
            console.log(res.data);

            resData = await res.data;
        });
    // .catch(async (err) => {
    //     console.log("PUT data error");
    //     console.log(err.message);
    //     console.log(err);

    //     resData = await err;
    //     // useSnackbar()
    //     //     // close modal
    // });

    return resData;
};

export const deleteData = async (path: string, id: any) => {
    const token = await authService.getCurrentToken();
    const config = {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token?.accessToken}`,
        },
    };

    // console.log("DELETE path");
    // console.log(API_URL + path + "?id=" + id);

    let resData: any = [];
    await axios.delete(API_URL + path + "/" + id, config).then(async (res) => {
        console.log("DELETE data ress");
        console.log(res);
        console.log(res.data);

        resData = await res.data;
    });
    // .catch(async (err) => {
    //     console.log("DELETE data error");
    //     console.log(err.message);
    //     console.log(err);

    //     resData = await err;

    // });

    return await resData;
};
