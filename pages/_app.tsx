import React, { useRef, useEffect, useState } from "react";
import Script from "next/script";
// import { SessionProvider, useSession } from "next-auth/react";
import { defineCustomElements as ionDefineCustomElements } from "@ionic/core/loader";

// Core CSS required for Ionic components to work properly
import "@ionic/core/css/core.css";

// Basic CSS for apps built with Ionic
import "@ionic/core/css/normalize.css";
import "@ionic/core/css/structure.css";
import "@ionic/core/css/typography.css";

//Optional CSS utils that can be commented out
import "@ionic/core/css/padding.css";
import "@ionic/core/css/float-elements.css";
import "@ionic/core/css/text-alignment.css";
import "@ionic/core/css/text-transformation.css";
import "@ionic/core/css/flex-utils.css";
import "@ionic/core/css/display.css";

// MUI Theme
import { Jost } from "next/font/google";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "../styles-mui/theme";
const mainFont = Jost({
    weight: ["300", "400", "500", "600", "700"],
    style: ["normal"],
    subsets: ["latin"],
});

// Custom
import "../styles-ionic/variables.scss";
import "../styles-mui/index.scss";
import "../styles-ionic/index.scss";

import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenu,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonApp,
    IonButton,
    IonMenuToggle,
    IonCol,
    IonRow,
    IonIcon,
    IonPopover,
    IonItem,
    IonLabel,
    IonGrid,
    IonFooter,
} from "@ionic/react";
import { menuController } from "@ionic/core";
import FooterMain from "../components/footer-main";

// Init/apply all ionic features
import { setupIonicReact } from "@ionic/react";
setupIonicReact();
import NonSSRWrapper from "../components/NoSSRWrapper";

// import AuthService from "../services/auth.service";
import authService from "../services/auth.service";
import Link from "next/link";
import MainNav from "../components/layout/main-nav";
import { menuOutline, personCircleOutline } from "ionicons/icons";
import ProfileMenu from "../components/header/profile-menu";
import { Box, Button, IconButton, Menu } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import SearchComponent from "../components/inputs/search";
// import ErrorBoundary from "../components/hooks/error-boundary";
import { ErrorBoundary } from "react-error-boundary";
import ErrorFallback from "../components/hooks/error-fallback";
import Head from "next/head";
import HeadShared from "../components/head-shared";

// function App({ Component, pageProps: { session, ...pageProps } }) {
function App({ Component, pageProps }) {
    // const isUser = AuthService.getCurrentUser();
    // const isAuth = AuthService.checkToken();

    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
    };

    const [userDetails, setUserDetails] = useState<boolean>(false);
    const getUserDetails = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
    };

    useEffect(() => {
        if (pageProps || Component) {
            (async () => {
                ionDefineCustomElements(window);
                checkToken();
                getUserDetails();
            })();
        }
    }, [pageProps, Component]);

    const menuRef = useRef<HTMLIonMenuElement>(null);

    // - - - - - - - - -
    // Check Area change and default the data for infinite scroll
    // - - - - - - - - -
    const handleAreaChange = (e) => {
        console.log("handleAreaChange");
        console.log(e);
        // if (e) {
        //     setEvents([]);
        //     getAllData(area, 10, 0);
        // }
    };

    // - - - - - - - - -
    // Check Search change and pass to components
    // - - - - - - - - -
    const [searchTxt, setSearchTxt] = useState("");
    const handleSearchChange = (e) => {
        console.log("handleSearchChange");
        console.log(e);
        setSearchTxt(e);
    };

    console.log("pageProps");
    console.log(pageProps);
    console.log("Component");
    console.log(Component);

    return (
        <>
            {/* Google tag (gtag.js) */}
            <Script
                async
                src="https://www.googletagmanager.com/gtag/js?id=G-NX59W092BG"
                strategy="afterInteractive"
            />
            <Script id="google-analytics" strategy="afterInteractive">
                {`
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', 'G-NX59W092BG');
                `}
            </Script>

            <HeadShared
                title={pageProps?.head?.title}
                description={pageProps?.head?.description}
                image={pageProps?.head?.image}
                type={pageProps?.head?.type}
            />

            <NonSSRWrapper>
                <IonApp className={mainFont.className}>
                    <ThemeProvider theme={theme}>
                        <IonMenu contentId="main-content" ref={menuRef}>
                            <IonHeader>
                                <IonToolbar>
                                    <IonTitle>Menu</IonTitle>
                                </IonToolbar>
                            </IonHeader>
                            <IonContent>
                                <MainNav
                                    isSmScreen={true}
                                    isAuth={isAuth}
                                    handleAreaChange={handleAreaChange}
                                />
                            </IonContent>
                            {/* {!isAuth && <IonContent>Public Menu Here!</IonContent>} */}
                        </IonMenu>

                        <IonPage id="main-content">
                            <IonHeader
                                translucent={true}
                                className="main-header"
                            >
                                <IonToolbar
                                    className="ion-display-flex ion-align-items-center"
                                    color="dark"
                                >
                                    <IonGrid className="container">
                                        <IonRow className="ion-align-items-center">
                                            <IonCol
                                                size-xs="3"
                                                size-sm="4"
                                                className="ion-hide-lg-up pl0"
                                            >
                                                {/* <IonButton
                                                fill="clear"
                                                color="light"
                                                onClick={async () =>
                                                    menuRef.current?.toggle()
                                                }
                                            >
                                                <IonIcon
                                                    slot="icon-only"
                                                    icon={menuOutline}
                                                ></IonIcon>
                                            </IonButton> */}
                                                <IconButton
                                                    color="inherit"
                                                    onClick={async () =>
                                                        menuRef.current?.toggle()
                                                    }
                                                >
                                                    <MenuIcon />
                                                </IconButton>
                                            </IonCol>

                                            <IonCol
                                                size-xs="5"
                                                size-sm="4"
                                                size-lg="3"
                                                className="ion-display-flex ion-align-items-center ion-justify-content-center-md-down ion-justify-content-start-md pl0 pr0"
                                            >
                                                <Link
                                                    href="/"
                                                    className="ion-display-flex mr-12"
                                                >
                                                    <img
                                                        height="32"
                                                        src="/assets/images/TribeHood-Logo.svg"
                                                    />
                                                </Link>
                                                {/* <Image
                                                width="158"
                                                height="32"
                                                src="/assets/images/TribeHood-Logo.svg"
                                            /> */}
                                            </IonCol>

                                            {/* <IonCol
                                                size-xs="5"
                                                size-sm="4"
                                                size-lg="6"
                                                className="ion-display-flex ion-align-items-center ion-justify-content-center-md-down ion-justify-content-start-md pl0 pr0"
                                            >
                                                <Box
                                                    sx={{
                                                        display: "flex",
                                                        width: "100%",
                                                        justifyContent:
                                                            "center",
                                                    }}
                                                >
                                                    <SearchComponent
                                                        toolbarSearch={true}
                                                        handleSearch={(
                                                            e: string
                                                        ) =>
                                                            handleSearchChange(
                                                                e
                                                            )
                                                        }
                                                    />
                                                </Box>
                                            </IonCol> */}

                                            <IonCol
                                                size-xs="4"
                                                size-sm="4"
                                                size-lg="9"
                                                className="ion-display-flex ion-justify-content-end pl0 pr0"
                                            >
                                                <div>
                                                    {isAuth && (
                                                        <ProfileMenu
                                                            userDetails={
                                                                userDetails
                                                            }
                                                        />
                                                    )}
                                                    {!isAuth && (
                                                        <Box
                                                            sx={{
                                                                display: "flex",
                                                                justifyContent:
                                                                    "end",
                                                            }}
                                                        >
                                                            <Link href="/users/login">
                                                                <Button
                                                                    sx={{
                                                                        mr: {
                                                                            sm: 2,
                                                                        },
                                                                    }}
                                                                    color="primary"
                                                                    size="small"
                                                                    variant="outlined"
                                                                >
                                                                    Login
                                                                </Button>
                                                            </Link>

                                                            <Box
                                                                sx={{
                                                                    display: {
                                                                        xs: "none",
                                                                        sm: "block",
                                                                    },
                                                                }}
                                                            >
                                                                <Link href="/users/register">
                                                                    <Button
                                                                        color="primary"
                                                                        size="small"
                                                                        variant="contained"
                                                                    >
                                                                        Register
                                                                    </Button>
                                                                </Link>
                                                            </Box>
                                                        </Box>
                                                    )}
                                                </div>
                                            </IonCol>
                                        </IonRow>
                                    </IonGrid>
                                </IonToolbar>
                            </IonHeader>

                            {/* <ErrorBoundary fallback="Error"> */}
                            <ErrorBoundary
                                FallbackComponent={ErrorFallback}
                                onReset={() =>
                                    console.log(
                                        "onReset clicked setCurrentUserId(0)"
                                    )
                                }
                                resetKeys={["currentUserId"]}
                                // fallback={<div>Something went wrong</div>}
                            >
                                <Component
                                    {...pageProps}
                                    handleSearchNav={searchTxt.trim()}
                                    // handleAreaChange={handleAreaChange}
                                />
                            </ErrorBoundary>

                            {/* Create account footer - XS */}
                            {/* {!isAuth && (
                                <IonFooter className="main-footer">
                                    <IonToolbar
                                        className="ion-display-flex ion-align-items-center ion-justify-content"
                                        color="dark"
                                    >
                                        <Box
                                            sx={{
                                                justifyContent: "center",
                                                alignItems: "center",
                                                display: {
                                                    xs: "flex",
                                                    sm: "none",
                                                },
                                            }}
                                        >
                                            <Link href="/users/register">
                                                <Button
                                                    color="primary"
                                                    size="small"
                                                    variant="contained"
                                                >
                                                    Create Account
                                                </Button>
                                            </Link>
                                        </Box>
                                    </IonToolbar>
                                </IonFooter>
                            )} */}
                        </IonPage>
                    </ThemeProvider>
                </IonApp>
            </NonSSRWrapper>
        </>
    );
    // <IonApp>
    //     <IonContent fullscreen>
    //         {/* <SessionProvider session={pageProps.session}> */}
    //         <Component {...pageProps} />
    //         {/* </SessionProvider> */}
    //     </IonContent>
    //     {/* <FooterMain /> */}
    // </IonApp>
}

export default App;
