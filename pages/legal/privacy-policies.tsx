import {
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonContent,
} from "@ionic/react";
import HeadShared from "../../components/head-shared";
import HeaderSecondary from "../../components/header/header-secondary";

export default function PrivacyPoliciesPage() {
    return (
        <>
            <HeadShared
                title="Privacy Policy"
                description="TribeHood's privacy policy"
            ></HeadShared>

            <HeaderSecondary
                title="Privacy Policy"
                isBack={true}
                isToolbar={true}
            ></HeaderSecondary>

            <IonContent>
                <IonGrid className="container">
                    <IonRow>
                        <IonCol size="12">
                            <IonCard>
                                <IonCardHeader>
                                    <IonCardTitle>Privacy Policy</IonCardTitle>
                                </IonCardHeader>
                                <IonCardContent>
                                    <p>Coming soon!</p>
                                </IonCardContent>
                            </IonCard>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </>
    );
}
