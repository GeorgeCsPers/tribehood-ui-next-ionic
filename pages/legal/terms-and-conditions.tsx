import {
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonContent,
} from "@ionic/react";
import HeadShared from "../../components/head-shared";
import HeaderSecondary from "../../components/header/header-secondary";

export default function TermsAndConditionsPage() {
    return (
        <>
            <HeadShared
                title="Terms and Conditions"
                description="TribeHood's terms and conditions"
            ></HeadShared>

            <HeaderSecondary
                title="Terms and Conditions"
                isBack={true}
                isToolbar={true}
            ></HeaderSecondary>

            <IonContent>
                <IonGrid className="container">
                    <IonRow>
                        <IonCol size="12">
                            <IonCard>
                                <IonCardHeader>
                                    <IonCardTitle>
                                        Terms and Conditions
                                    </IonCardTitle>
                                </IonCardHeader>
                                <IonCardContent>
                                    <p>Coming soon!</p>
                                </IonCardContent>
                            </IonCard>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </>
    );
}
