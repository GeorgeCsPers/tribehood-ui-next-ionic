import {
    IonContent,
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
} from "@ionic/react";
import Image from "next/image";
import HeaderSecondary from "../components/header/header-secondary";
import { Container, Grid } from "@mui/material";
import LeftSide from "../components/layout/left-side";
import RightSide from "../components/layout/right-side";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import authService from "../services/auth.service";
// import Loading from "../components/common/loading";
// import CardEventComponent from "../components/surface/card-event";
// import { getPublicData } from "../services/data.service";
import HeadShared from "../components/head-shared";

export default function HomePage() {
    const router = useRouter();
    const { query = {} } = router || {};
    // const { area = "wallingford" } = query || {};
    console.log("HomePage query.area");
    console.log(query.area);
    const area = query.area || "all";

    // const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        // if (!isToken) {
        //     router.push("/users/login");
        // }
        // checkUser();
    };

    const [userDetails, setUserDetails] = useState<any>();
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
        // getData(user);
    };

    // API Data
    // const [events, setEvents] = useState([]);
    // const [categoryTypes, setCategoryTypes] = useState([]);
    // const [categories, setCategories] = useState([]);
    // const getAll = async () => {
    //     const eventsRawData = await getPublicData(
    //         "events?take=20&skip=0&sortBy=createdAt&sortOrder=asc"
    //     );
    //     const categoryTypesRawData = await getPublicData(
    //         "category-types/selection"
    //     );
    //     const categoriesRawData = await getPublicData("categories/selection");
    //     // const rawData = await getData("events");
    //     setEvents(eventsRawData);
    //     setCategoryTypes(categoryTypesRawData);
    //     setCategories(categoriesRawData);
    //     setIsLoading(false);
    // };

    useEffect(() => {
        if (area) {
            (async () => {
                await checkToken();
                let updatedArea = area;
                if (isAuth) {
                    await checkUser();
                    if (userDetails) {
                        if (userDetails.userArea) {
                            updatedArea = userDetails.userArea;
                        }
                    }
                }
                router.push(`/${updatedArea}`);
            })();
        }
        // checkToken();
        // getAll();
    }, [area, isAuth]);

    // - - - - - - - - -
    // Check Area change and default the data for infinite scroll
    // - - - - - - - - -
    // const handleAreaChange = (e) => {
    //     console.log("handleAreaChange");
    //     console.log(e);
    //     if (e) {
    //         setEvents([]);
    //         getAllData(area, 10, 0);
    //     }
    // };

    console.log("userDetails");
    console.log(userDetails);

    return (
        <>
            <HeadShared
                title="Tribehood Centre"
                description="Community events, places and news"
            ></HeadShared>

            <HeaderSecondary title="Centre"></HeaderSecondary>

            <IonContent>
                <Container sx={{ mt: 4, pb: 4 }}>
                    <IonRow>
                        <IonCol
                            size-lg="3"
                            size-xl="3"
                            className="ion-hide-lg-down"
                        >
                            <LeftSide
                            // fixedClass={fixedClass}
                            // handleAreaChange={handleAreaChange}
                            />
                        </IonCol>
                        <IonCol size-xs="12" size-lg="6" size-xl="6">
                            <Grid container spacing={2}>
                                {/* {!isLoading ? (
                                    events?.map((item, i) => (
                                        <Grid
                                            item
                                            xs={12}
                                            key={i}
                                            sx={{
                                                display: "flex",
                                                alignItems: "stretch",
                                            }}
                                        >
                                            <CardEventComponent
                                                data={item}
                                                // showModal={showModal}
                                                // setShowModal={setShowModal}
                                            />
                                        </Grid>
                                    ))
                                ) : (
                                    <Loading />
                                )} */}
                            </Grid>
                        </IonCol>
                        <IonCol
                            size-lg="3"
                            size-xl="3"
                            className="ion-hide-lg-down"
                        >
                            <RightSide />
                        </IonCol>
                    </IonRow>
                </Container>
            </IonContent>
        </>
    );
}
