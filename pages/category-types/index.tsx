import {
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardSubtitle,
    IonCardContent,
    IonContent,
} from "@ionic/react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
// import HeadShared from "../../components/head-shared";
import HeaderSecondary from "../../components/header/header-secondary";
import { postData, getData } from "../../services/data.service";
import authService from "../../services/auth.service";
import LoginOrRegister from "../../components/login-or-register";
import FullscreenModal from "../../components/modals/modal-fullscreen";
import {
    Avatar,
    Box,
    Button,
    Card,
    CardContent,
    CardHeader,
    Container,
    Divider,
    Grid,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
    Typography,
} from "@mui/material";
import React from "react";
import Loading from "../../components/common/loading";
import CategoryTypesForm from "../../components/forms/category-types-form";

export default function CategoryTypesPage() {
    const router = useRouter();
    // const isAuth = authService.checkToken();

    const [isLoading, setIsLoading] = useState<boolean>(true);

    // - - - - - - - -
    // Check if user logged in and token is valid
    // - - - - - - - -
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        // if (!isToken) {
        //     router.push("/users/login");
        // }
    };
    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
    };

    // - - - - - - -
    // GET APIs Data
    // - - - - - - -
    const [categoryTypes, setCategoryTypes] = useState([]);
    // const [isAllowedToManage, setIsAllowedToManage] = useState<boolean>(false);
    const getAllData = async () => {
        const rawData = await getData(`category-types`);
        setCategoryTypes(rawData);

        // Check if user allowed to manage
        // if (isAuth) {
        //     const isAllowed = isAllowedToManageChecker(
        //         userDetails,
        //         eventsRawData
        //     );
        //     setIsAllowedToManage(isAllowed);
        // }
        // console.log("isAllowedToManage");
        // console.log(isAllowedToManage);

        setIsLoading(false);
    };

    useEffect(() => {
        (async () => {
            await checkToken();
            await checkUser();
            // if (isAuth) {
            await getAllData();
            // }
        })();
    }, [isAuth]);

    console.log("Category Types");
    console.log(categoryTypes);
    console.log("isAuth");
    console.log(isAuth);

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    return (
        <>
            {/* <HeadShared
                title="Tools"
                description="Collection of useful tools"
            ></HeadShared> */}

            <HeaderSecondary
                title="Category Types"
                isBack={true}
                isToolbar={true}
            ></HeaderSecondary>

            {/* {isAuth && isAllowedToManage && ( */}
            {isAuth && (
                <FullscreenModal
                    title={modalTitle}
                    showModal={showModal}
                    setShowModal={setShowModal}
                >
                    {!isLoading && (
                        <CategoryTypesForm
                            recordForEdit={recordForEdit}
                            userId={userDetails?.id}
                        />
                    )}
                </FullscreenModal>
            )}

            <IonContent>
                {!isLoading ? (
                    <Box sx={{ mt: 4, pb: 4 }}>
                        <Container sx={{ mt: 4, pb: 4 }}>
                            {isAuth ? (
                                <Card>
                                    <CardHeader
                                        title="Category Types"
                                        action={
                                            <Button
                                                sx={{
                                                    display: "flex",
                                                }}
                                                variant="contained"
                                                size="small"
                                                color="secondary"
                                                onClick={() => {
                                                    setShowModal(true);
                                                    setRecordForEdit(null);
                                                    setModalTitle(
                                                        "Add New Category Type"
                                                    );
                                                }}
                                            >
                                                Add New
                                            </Button>
                                        }
                                    ></CardHeader>

                                    <CardContent sx={{ pt: 0 }}>
                                        {/* {isAuth ? ( */}

                                        <List
                                            sx={{
                                                width: "100%",
                                            }}
                                        >
                                            {categoryTypes?.map((item, i) => (
                                                <Box key={i}>
                                                    <ListItem
                                                        sx={{
                                                            display: "flex",
                                                            alignItems:
                                                                "center",
                                                            justifyContent:
                                                                "start",
                                                            width: "100%",
                                                        }}
                                                    >
                                                        <ListItemAvatar
                                                            sx={{
                                                                display: "flex",
                                                                alignItems:
                                                                    "center",
                                                                justifyContent:
                                                                    "start",
                                                            }}
                                                        >
                                                            <Avatar
                                                                alt="C"
                                                                src={
                                                                    item?.featuredImageUrl
                                                                }
                                                            />
                                                        </ListItemAvatar>

                                                        <Box
                                                            sx={{
                                                                display: "flex",
                                                                alignItems:
                                                                    "start",
                                                                justifyContent:
                                                                    "start",
                                                                width: "100%",
                                                                flexDirection:
                                                                    "column",
                                                            }}
                                                        >
                                                            <Typography variant="h6">
                                                                {item.name}
                                                            </Typography>

                                                            <Typography variant="body1">
                                                                {
                                                                    item.description
                                                                }
                                                            </Typography>
                                                        </Box>

                                                        <Box
                                                            sx={{
                                                                display: "flex",
                                                                alignItems:
                                                                    "center",
                                                                justifyContent:
                                                                    "end",
                                                            }}
                                                        >
                                                            <Button
                                                                sx={{
                                                                    display:
                                                                        "flex",
                                                                    justifySelf:
                                                                        "end",
                                                                    mr: 2,
                                                                }}
                                                                variant="contained"
                                                                size="small"
                                                                color="primary"
                                                                onClick={() => {
                                                                    setShowModal(
                                                                        true
                                                                    );
                                                                    setRecordForEdit(
                                                                        item
                                                                    );
                                                                    setModalTitle(
                                                                        "Edit Category Type"
                                                                    );
                                                                }}
                                                            >
                                                                Edit
                                                            </Button>
                                                        </Box>
                                                    </ListItem>
                                                    <Divider />
                                                </Box>
                                            ))}
                                        </List>
                                    </CardContent>
                                </Card>
                            ) : (
                                <LoginOrRegister />
                            )}
                        </Container>
                    </Box>
                ) : (
                    <Loading />
                )}
            </IonContent>
        </>
    );
}
