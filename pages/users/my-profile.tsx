import {
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardSubtitle,
    IonCardContent,
    IonContent,
    IonModal,
    IonButton,
} from "@ionic/react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import HeadShared from "../../components/head-shared";
import HeaderSecondary from "../../components/header/header-secondary";
import {
    postData,
    getData,
    getPublicData,
    getSingleData,
} from "../../services/data.service";
import authService from "../../services/auth.service";
import LoginOrRegister from "../../components/login-or-register";
import FullscreenModal from "../../components/modals/modal-fullscreen";
import StorageService from "../../services/storage.service";

export default function MyProfilePage() {
    const router = useRouter();
    // const isAuth = authService.checkToken();

    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        // const user = await authService.getCurrentUser();
        // if (await user) {
        //     setUserDetails(await user);
        // }
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        if (!isToken) {
            router.push("/users/login");
        } else {
            checkUser();
        }
    };
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        // setUserDetails(user);
        getData(user);
    };

    // API Data
    const [profileData, setProfileData] = useState([]);
    const getData = async (user: any) => {
        console.log("userDetails");
        console.log(user);

        // const rawData = await getPublicData("events");
        // const rawData = await getData(`users`);
        const rawData = await getSingleData(`users`, user?.id);
        console.log("rawData");
        console.log(rawData);
        setProfileData(rawData);
    };

    useEffect(() => {
        checkToken();
    }, []);

    console.log("profileData");
    console.log(profileData);
    console.log("isAuth");
    console.log(isAuth);

    return (
        <>
            {/* <HeadShared
                title="Commands"
                description="Snapshot to useful commands"
            ></HeadShared> */}

            <HeaderSecondary title="My Profile"></HeaderSecondary>

            <IonContent>
                <IonGrid className="container">
                    <IonRow>
                        <IonCol
                            size="12"
                            // size-sm="6"
                            // size-md="3"
                        >
                            <IonCard>
                                {/* <Image
                                    src="/cat.jpg"
                                    alt="Picture of the author"
                                    width={500}
                                    height={500}
                                /> */}
                                <IonCardHeader>
                                    <IonCardTitle>Details</IonCardTitle>
                                    <IonCardSubtitle></IonCardSubtitle>
                                </IonCardHeader>
                                <IonCardContent>
                                    {/* <ion-icon
                                        name="pin"
                                        slot="start"
                                    ></ion-icon> */}
                                    {/* <pre className="code-box">
                                                {item.command1}
                                            </pre> */}
                                </IonCardContent>
                            </IonCard>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </>
    );
}

// // This function gets called at build time
// export async function getStaticProps() {
//     // Call an external API endpoint to get posts
//     // const rawData = await getData(null, "commands");
//     const commands = await getPublicData("commands");

//     // By returning { props: { posts } }, the Blog component
//     // will receive `posts` as a prop at build time
//     return {
//         props: {
//             commands,
//         },
//     };
// }
