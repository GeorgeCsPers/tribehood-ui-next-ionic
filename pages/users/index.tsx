import {
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardSubtitle,
    IonCardContent,
    IonContent,
} from "@ionic/react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
// import HeadShared from "../../components/head-shared";
import HeaderSecondary from "../../components/header/header-secondary";
import { postData, getData, getPublicData } from "../../services/data.service";
import authService from "../../services/auth.service";
import LoginOrRegister from "../../components/login-or-register";

export default function UsersPage() {
    const router = useRouter();
    // const isAuth = authService.checkToken();

    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
    };

    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userRole, setUserRole] = useState({});
    const [users, setUsers] = useState([]);
    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = async () => {
        // const user = await authService.getCurrentUser();
        const userRoleCheck = await authService.checkRole();
        // setUserDetails(user);
        setUserRole(userRoleCheck);
        // setUserRole(userRole);
    };

    useEffect(() => {
        (async () => {
            await checkToken();

            if (isAuth) {
                await checkUser();
                if (userRole) {
                    if (
                        (isAuth && userRole === "sys_admin") ||
                        (isAuth && userRole === "admin")
                    ) {
                        // getAll();
                        // API Data

                        // const getAll = async () => {
                        // if (isAuth || userRole === "sys_admin") {
                        // const rawData = await getPublicData("tools");
                        const rawData = await getData("users");
                        setUsers(rawData);
                        // }
                        // };
                    } else {
                        router.push("/users/login");
                    }
                }
            }
        })();
        // getAll();
        // getAllData();
        // console.log("Commands");
        // console.log(getAllData());
        // const rawData = getAllData();
        // setCommands(rawData);
    }, [isAuth, userRole]);

    console.log("Users");
    console.log(users);
    console.log("isAuth");
    console.log(isAuth);

    return (
        <>
            {/* <HeadShared
                title="Tools"
                description="Collection of useful tools"
            ></HeadShared> */}

            <HeaderSecondary
                title="Users"
                isBack={true}
                isToolbar={true}
            ></HeaderSecondary>

            <IonContent>
                <IonGrid className="container">
                    <IonRow>
                        {isAuth ? (
                            users?.map((item, i) => (
                                <IonCol
                                    key={i}
                                    size="12"
                                    size-sm="6"
                                    // size-md="3"
                                >
                                    <IonCard>
                                        {/* <Image
                                    src="/cat.jpg"
                                    alt="Picture of the author"
                                    width={500}
                                    height={500}
                                /> */}
                                        <IonCardHeader>
                                            <IonCardTitle>
                                                {item.firstName} {item.lastName}
                                            </IonCardTitle>
                                            <IonCardSubtitle>
                                                {/* {item.role.name} */}
                                            </IonCardSubtitle>
                                        </IonCardHeader>
                                        <IonCardContent>
                                            <p>{item.email}</p>
                                        </IonCardContent>
                                    </IonCard>
                                </IonCol>
                            ))
                        ) : (
                            // <IonCol size="12">
                            <LoginOrRegister />
                            // </IonCol>
                        )}
                    </IonRow>
                </IonGrid>
            </IonContent>
        </>
    );
}
