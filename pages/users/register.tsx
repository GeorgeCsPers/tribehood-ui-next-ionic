import axios from "axios";
import { useRouter } from "next/router";
import { useState, FormEventHandler, useEffect } from "react";
import HeaderMain from "../../components/header/header-secondary";
import { personCircle } from "ionicons/icons";
import { useForm } from "react-hook-form";
// https://github.com/effiongcharles/login_auth/blob/master/src/pages/Login.tsx
import {
    IonButton,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonCard,
    IonPage,
    IonContent,
    IonAlert,
    IonIcon,
    IonToggle,
    IonCol,
    IonGrid,
    IonRow,
    IonCardContent,
    IonCardHeader,
    IonCardTitle,
} from "@ionic/react";

import authService from "../../services/auth.service";
import Link from "next/link";
import {
    Box,
    Button,
    Card,
    CardContent,
    Container,
    Divider,
    FormControl,
    InputLabel,
    Select,
    Grid,
    MenuItem,
    TextField,
    Typography,
} from "@mui/material";
import { getPublicData } from "../../services/data.service";
import Loading from "../../components/common/loading";
import SnackbarComp from "../../components/common/snackbars";

function validateEmail(email: string) {
    const re =
        /^((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))$/;
    return re.test(String(email).toLowerCase());
}
const emailPattern =
    /^((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))$/;
// const API_URL = process.env.NEXT_PUBLIC_API_URL;
// const API_URL = "https://univ-s1.goldilab.com/api/";

const accountTypes = [
    {
        id: "personal",
        name: "Personal",
    },
    {
        id: "organisation",
        name: "Organisation",
    },
];

export default function Login() {
    const router = useRouter();
    const {
        register,
        handleSubmit,
        control,
        setValue,
        getValues,
        reset,
        formState: { isSubmitting, isDirty, isValid, errors },
    } = useForm({
        mode: "onChange",
    });

    const formMessages = {
        email: "Please enter a valid email address",
        password: "Please enter a valid password (min 6 character)",
        firstName: "Please enter your first name",
        lastName: "Please enter your last name",
    };

    // Message handlers
    const [showSnackbar, setShowSnackbar] = useState(false);
    const [snackbarType, setSnackbarType] = useState("");
    const [snackbarMessage, setSnackbarMessage] = useState("");

    // Check token if exist, if yes redirect
    const checkToken = async () => {
        const isToken = await authService.checkToken();

        console.log("isToken");
        console.log(isToken);
        console.log(router.route);

        if (isToken && router.route === "/users/register") {
            router.push("/");
        }
    };

    const [isSelectionLoading, setIsSelectionLoading] = useState(true);
    const [isConfirmMessage, setIsConfirmMessage] = useState(false);
    const [areas, setAreas] = useState([]);
    const [accountTypes, setAccountTypes] = useState([]);

    const getSelectionData = async () => {
        const areasRawData = await getPublicData("location/areas/selection");
        const accountTypesRawData = await getPublicData(
            "account-types/selection"
        );
        setAreas(areasRawData);
        setUserAreaProp(areasRawData[0].id);
        setValue("userArea", { id: areasRawData[0].id });

        setAccountTypes(accountTypesRawData);
        setAccountTypeProp(accountTypesRawData[0].id);
        setValue("accountType", { id: accountTypesRawData[0].id });

        setIsSelectionLoading(false);
    };

    useEffect(() => {
        checkToken();
        getSelectionData();
    }, []);

    console.log("areas");
    console.log(areas);
    console.log("accountTypes");
    console.log(accountTypes);

    // Selection
    const [accountTypeProp, setAccountTypeProp] = useState(
        accountTypes[0]?.id
        // props.recordForEdit?.categoryTypes || []
    );
    const handleAccountTypeSelection = (data: any) => {
        console.log("handleCategoryTypeSelection");
        console.log(data);
        console.log(data.target.value);

        setAccountTypeProp(data.target.value);
        setValue("accountType", [{ id: data.target.value }]);
    };

    const [userAreaProp, setUserAreaProp] = useState([]);
    const handleUserAreaSelection = (data: any) => {
        console.log("handleUserAreaSelection");
        console.log(data);
        console.log(data.target.value);

        setUserAreaProp(data.target.value);
        setValue("userArea", { id: data.target.value });
    };
    console.log("userAreaProp");
    console.log(userAreaProp);

    async function onSubmit({
        firstName,
        lastName,
        email,
        password,
        accountType,
        userArea,
    }) {
        console.log("onSubmit");

        if (!email) {
            setSnackbarType("error");
            setSnackbarMessage("Please enter a valid email");
            setShowSnackbar(true);
            return;
        }

        if (validateEmail(email) === false) {
            setSnackbarType("error");
            setSnackbarMessage("The email address is invalid");
            setShowSnackbar(true);
            return;
        }

        // if (!password || password.length < 6) {
        if (!password) {
            setSnackbarType("error");
            setSnackbarMessage("Please enter your password");
            setShowSnackbar(true);

            return;
        }

        if (password.length < 6) {
            setSnackbarType("error");
            setSnackbarMessage("Password must be min 6 characters");
            setShowSnackbar(true);

            return;
        }

        const dataBody = {
            firstName,
            lastName,
            email,
            password,
            accountType,
            userArea,
        };

        console.log("dataBody");
        console.log(dataBody);

        authService.register(dataBody).then(
            async (res) => {
                console.log("register res");
                console.log(await res);

                reset([]);
                setIsConfirmMessage(true);
                // router.push("/users/login");
            },
            async (err) => {
                console.log("err");
                console.log(await err);

                setSnackbarType("error");
                setSnackbarMessage(await err.response.data.message);
                setShowSnackbar(true);
            }
        );
    }

    return (
        <>
            {/* <HeaderMain props={{ title: "Login" }}></HeaderMain> */}

            <IonContent fullscreen className="ion-padding ion-text-start">
                {/* <IonGrid>
                    <form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                        <IonRow>
                            <IonCol> */}
                {showSnackbar && (
                    <SnackbarComp
                        type={snackbarType}
                        message={snackbarMessage}
                        // transition="TransitionUp"
                    />
                )}
                {/* </IonCol>
                        </IonRow> */}

                <Container>
                    <Grid
                        container
                        spacing={2}
                        className="full-height-420"
                        sx={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                        }}
                    >
                        <Grid item xs={12} sm={8} md={6} lg={5} xl={5}>
                            <Card>
                                <CardContent>
                                    <Typography variant="h4" sx={{ mb: 4 }}>
                                        Create your account
                                    </Typography>
                                    {!isConfirmMessage ? (
                                        <Box>
                                            <Box
                                                component="form"
                                                noValidate
                                                autoComplete="off"
                                                onSubmit={handleSubmit(
                                                    onSubmit
                                                )}
                                            >
                                                <TextField
                                                    fullWidth
                                                    label="First Name"
                                                    type="text"
                                                    sx={{ mb: 2 }}
                                                    helperText={
                                                        errors.firstName ? (
                                                            <Typography
                                                                variant="body2"
                                                                color="error"
                                                            >
                                                                {
                                                                    formMessages.firstName
                                                                }
                                                            </Typography>
                                                        ) : (
                                                            ""
                                                        )
                                                    }
                                                    {...register("firstName", {
                                                        required: true,
                                                    })}
                                                    color={
                                                        errors.firstName
                                                            ? "error"
                                                            : "primary"
                                                    }
                                                    required
                                                />

                                                <TextField
                                                    fullWidth
                                                    label="Last Name"
                                                    type="text"
                                                    sx={{ mb: 2 }}
                                                    helperText={
                                                        errors.lastName ? (
                                                            <Typography
                                                                variant="body2"
                                                                color="error"
                                                            >
                                                                {
                                                                    formMessages.lastName
                                                                }
                                                            </Typography>
                                                        ) : (
                                                            ""
                                                        )
                                                    }
                                                    {...register("lastName", {
                                                        required: true,
                                                    })}
                                                    color={
                                                        errors.lastName
                                                            ? "error"
                                                            : "primary"
                                                    }
                                                    required
                                                />

                                                {!isSelectionLoading ? (
                                                    <FormControl
                                                        fullWidth
                                                        sx={{ mb: 2 }}
                                                    >
                                                        <InputLabel id="type-selection-label">
                                                            Account Type*
                                                        </InputLabel>
                                                        <Select
                                                            labelId="type-selection-label"
                                                            onChange={(e) =>
                                                                handleAccountTypeSelection(
                                                                    e
                                                                )
                                                            }
                                                            value={
                                                                accountTypeProp
                                                            }
                                                            // defaultValue={categoryTypesProp}
                                                            // renderValue={() => categoryTypesProp[0].id}
                                                            // inputProps={register(
                                                            //     "accountTypeId",
                                                            //     {
                                                            //         required:
                                                            //             "Please choose",
                                                            //     }
                                                            // )}
                                                            inputProps={register(
                                                                "accountType",
                                                                {
                                                                    required:
                                                                        "Please choose",
                                                                }
                                                            )}
                                                        >
                                                            {accountTypes.map(
                                                                (item, i) => (
                                                                    <MenuItem
                                                                        key={i}
                                                                        value={
                                                                            item.id
                                                                        }
                                                                    >
                                                                        {
                                                                            item.name
                                                                        }
                                                                    </MenuItem>
                                                                )
                                                            )}
                                                        </Select>
                                                    </FormControl>
                                                ) : (
                                                    <Loading />
                                                )}

                                                {!isSelectionLoading ? (
                                                    <FormControl
                                                        fullWidth
                                                        sx={{ mb: 2 }}
                                                    >
                                                        <InputLabel id="area-selection-label">
                                                            Default Area*
                                                        </InputLabel>
                                                        <Select
                                                            labelId="area-selection-label"
                                                            onChange={(e) =>
                                                                handleUserAreaSelection(
                                                                    e
                                                                )
                                                            }
                                                            // value={areas[0].id}
                                                            value={userAreaProp}
                                                            // defaultValue={areas[0].id}
                                                            // defaultValue={categoryTypesProp}
                                                            // renderValue={() => categoryTypesProp[0].id}
                                                            // inputProps={register(
                                                            //     "userAreaId",
                                                            //     {
                                                            //         required:
                                                            //             "Please choose",
                                                            //     }
                                                            // )}
                                                            inputProps={register(
                                                                "userArea",
                                                                {
                                                                    required:
                                                                        "Please choose",
                                                                }
                                                            )}
                                                        >
                                                            {areas.map(
                                                                (item, i) => (
                                                                    <MenuItem
                                                                        key={i}
                                                                        value={
                                                                            item.id
                                                                        }
                                                                    >
                                                                        {
                                                                            item.name
                                                                        }
                                                                    </MenuItem>
                                                                )
                                                            )}
                                                        </Select>
                                                    </FormControl>
                                                ) : (
                                                    <Loading />
                                                )}

                                                <TextField
                                                    fullWidth
                                                    label="Email"
                                                    type="email"
                                                    sx={{ mb: 2 }}
                                                    helperText={
                                                        errors.email ? (
                                                            <Typography
                                                                variant="body2"
                                                                color="error"
                                                            >
                                                                {
                                                                    formMessages.email
                                                                }
                                                            </Typography>
                                                        ) : (
                                                            ""
                                                        )
                                                    }
                                                    {...register("email", {
                                                        required: true,
                                                        pattern: emailPattern,
                                                    })}
                                                    color={
                                                        errors.email
                                                            ? "error"
                                                            : "primary"
                                                    }
                                                    required
                                                />

                                                <TextField
                                                    fullWidth
                                                    label="Password"
                                                    type="password"
                                                    sx={{ mb: 2 }}
                                                    helperText={
                                                        errors.password ? (
                                                            <Typography
                                                                variant="body2"
                                                                color="error"
                                                            >
                                                                {
                                                                    formMessages.password
                                                                }
                                                            </Typography>
                                                        ) : (
                                                            ""
                                                        )
                                                    }
                                                    {...register("password", {
                                                        minLength: 6,
                                                        required: true,
                                                    })}
                                                    color={
                                                        errors.password
                                                            ? "error"
                                                            : "primary"
                                                    }
                                                    required
                                                />

                                                <Box>
                                                    <Typography
                                                        variant="body2"
                                                        sx={{ mb: 2 }}
                                                    >
                                                        By clicking Register you
                                                        agree to our{" "}
                                                        <Link
                                                            target="_blank"
                                                            href="/legal/terms-and-conditions"
                                                        >
                                                            Terms and Conditions
                                                        </Link>{" "}
                                                        and{" "}
                                                        <Link
                                                            target="_blank"
                                                            href="/legal/privacy-policy"
                                                        >
                                                            Privacy Policy
                                                        </Link>
                                                    </Typography>
                                                </Box>
                                                <Button
                                                    fullWidth
                                                    type="submit"
                                                    size="large"
                                                    variant="contained"
                                                    color="secondary"
                                                    disabled={
                                                        !isDirty || !isValid
                                                    }
                                                >
                                                    Register
                                                </Button>
                                            </Box>

                                            <Box>
                                                <Divider
                                                    // variant="center"
                                                    sx={{ mt: 4, mb: 4 }}
                                                />
                                            </Box>

                                            <Box sx={{ mb: 2 }}>
                                                <Typography
                                                    variant="h6"
                                                    sx={{ mb: 2 }}
                                                >
                                                    Already having an account?
                                                </Typography>
                                                <Link href="/users/login">
                                                    <Button
                                                        fullWidth
                                                        size="large"
                                                        variant="outlined"
                                                        color="secondary"
                                                    >
                                                        Login
                                                    </Button>
                                                </Link>
                                            </Box>
                                        </Box>
                                    ) : (
                                        <Box sx={{ pt: 8, pb: 2 }}>
                                            <Typography
                                                variant="h5"
                                                textAlign="center"
                                            >
                                                Please check your email to
                                                confirm your account!
                                            </Typography>
                                            <Typography
                                                variant="body1"
                                                textAlign="center"
                                                mt="16px"
                                                mb="16px"
                                            >
                                                Confirmation link is active for
                                                2 hours.
                                            </Typography>

                                            <Link href="/users/login">
                                                <Button
                                                    fullWidth
                                                    size="large"
                                                    variant="contained"
                                                    color="primary"
                                                    sx={{ mt: 8 }}
                                                >
                                                    Login
                                                </Button>
                                            </Link>
                                        </Box>
                                    )}
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Container>
            </IonContent>
        </>
    );
}
