import { useRouter } from "next/router";
import {
    getPublicData,
    putPublicData,
} from "../../../../../services/data.service";
import Link from "next/link";
import {
    Box,
    Button,
    Card,
    CardContent,
    Container,
    Grid,
    TextField,
    Typography,
} from "@mui/material";
import { useState, useEffect } from "react";
import { IonContent } from "@ionic/react";
import { useForm } from "react-hook-form";
import SnackbarComp from "../../../../../components/common/snackbars";

export default function PasswordResetPage() {
    const router = useRouter();
    const {
        register,
        handleSubmit,
        control,
        setValue,
        getValues,
        reset,
        formState: { isSubmitting, isDirty, isValid, errors },
    } = useForm({
        mode: "onChange",
    });

    const formMessages = {
        password: "Please enter a valid password (min 6 character)",
        repeatPassword: "Please enter a valid password (min 6 character)",
    };

    const { query } = router || {};
    const userId = query.userId;
    // const userEmail = query.userEmail;
    const passwordResetId = query.passwordResetId;

    // Message handlers
    const [showSnackbar, setShowSnackbar] = useState(false);
    const [snackbarType, setSnackbarType] = useState("");
    const [snackbarMessage, setSnackbarMessage] = useState("");

    const [user, setUser] = useState<any>({});
    const [isExpired, setIsExpired] = useState<boolean>(false);
    const [isValidId, setIsValidId] = useState<boolean>(true);

    useEffect(() => {
        (async () => {
            if (userId) {
                await getPublicData(`users/reset/${userId}/null`).then(
                    async (resp: any) => {
                        setUser(resp);
                        validateResetPassword(resp);
                    },
                    async (err) => {
                        setSnackbarType("error");
                        setSnackbarMessage(await err.response.data.message);
                        setShowSnackbar(true);
                    }
                );
            }
        })();
    }, [userId]);

    const validateResetPassword = async (user: any) => {
        console.log("validateResetPassword user");
        console.log(user);
        if (user.passwordResetId === passwordResetId) {
            setIsValidId(true);

            if (new Date(user.passwordResetExpires) > new Date()) {
                setIsExpired(false);
            } else {
                setIsExpired(true);
            }
        } else {
            setIsValidId(false);
        }
    };

    async function onSubmitResetPassword({ password, repeatPassword }) {
        // Check password validity
        if (!password) {
            setSnackbarType("error");
            setSnackbarMessage("Please enter your password");
            setShowSnackbar(true);

            return;
        }

        if (password.length < 6) {
            setSnackbarType("error");
            setSnackbarMessage("Password must be min 6 characters");
            setShowSnackbar(true);

            return;
        }

        // Check if password matching
        const isPswCheckOk = await checkPasswordMatch(password, repeatPassword);

        if (isPswCheckOk) {
            // Prepare and update user password
            const dataBody = {
                id: user.id,
                passwordResetExpires: user.passwordResetExpires,
                password,
            };

            await putPublicData(`users/reset`, user.id, dataBody).then(
                async (resp: any) => {
                    reset([]);
                    setSnackbarType("success");
                    setSnackbarMessage("Password successfully updated!");
                    setShowSnackbar(true);

                    setTimeout(() => {
                        router.push("/users/login");
                    }, 2000);
                },
                async (err) => {
                    setSnackbarType("error");
                    setSnackbarMessage(await err.response.data.message);
                    setShowSnackbar(true);
                }
            );
        }
    }

    const [isPasswordMatching, setIsPasswordMatching] = useState<boolean>(null);
    const checkPasswordMatch = async (
        password: string,
        repeatPassword: string
    ) => {
        if (password === repeatPassword) {
            setIsPasswordMatching(true);
            return true;
        } else {
            setIsPasswordMatching(false);
            return false;
        }
    };

    const resendPasswordReset = async () => {
        await putPublicData(`users/send-reset`, user.id, {
            id: user.id,
        }).then((resp: any) => {
            console.log("resendPasswordReset putPublicData send-reset resp");
            console.log(resp);

            setSnackbarType("success");
            setSnackbarMessage(
                "New Email for Password Reset has been sent to your email!"
            );
            setShowSnackbar(true);
        });
    };

    console.log("ConfirmEmailPage user");
    console.log(user);
    console.log(passwordResetId);

    return (
        <>
            <IonContent fullscreen className="ion-padding ion-text-start">
                {showSnackbar && (
                    <SnackbarComp
                        type={snackbarType}
                        message={snackbarMessage}
                        // transition="TransitionUp"
                    />
                )}

                <Container>
                    <Grid
                        container
                        spacing={2}
                        className="full-height-420"
                        sx={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                        }}
                    >
                        <Grid item xs={12} sm={8} md={6} lg={5} xl={5}>
                            <Card>
                                <CardContent>
                                    <Typography variant="h4" sx={{ mb: 8 }}>
                                        Account confirmation
                                    </Typography>

                                    {/* <Typography variant="body1">
                                        {new Date(
                                            user.passwordResetExpires
                                        ).getHours()}{" "}
                                        vs currrent {new Date().getHours()}
                                    </Typography>
                                    <Typography variant="body1">
                                        {user.confirmationId} vs query
                                        confirmationId {confirmationId}
                                    </Typography> */}

                                    {isValidId ? (
                                        <Box>
                                            {!isExpired ? (
                                                <Box
                                                    component="form"
                                                    noValidate
                                                    autoComplete="off"
                                                    onSubmit={handleSubmit(
                                                        onSubmitResetPassword
                                                    )}
                                                >
                                                    <TextField
                                                        fullWidth
                                                        label="New Password"
                                                        type="password"
                                                        sx={{ mb: 2 }}
                                                        helperText={
                                                            errors.password ? (
                                                                <Typography
                                                                    variant="body2"
                                                                    color="error"
                                                                >
                                                                    {
                                                                        formMessages.password
                                                                    }
                                                                </Typography>
                                                            ) : (
                                                                ""
                                                            )
                                                        }
                                                        {...register(
                                                            "password",
                                                            {
                                                                minLength: 6,
                                                                required: true,
                                                            }
                                                        )}
                                                        color={
                                                            errors.password
                                                                ? "error"
                                                                : "primary"
                                                        }
                                                        required
                                                    />

                                                    <TextField
                                                        fullWidth
                                                        label="Repeat New Password"
                                                        type="password"
                                                        sx={{ mb: 2 }}
                                                        helperText={
                                                            errors.repeatPassword ? (
                                                                <Typography
                                                                    variant="body2"
                                                                    color="error"
                                                                >
                                                                    {
                                                                        formMessages.repeatPassword
                                                                    }
                                                                </Typography>
                                                            ) : (
                                                                ""
                                                            )
                                                        }
                                                        {...register(
                                                            "repeatPassword",
                                                            {
                                                                minLength: 6,
                                                                required: true,
                                                            }
                                                        )}
                                                        color={
                                                            errors.repeatPassword
                                                                ? "error"
                                                                : "primary"
                                                        }
                                                        required
                                                    />

                                                    {isPasswordMatching ===
                                                        false && (
                                                        <Typography variant="body1">
                                                            Password not
                                                            matching, please
                                                            double check or
                                                            re-type them.
                                                        </Typography>
                                                    )}
                                                    {/* {!isAlreadyConfirmed ? (
                                                <Typography
                                                    variant="h5"
                                                    textAlign="center"
                                                >
                                                    Well done, you have
                                                    succesfully confirmed your
                                                    email!
                                                </Typography>
                                            ) : (
                                                <Typography
                                                    variant="h5"
                                                    textAlign="center"
                                                >
                                                    Your email confirmation is
                                                    already done!
                                                </Typography>
                                            )}
                                            <Link href="/users/login">
                                                <Button
                                                    fullWidth
                                                    size="large"
                                                    variant="contained"
                                                    color="primary"
                                                    sx={{ mt: 8, mb: 2 }}
                                                >
                                                    Login
                                                </Button>
                                            </Link> */}

                                                    <Button
                                                        fullWidth
                                                        type="submit"
                                                        size="large"
                                                        variant="contained"
                                                        color="secondary"
                                                        disabled={
                                                            !isDirty || !isValid
                                                        }
                                                        sx={{ mt: 2, mb: 4 }}
                                                    >
                                                        Set New Password
                                                    </Button>
                                                </Box>
                                            ) : (
                                                <Box>
                                                    <Typography textAlign="center">
                                                        Sorry, but looks the
                                                        reset password link has
                                                        been expired! <br />
                                                        Please request another
                                                        (will be active for 1
                                                        hour).
                                                    </Typography>
                                                    <Button
                                                        fullWidth
                                                        size="large"
                                                        variant="contained"
                                                        color="primary"
                                                        sx={{ mt: 4, mb: 4 }}
                                                        onClick={() =>
                                                            resendPasswordReset()
                                                        }
                                                    >
                                                        Request Reset Password
                                                        Email
                                                    </Button>
                                                </Box>
                                            )}
                                        </Box>
                                    ) : (
                                        <Box>
                                            <Typography textAlign="center">
                                                Sorry, but the link is not
                                                valid.
                                                <br />
                                                Please request another (will be
                                                active for 1 hour).
                                            </Typography>
                                            <Button
                                                fullWidth
                                                size="large"
                                                variant="contained"
                                                color="primary"
                                                sx={{ mt: 4, mb: 4 }}
                                                onClick={() =>
                                                    resendPasswordReset()
                                                }
                                            >
                                                Request Reset Password Email
                                            </Button>
                                        </Box>
                                    )}
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Container>
            </IonContent>
        </>
    );
}
