import axios from "axios";
import { useRouter } from "next/router";
import { useState, FormEventHandler, useEffect } from "react";
import HeaderMain from "../../components/header/header-secondary";
// https://github.com/effiongcharles/login_auth/blob/master/src/pages/Login.tsx
import {
    IonButton,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonCard,
    IonPage,
    IonContent,
    IonAlert,
    IonIcon,
    IonToggle,
    IonCol,
    IonGrid,
    IonRow,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
} from "@ionic/react";

import authService from "../../services/auth.service";
import Link from "next/link";
import {
    Box,
    Button,
    Card,
    CardContent,
    Container,
    Divider,
    Grid,
    TextField,
    Typography,
} from "@mui/material";
import { useForm } from "react-hook-form";
import SnackbarComp from "../../components/common/snackbars";
import { getPublicData, putPublicData } from "../../services/data.service";

function validateEmail(email: string) {
    const re =
        /^((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))$/;
    return re.test(String(email).toLowerCase());
}

const emailPattern =
    /^((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))$/;
const API_URL = process.env.NEXT_PUBLIC_API_URL;
// const API_URL = "https://univ-s1.goldilab.com/api/";

export default function Login() {
    const router = useRouter();
    const {
        register,
        handleSubmit,
        control,
        setValue,
        getValues,
        formState: { isSubmitting, isDirty, isValid, errors },
    } = useForm({
        mode: "onChange",
    });

    const formMessages = {
        email: "Please enter a valid email address",
        password: "Please enter a valid password",
    };

    // Message handlers
    const [showSnackbar, setShowSnackbar] = useState(false);
    const [snackbarType, setSnackbarType] = useState("");
    const [snackbarMessage, setSnackbarMessage] = useState("");

    // Check token if exist, if yes redirect
    const checkToken = async () => {
        const isToken = await authService.checkToken();

        console.log("isToken");
        console.log(isToken);
        console.log(router.route);

        if (isToken && router.route === "/users/login") {
            router.push("/");
            // router.back();
        }
    };

    useEffect(() => {
        checkToken();
        setIsPasswordReset(false);
        setIsPasswordResetSent(false);
    }, []);

    async function onSubmit({ email, password }) {
        if (!email) {
            setSnackbarType("error");
            setSnackbarMessage("Please enter a valid email");
            setShowSnackbar(true);
            return;
        }

        if (validateEmail(email) === false) {
            setSnackbarType("error");
            setSnackbarMessage("The email address is invalid");
            setShowSnackbar(true);
            return;
        }

        // if (!password || password.length < 6) {
        if (!password) {
            setSnackbarType("error");
            setSnackbarMessage("Please enter your password");
            setShowSnackbar(true);

            return;
        }

        const dataBody = {
            // userId,
            email,
            password,
        };

        console.log("dataBody");
        console.log(dataBody);

        await authService.login(dataBody).then(
            async (res) => {
                console.log("login res");
                console.log(await res);

                await authService.getCurrentToken().then(async (userToken) => {
                    console.log("userToken");
                    console.log(await userToken);
                    await userToken;

                    if (await userToken) {
                        await authService.getCurrentUser().then(
                            async (user) => {
                                console.log("login getCurrentUser");
                                console.log(await user);

                                let areaSlug = "all";
                                if (await user.userArea) {
                                    areaSlug = await user.userArea.slug;
                                }
                                router.push(`/${areaSlug}`);
                            },
                            (err) => {
                                setSnackbarType("error");
                                setSnackbarMessage(err.response.data.message);
                                setShowSnackbar(true);
                            }
                        );
                    }
                });
            },
            async (err) => {
                console.log("err");
                console.log(await err);

                setSnackbarType("error");
                setSnackbarMessage(await err.response.data.message);
                setShowSnackbar(true);
            }
        );
    }

    const [isPasswordReset, setIsPasswordReset] = useState(false);
    const [isPasswordResetSent, setIsPasswordResetSent] = useState(false);
    async function onPasswordResetSubmit({ email }) {
        const dataBody = {
            // userId,
            email,
        };
        console.log(dataBody);

        await getPublicData(`users/reset/null/${dataBody.email}`).then(
            async (resp: any) => {
                console.log("getPublicData");
                console.log(resp);

                await putPublicData(`users/send-reset`, resp.id, {
                    id: resp.id,
                }).then((resp: any) => {
                    console.log("validateConfirmation putPublicData resp");
                    console.log("users/send-reset resp");
                    console.log(resp);
                });
                // setUser(resp);
                // validateConfirmation(resp);
            }
        );

        setIsPasswordResetSent(true);
    }

    return (
        <>
            {/* <HeaderMain props={{ title: "Login" }}></HeaderMain> */}

            {showSnackbar && (
                <SnackbarComp
                    type={snackbarType}
                    message={snackbarMessage}
                    // transition="TransitionUp"
                />
            )}

            <IonContent fullscreen className="ion-padding ion-text-start">
                {/* <Box
                    component="form"
                    noValidate
                    autoComplete="off"
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <Card>
                        <CardContent>
                            <TextField
                                fullWidth
                                label="Email"
                                type="text"
                                sx={{ mb: 2 }}
                                // onChange={(e) => setEmail(e.target.value)}
                                {...register("email", {
                                    pattern: emailPattern,
                                })}
                            />
                            <TextField
                                fullWidth
                                label="Password"
                                type="password"
                                sx={{ mb: 2 }}
                                // onChange={(e) => setPassword(e.target.value)}
                                {...register("password")}
                            />
                            <Button
                                fullWidth
                                type="submit"
                                size="large"
                                variant="contained"
                                color="primary"
                                disabled={formState.isSubmitting}
                            >
                                Login
                            </Button>
                            <Typography
                                component="p"
                                variant="body2"
                                sx={{
                                    mt: 4,
                                    display: "flex",
                                    justifyContent: "space-between",
                                }}
                            >
                                <Link
                                    href="/users/register"
                                    className="secondary-link ion-text-left"
                                >
                                    Create Account
                                </Link>
                                <Link
                                    href="/users/register"
                                    className="secondary-link ion-text-right"
                                >
                                    Forgotten Password
                                </Link>
                            </Typography>
                        </CardContent>
                    </Card>
                </Box> */}

                <Container>
                    <Grid
                        container
                        spacing={2}
                        className="full-height-420"
                        sx={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                        }}
                    >
                        <Grid
                            item
                            xs={12}
                            sm={8}
                            // sm-offset={2}
                            md={6}
                            lg={5}
                            // lg-offset={4}
                            xl={5}
                            // xl-offset={4}
                            // sx={{
                            //     display: "flex",
                            //     alignItems: "center",
                            //     justifyContent: "center",
                            // }}
                        >
                            <Card>
                                {!isPasswordReset ? (
                                    <CardContent>
                                        <Typography variant="h4" sx={{ mb: 4 }}>
                                            Login to your account
                                        </Typography>

                                        <Box
                                            component="form"
                                            noValidate
                                            autoComplete="on"
                                            onSubmit={handleSubmit(onSubmit)}
                                        >
                                            <TextField
                                                fullWidth
                                                label="Email"
                                                type="email"
                                                sx={{ mb: 2 }}
                                                helperText={
                                                    errors.email ? (
                                                        <Typography
                                                            variant="body2"
                                                            color="error"
                                                        >
                                                            {formMessages.email}
                                                        </Typography>
                                                    ) : (
                                                        ""
                                                    )
                                                }
                                                {...register("email", {
                                                    required: true,
                                                    pattern: emailPattern,
                                                })}
                                                color={
                                                    errors.email
                                                        ? "error"
                                                        : "primary"
                                                }
                                                required
                                            />

                                            <TextField
                                                fullWidth
                                                label="Password"
                                                type="password"
                                                sx={{ mb: 2 }}
                                                helperText={
                                                    errors.password ? (
                                                        <Typography
                                                            variant="body2"
                                                            color="error"
                                                        >
                                                            {
                                                                formMessages.password
                                                            }
                                                        </Typography>
                                                    ) : (
                                                        ""
                                                    )
                                                }
                                                {...register("password", {
                                                    required: true,
                                                })}
                                                color={
                                                    errors.password
                                                        ? "error"
                                                        : "primary"
                                                }
                                                required
                                            />

                                            <Button
                                                fullWidth
                                                type="submit"
                                                size="large"
                                                variant="contained"
                                                color="secondary"
                                                disabled={!isDirty || !isValid}
                                            >
                                                Login
                                            </Button>
                                            <Typography
                                                component="p"
                                                variant="body2"
                                                sx={{
                                                    mt: 2,
                                                    display: "flex",
                                                    justifyContent:
                                                        "space-between",
                                                }}
                                            >
                                                <Button
                                                    color="secondary"
                                                    onClick={() =>
                                                        setIsPasswordReset(true)
                                                    }
                                                >
                                                    Forgotten Password
                                                </Button>
                                                {/* <Link
                                                    href="/users/register"
                                                    className="secondary-link ion-text-right"
                                                >
                                                    Forgotten Password
                                                </Link> */}
                                            </Typography>
                                        </Box>

                                        <Box>
                                            <Divider
                                                // variant="center"
                                                sx={{ mt: 4, mb: 4 }}
                                            />
                                        </Box>

                                        <Box sx={{ mb: 2 }}>
                                            <Typography
                                                variant="h6"
                                                sx={{ mb: 2 }}
                                            >
                                                Not registered yet?
                                            </Typography>
                                            <Link href="/users/register">
                                                <Button
                                                    fullWidth
                                                    size="large"
                                                    variant="outlined"
                                                    color="secondary"
                                                >
                                                    Create Account
                                                </Button>
                                            </Link>
                                        </Box>
                                    </CardContent>
                                ) : (
                                    <CardContent>
                                        <Typography variant="h4" sx={{ mb: 4 }}>
                                            Reset your Password
                                        </Typography>

                                        {!isPasswordResetSent ? (
                                            <Box
                                                component="form"
                                                noValidate
                                                autoComplete="on"
                                                onSubmit={handleSubmit(
                                                    onPasswordResetSubmit
                                                )}
                                            >
                                                <Typography variant="body1">
                                                    You will receive an email to
                                                    the given email address,
                                                    please follow the
                                                    instructions in that email
                                                    to reset your password.
                                                </Typography>

                                                <TextField
                                                    fullWidth
                                                    label="Email"
                                                    type="email"
                                                    sx={{ mb: 4, mt: 4 }}
                                                    helperText={
                                                        errors.email ? (
                                                            <Typography
                                                                variant="body2"
                                                                color="error"
                                                            >
                                                                {
                                                                    formMessages.email
                                                                }
                                                            </Typography>
                                                        ) : (
                                                            ""
                                                        )
                                                    }
                                                    {...register("email", {
                                                        required: true,
                                                        pattern: emailPattern,
                                                    })}
                                                    color={
                                                        errors.email
                                                            ? "error"
                                                            : "primary"
                                                    }
                                                    required
                                                />

                                                <Button
                                                    fullWidth
                                                    type="submit"
                                                    size="large"
                                                    variant="contained"
                                                    color="secondary"
                                                    disabled={
                                                        !isDirty || !isValid
                                                    }
                                                    sx={{ mb: 2 }}
                                                >
                                                    Reset Password
                                                </Button>
                                            </Box>
                                        ) : (
                                            <Box>
                                                <Typography
                                                    variant="body1"
                                                    sx={{
                                                        display: "block",
                                                        textAlign: "center",
                                                        mt: 8,
                                                        mb: 8,
                                                    }}
                                                >
                                                    Password Reset Email sent.
                                                    <br />
                                                    Please check your email
                                                    inbox or spam folder to
                                                    continue!
                                                </Typography>

                                                {/* <Link href="/users/login">
                                                    <Button
                                                        fullWidth
                                                        size="large"
                                                        variant="contained"
                                                        color="primary"
                                                        sx={{ mt: 8, mb: 2 }}
                                                    >
                                                        Login
                                                    </Button>
                                                </Link> */}
                                            </Box>
                                        )}
                                    </CardContent>
                                )}
                            </Card>
                        </Grid>
                    </Grid>
                </Container>
            </IonContent>
        </>
    );
}
