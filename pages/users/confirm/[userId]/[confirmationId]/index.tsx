import { useRouter } from "next/router";
import {
    getPublicData,
    putPublicData,
} from "../../../../../services/data.service";
import Link from "next/link";
import {
    Box,
    Button,
    Card,
    CardContent,
    Container,
    Grid,
    Typography,
} from "@mui/material";
import { useState, useEffect } from "react";
import { IonContent } from "@ionic/react";

export default function ConfirmEmailPage() {
    const router = useRouter();
    const { query } = router || {};
    // const { area = "wallingford" } = query || {};
    console.log("ConfirmEmailPage queries");
    console.log(query);

    const userId = query.userId;
    // const userEmail = query.userEmail;
    const confirmationId = query.confirmationId;

    const [user, setUser] = useState<any>({});
    const [isExpired, setIsExpired] = useState<boolean>(false);
    const [isAlreadyConfirmed, setIsAlreadyConfirmed] =
        useState<boolean>(false);

    useEffect(() => {
        (async () => {
            console.log("useEffect userId");
            console.log(userId);
            if (userId) {
                // const userConfirm =
                await getPublicData(`users/confirm/${userId}`).then(
                    (resp: any) => {
                        console.log("getPublicData");
                        console.log(resp);
                        setUser(resp);
                        validateConfirmation(resp);
                    }
                );
                // setUser(await userConfirm);
                // validateConfirmation();
            }
        })();
    }, [userId]);

    const validateConfirmation = async (user: any) => {
        if (user.confirmationId === confirmationId) {
            console.log("confirmationId matching!");

            if (new Date(user.confirmationExpires) > new Date()) {
                if (!user.isConfirmed) {
                    console.log("validateConfirmation OK!");
                    await putPublicData(`users/confirm`, user.id, {
                        id: user.id,
                        isConfirmed: true,
                    }).then((resp: any) => {
                        console.log("validateConfirmation putPublicData resp");
                        console.log(resp);
                    });
                } else {
                    setIsAlreadyConfirmed(true);
                }
            } else {
                console.log("validateConfirmation expired!");
                console.log(new Date(user.confirmationExpires));
                console.log(new Date());
                setIsExpired(true);
            }
        }
    };

    const resendConfirmation = async () => {
        await putPublicData(`users/resend-confirm`, user.id, {
            id: user.id,
        }).then((resp: any) => {
            console.log("resendConfirmation putPublicData resp");
            console.log(resp);
        });
    };

    console.log("ConfirmEmailPage user");
    console.log(user);
    console.log(confirmationId);

    return (
        <>
            <IonContent fullscreen className="ion-padding ion-text-start">
                <Container>
                    <Grid
                        container
                        spacing={2}
                        className="full-height-420"
                        sx={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                        }}
                    >
                        <Grid item xs={12} sm={8} md={6} lg={5} xl={5}>
                            <Card>
                                <CardContent>
                                    <Typography variant="h4" sx={{ mb: 8 }}>
                                        Account confirmation
                                    </Typography>

                                    {/* <Typography variant="body1">
                                        {new Date(
                                            user.confirmationExpires
                                        ).getHours()}{" "}
                                        vs currrent {new Date().getHours()}
                                    </Typography>
                                    <Typography variant="body1">
                                        {user.confirmationId} vs query
                                        confirmationId {confirmationId}
                                    </Typography> */}

                                    {!isExpired ? (
                                        <Box>
                                            {!isAlreadyConfirmed ? (
                                                <Typography
                                                    variant="h5"
                                                    textAlign="center"
                                                >
                                                    Well done, you have
                                                    succesfully confirmed your
                                                    email!
                                                </Typography>
                                            ) : (
                                                <Typography
                                                    variant="h5"
                                                    textAlign="center"
                                                >
                                                    Your email confirmation is
                                                    already done!
                                                </Typography>
                                            )}
                                            <Link href="/users/login">
                                                <Button
                                                    fullWidth
                                                    size="large"
                                                    variant="contained"
                                                    color="primary"
                                                    sx={{ mt: 8, mb: 2 }}
                                                >
                                                    Login
                                                </Button>
                                            </Link>
                                        </Box>
                                    ) : (
                                        <Box>
                                            <Typography textAlign="center">
                                                Sorry, but looks the
                                                confirmation has been expired!
                                                Confirmation is active for 2
                                                hours.
                                            </Typography>
                                            <Button
                                                fullWidth
                                                size="large"
                                                variant="contained"
                                                color="primary"
                                                sx={{ mt: 4 }}
                                                onClick={() =>
                                                    resendConfirmation()
                                                }
                                            >
                                                Resend Confirmation Email
                                            </Button>
                                        </Box>
                                    )}
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Container>
            </IonContent>
        </>
    );
}
