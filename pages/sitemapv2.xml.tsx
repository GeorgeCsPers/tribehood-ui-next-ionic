import axios from "axios";
import dayjs from "dayjs";
import { ServerResponse } from "http";
import { GetServerSideProps } from "next";
import categoryTypeFormatter from "../components/hooks/category-type-formatter";
import { resolve } from "styled-jsx/css";
const fs = require("fs");

// https://www.youtube.com/watch?v=khYC98faSrg
// https://www.skies.dev/dynamic-sitemap
export default function Sitemap() {
    return null;
}

async function getPages() {
    const config = {
        headers: {
            "Cache-Control": "public, s-maxage=10, stale-while-revalidate=59",
            "Content-Type": "application/json",
        },
    };

    let resData: any = [];
    const apiURL =
        "https://tribehood-s1.goldilab.com/api/feeds?take=1000&skip=0&sortBy=createdAt&sortOrder=desc&area=all"; // process.env.NEXT_PUBLIC_API_URL
    await axios.get(apiURL, config).then(async (res) => {
        console.log("getData");
        console.log(res.data);
        resData = await res.data;
    });

    // let feeds = [];
    const feeds = await resData.data;
    // console.log("feeds");
    // console.log(feeds);

    return feeds;
}

// export const getServerSideProps: GetServerSideProps<{}> = async ({ res }) => {
// export const getServerSideProps = async ({ res }) => {
export const getStaticProps = async (res) => {
    getSitemap(await getPages(), res);

    // res.setHeader("Content-Type", "text/xml");
    // res.write(getSitemap(await getPages()));
    // res.end();
    return {
        props: {},
    };
};

// function getSitemap(pages: any, res: ServerResponse) {
function getSitemap(pages: any, res: any) {
    console.log("res");
    console.log(res);
    console.log("pages");
    console.log(pages.length);
    // const hostURL = process.env.NEXT_PUBLIC_URL;
    const hostURL = "https://tribehood.co.uk/";
    const xmlData = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
    <url>
          <loc>https://tribehood.co.uk/wallingford/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>1</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/wallingford/events/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/wallingford/activities/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/wallingford/public-notices/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/wallingford/places/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/wallingford/services/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/wallingford/places-to-visit/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>

    <url>
          <loc>https://tribehood.co.uk/didcot/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>1</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/didcot/events/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/didcot/activities/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/didcot/public-notices/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/didcot/places/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/didcot/services/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>
    <url>
          <loc>https://tribehood.co.uk/didcot/places-to-visit/</loc>
          <lastmod>${dayjs(new Date()).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.9</priority>
    </url>

    ${pages
        .map(
            (page) => `<url>
          <loc>${
              hostURL +
              page.areas[0]?.slug +
              "/" +
              categoryTypeFormatter(page.categoryTypes[0]?.name, "slug") +
              "/" +
              page.slug
          }</loc>
          <lastmod>${dayjs(page.updatedAt).format("YYYY-MM-DD")}</lastmod>
          <changefreq>daily</changefreq>
          <priority>0.8</priority>
          </url>`
        )
        .join("")}
          </urlset>
          `;

    console.log("xmlData");
    console.log(xmlData);

    fs.writeFileSync("public/sitemap.xml", xmlData);

    // res.setHeader("Content-Type", "text/xml");
    // res.write(xmlData);
    // res.end();
}
