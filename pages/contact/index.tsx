import {
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonContent,
} from "@ionic/react";
import HeadShared from "../../components/head-shared";
import HeaderSecondary from "../../components/header/header-secondary";
import Link from "next/link";
import { Box, Container, Typography } from "@mui/material";
import LeftSide from "../../components/layout/left-side";
import RightSide from "../../components/layout/right-side";

export default function ContactPage() {
    return (
        <>
            <HeadShared title="Contact" description="Contact us"></HeadShared>

            <HeaderSecondary title="Contact"></HeaderSecondary>

            <IonContent>
                <Container sx={{ mt: 4, pb: 4 }}>
                    <IonRow>
                        <IonCol
                            size-lg="3"
                            size-xl="3"
                            className="ion-hide-lg-down pos-rel"
                        >
                            <Box
                                sx={{ display: "flex" }}
                                className="fixed left-side"
                            >
                                <LeftSide
                                // fixedClass={fixedClass}
                                // handleAreaChange={handleAreaChange}
                                />
                            </Box>
                        </IonCol>

                        <IonCol
                            size-xs="12"
                            size-lg="6"
                            size-xl="6"
                            className="pos-rel"
                        >
                            <IonCard>
                                <IonCardHeader>
                                    <IonCardTitle>Get in Touch</IonCardTitle>
                                </IonCardHeader>
                                <IonCardContent>
                                    <Typography variant="body1" sx={{ mt: 2 }}>
                                        For any project related enquiry or
                                        information please write us to{" "}
                                        <Link href="mailto:info@tribehood.co.uk">
                                            info@tribehood.co.uk
                                        </Link>
                                        !
                                    </Typography>
                                </IonCardContent>
                            </IonCard>
                        </IonCol>

                        <IonCol
                            size-lg="3"
                            size-xl="3"
                            className="ion-hide-lg-down pos-rel"
                        >
                            <Box
                                sx={{ display: "flex" }}
                                className="fixed right-side"
                            >
                                <RightSide />
                            </Box>
                        </IonCol>
                    </IonRow>
                </Container>
            </IonContent>
        </>
    );
}
