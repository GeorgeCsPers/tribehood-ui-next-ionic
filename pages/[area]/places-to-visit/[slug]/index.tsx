import { useRouter } from "next/router";
import HeadShared from "../../../../components/head-shared";
import HeaderSecondary from "../../../../components/header/header-secondary";
import FullscreenModal from "../../../../components/modals/modal-fullscreen";
import { useEffect, useState } from "react";
import authService from "../../../../services/auth.service";
import { IonContent, IonItem } from "@ionic/react";
import {
    Container,
    Button,
    Grid,
    Card,
    CardContent,
    Box,
    CardHeader,
    CardMedia,
    Typography,
} from "@mui/material";
import { getData, getPublicData } from "../../../../services/data.service";
import Loading from "../../../../components/common/loading";
import isAllowedToManageChecker from "../../../../components/hooks/is-allowed-to-manage";
import PlacesToVisitForm from "../../../../components/forms/places-to-visit-form";
import { pascalCaseFormatter } from "../../../../components/hooks/text-formatter";

// Assets URL
const serverUrl = process.env.NEXT_PUBLIC_ASSETS_URL;

export default function PlaceToVisitPage() {
    const router = useRouter();
    const { query } = router || {};
    const { area = query.area || "all" } = query || {};
    // const area = query.area || "all";
    const { slug } = query || {};
    console.log("PlaceToVisitPage query.area");
    console.log(query.area);
    console.log(area);
    console.log(slug);

    const [isLoading, setIsLoading] = useState<boolean>(true);

    // - - - - - - - -
    // Check if user logged in and token is valid
    // - - - - - - - -
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        // if (!isToken) {
        //     router.push("/users/login");
        // }
    };

    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
    };

    // - - - - - - -
    // GET APIs Data
    // - - - - - - -
    const [placeToVisit, setPlaceToVisit] = useState<any>({});
    const [isAllowedToManage, setIsAllowedToManage] = useState<boolean>(false);
    const getAllData = async (slug) => {
        const placeToVisitRawData = await getPublicData(
            `places-to-visit/${slug}`
        );
        setPlaceToVisit(placeToVisitRawData);

        // Check if user allowed to manage
        if (isAuth) {
            const isAllowed = isAllowedToManageChecker(
                userDetails,
                placeToVisitRawData
            );
            setIsAllowedToManage(isAllowed);
        }
        console.log("isAllowedToManage");
        console.log(isAllowedToManage);

        setIsLoading(false);
    };

    useEffect(() => {
        if (slug && area) {
            (async () => {
                await checkToken();
                await checkUser();
                // if (isAuth) {
                await getAllData(slug);
                // }
            })();
        }
    }, [area, isAuth, slug]);

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    // - - - - - - - - -
    // Image URL adjustment
    // - - - - - - - - -
    let imageUrl = "/assets/images/TribeHood-better-connected-community.webp";
    if (
        placeToVisit?.featuredImageUrl &&
        placeToVisit?.featuredImageUrl.indexOf("http") > -1
    ) {
        imageUrl = placeToVisit?.featuredImageUrl;
    } else if (placeToVisit?.featuredImageUrl) {
        imageUrl = serverUrl + placeToVisit?.featuredImageUrl;
    }

    console.log("Single Place to Visit");
    console.log(placeToVisit);

    return (
        <>
            {/* {!isLoading && (
                <HeadShared
                    title={
                        placeToVisit.name +
                        " - Places to Visit in the " +
                        // placeToVisit.address[0].city +
                        // ", " +
                        (placeToVisit.areas[0].name !== "all"
                            ? pascalCaseFormatter(placeToVisit.areas[0].name) +
                              " "
                            : "") +
                        "area"
                    }
                    description={
                        placeToVisit.shortDescription +
                        " || Places to Visit in the " +
                        (placeToVisit.areas[0].name !== "all"
                            ? pascalCaseFormatter(placeToVisit.areas[0].name) +
                              " "
                            : "") +
                        "area"
                    }
                    image={imageUrl}
                ></HeadShared>
            )} */}

            {isAuth && isAllowedToManage && (
                <FullscreenModal
                    title={modalTitle}
                    showModal={showModal}
                    setShowModal={setShowModal}
                >
                    {!isLoading && (
                        <PlacesToVisitForm
                            recordForEdit={recordForEdit}
                            userId={userDetails?.id}
                            userDetails={userDetails}
                        />
                    )}
                </FullscreenModal>
            )}

            <IonContent>
                <HeaderSecondary
                    addNew="placesToVisit"
                    item={placeToVisit}
                    isBack={true}
                    isSingleView={true}
                    isAuth={isAuth}
                    userDetails={userDetails}
                    image={imageUrl}
                ></HeaderSecondary>

                {!isLoading ? (
                    <Box sx={{ mt: 4, pb: 4 }}>
                        <Container sx={{ mt: 4, pb: 4 }}>
                            <Card sx={{ mb: 4 }}>
                                <CardHeader
                                    title="Details"
                                    // subheader=""
                                    action={
                                        <Box>
                                            {isAllowedToManage && (
                                                <Grid
                                                    item
                                                    sx={{
                                                        display: "flex",
                                                        alignItems: "center",
                                                        justifyContent: "end",
                                                    }}
                                                >
                                                    <Button
                                                        sx={{
                                                            display: "flex",
                                                            mr: 2,
                                                        }}
                                                        variant="contained"
                                                        size="small"
                                                        color="primary"
                                                        onClick={() => {
                                                            setShowModal(true);
                                                            setRecordForEdit(
                                                                placeToVisit
                                                            );
                                                            setModalTitle(
                                                                "Edit Place to Visit"
                                                            );
                                                        }}
                                                    >
                                                        Edit
                                                    </Button>
                                                    <Button
                                                        sx={{
                                                            display: "flex",
                                                        }}
                                                        variant="contained"
                                                        size="small"
                                                        color="primary"
                                                        onClick={() => {
                                                            setShowModal(true);
                                                            setRecordForEdit(
                                                                null
                                                            );
                                                            setModalTitle(
                                                                "Add New Place to Visit"
                                                            );
                                                        }}
                                                    >
                                                        Add New
                                                    </Button>
                                                </Grid>
                                            )}
                                        </Box>
                                    }
                                ></CardHeader>

                                <CardContent sx={{ pt: 0 }}>
                                    {/* {isAuth ? ( */}

                                    <Grid container spacing={2}>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={4}
                                        >
                                            <CardMedia
                                                component="img"
                                                // height="240"
                                                image={imageUrl}
                                                alt={placeToVisit?.name}
                                            />
                                        </Grid>
                                        {!isLoading ? (
                                            <Grid
                                                item
                                                xs={12}
                                                sm={12}
                                                md={6}
                                                lg={8}
                                            >
                                                <Grid
                                                    container
                                                    spacing={2}
                                                    sx={{
                                                        width: "100%",
                                                        display: "flex",
                                                        mb: 6,
                                                    }}
                                                >
                                                    <Grid item xs={12} sm={6}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Event Name
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {placeToVisit.name}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Category
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {placeToVisit
                                                                ?.categories
                                                                .length > 0 ? (
                                                                placeToVisit.categories.map(
                                                                    (
                                                                        item,
                                                                        i
                                                                    ) => (
                                                                        <span
                                                                            key={
                                                                                i
                                                                            }
                                                                        >
                                                                            {
                                                                                item.name
                                                                            }
                                                                        </span>
                                                                    )
                                                                )
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={12}
                                                        md={12}
                                                        lg={12}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Address:
                                                        </Typography>
                                                        {placeToVisit?.address ? (
                                                            <Typography variant="body1">
                                                                {placeToVisit
                                                                    ?.address[0]
                                                                    ?.addressLine1 && (
                                                                    <span>
                                                                        {
                                                                            placeToVisit
                                                                                ?.address[0]
                                                                                ?.addressLine1
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {placeToVisit
                                                                    ?.address[0]
                                                                    ?.addressLine2 && (
                                                                    <span>
                                                                        {
                                                                            placeToVisit
                                                                                ?.address[0]
                                                                                ?.addressLine2
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {placeToVisit
                                                                    ?.address[0]
                                                                    ?.addressLine3 && (
                                                                    <span>
                                                                        {
                                                                            placeToVisit
                                                                                ?.address[0]
                                                                                ?.addressLine3
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {placeToVisit
                                                                    ?.address[0]
                                                                    ?.city && (
                                                                    <span>
                                                                        {
                                                                            placeToVisit
                                                                                ?.address[0]
                                                                                ?.city
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {placeToVisit
                                                                    ?.address[0]
                                                                    ?.postcode && (
                                                                    <span>
                                                                        {
                                                                            placeToVisit
                                                                                ?.address[0]
                                                                                ?.postcode
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {placeToVisit
                                                                    ?.address[0]
                                                                    ?.county && (
                                                                    <span>
                                                                        {
                                                                            placeToVisit
                                                                                ?.address[0]
                                                                                ?.county
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {placeToVisit
                                                                    ?.address[0]
                                                                    ?.region && (
                                                                    <span>
                                                                        {
                                                                            placeToVisit
                                                                                ?.address[0]
                                                                                ?.region
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}
                                                                {placeToVisit
                                                                    ?.address[0]
                                                                    ?.country
                                                                    .length >
                                                                    0 && (
                                                                    <span>
                                                                        {
                                                                            placeToVisit
                                                                                ?.address[0]
                                                                                ?.country[0]
                                                                                .name
                                                                        }
                                                                    </span>
                                                                )}
                                                            </Typography>
                                                        ) : (
                                                            <span>N/A</span>
                                                        )}
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Short Description
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {placeToVisit.shortDescription ? (
                                                                <span>
                                                                    {
                                                                        placeToVisit.shortDescription
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Description
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {placeToVisit.description ? (
                                                                <span>
                                                                    {
                                                                        placeToVisit.description
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={6}
                                                        lg={6}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Created By:
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {
                                                                placeToVisit
                                                                    .createdBy
                                                                    ?.firstName
                                                            }{" "}
                                                            {
                                                                placeToVisit
                                                                    .createdBy
                                                                    ?.lastName
                                                            }
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={6}
                                                        lg={6}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Updated By:
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {placeToVisit
                                                                .updatedBy
                                                                ?.firstName ? (
                                                                <span>
                                                                    {
                                                                        placeToVisit
                                                                            .updatedBy
                                                                            ?.firstName
                                                                    }{" "}
                                                                    {
                                                                        placeToVisit
                                                                            .updatedBy
                                                                            ?.lastName
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        ) : (
                                            <Loading />
                                        )}
                                    </Grid>
                                    {/* ) : (
                                        <LoginOrRegister />
                                    )} */}
                                </CardContent>
                            </Card>

                            {/* Contact Details */}
                            <Card>
                                <CardHeader title="Contact"></CardHeader>
                                <CardContent sx={{ pt: 0 }}>
                                    <Grid container>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Contact Person:
                                            </Typography>
                                            <Typography variant="body1">
                                                {placeToVisit.contactPersonName ? (
                                                    <span>
                                                        {
                                                            placeToVisit.contactPersonName
                                                        }
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Email:
                                            </Typography>
                                            <Typography variant="body1">
                                                {placeToVisit.contactEmail ? (
                                                    <span>
                                                        {
                                                            placeToVisit.contactEmail
                                                        }
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Mobile:
                                            </Typography>
                                            <Typography variant="body1">
                                                {placeToVisit.contactMobile ? (
                                                    <span>
                                                        {
                                                            placeToVisit.contactMobile
                                                        }
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Phone:
                                            </Typography>
                                            <Typography variant="body1">
                                                {placeToVisit.contactPhone ? (
                                                    <span>
                                                        {
                                                            placeToVisit.contactPhone
                                                        }
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={6}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                URL 1:
                                            </Typography>
                                            <Typography variant="body1"></Typography>
                                            {placeToVisit.url1 ? (
                                                <span>{placeToVisit.url1}</span>
                                            ) : (
                                                <span>N/A</span>
                                            )}
                                        </Grid>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={6}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                URL 2:
                                            </Typography>
                                            <Typography variant="body1"></Typography>
                                            {placeToVisit.url2 ? (
                                                <span>{placeToVisit.url2}</span>
                                            ) : (
                                                <span>N/A</span>
                                            )}
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Container>
                    </Box>
                ) : (
                    <Loading />
                )}
            </IonContent>
        </>
    );
}

export const getServerSideProps = async (context) => {
    const { slug, area } = context.params;
    console.log("place to visit getServerSideProps slug");
    console.log(slug);
    console.log(area);

    const apiUrlParams = `places-to-visit/${slug}`;
    const rawData = await getPublicData(apiUrlParams);

    console.log("place to visit getServerSideProps data");
    console.log(rawData);

    const headMetaTitle = `${rawData.name} - Place to Visit in ${
        rawData.address[0].city
    }, ${
        rawData.areas[0].name !== "all"
            ? pascalCaseFormatter(rawData.areas[0].name)
            : ""
    } area`;

    const headMetaDescription = `${
        rawData.shortDescription
    } || Place to Visit in the ${
        rawData.areas[0].name !== "all"
            ? pascalCaseFormatter(rawData.areas[0].name)
            : ""
    } area`;
    const headMetaImage = rawData?.featuredImageUrl;
    const headMetaType = "article";

    return {
        props: {
            // data: rawData,
            head: {
                title: headMetaTitle,
                description: headMetaDescription,
                image: headMetaImage,
                type: headMetaType,
            },
        },
    };
};
