import { useRouter } from "next/router";
import HeadShared from "../../../../components/head-shared";
import HeaderSecondary from "../../../../components/header/header-secondary";
import FullscreenModal from "../../../../components/modals/modal-fullscreen";
import { useEffect, useState } from "react";
import authService from "../../../../services/auth.service";
import { IonContent, IonItem } from "@ionic/react";
import {
    Container,
    Button,
    Grid,
    Card,
    CardContent,
    Box,
    CardMedia,
    Typography,
    CardHeader,
} from "@mui/material";
import { getData, getPublicData } from "../../../../services/data.service";
import Loading from "../../../../components/common/loading";
import LoginOrRegister from "../../../../components/login-or-register";
import ServicesForm from "../../../../components/forms/service-form";
import isAllowedToManageChecker from "../../../../components/hooks/is-allowed-to-manage";
import { pascalCaseFormatter } from "../../../../components/hooks/text-formatter";

// Assets URL
const serverUrl = process.env.NEXT_PUBLIC_ASSETS_URL;

export default function ServicePage() {
    const router = useRouter();
    const { query } = router || {};
    const { area = query.area || "all" } = query || {};
    // const area = query.area || "all";
    const { slug } = query || {};
    console.log("ServicePage query.area");
    console.log(query.area);
    console.log(area);
    console.log(slug);

    const [isLoading, setIsLoading] = useState<boolean>(true);

    // - - - - - - - -
    // Check if user logged in and token is valid
    // - - - - - - - -
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        // if (!isToken) {
        //     router.push("/users/login");
        // }
    };

    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
    };

    // - - - - - - -
    // GET APIs Data
    // - - - - - - -
    const [service, setService] = useState<any>({});
    const [isAllowedToManage, setIsAllowedToManage] = useState<boolean>(false);
    const getAllData = async (slug) => {
        // const servicesRawData = await getPublicData(`services/${slug}`);
        const servicesRawData = await getData(`services/${slug}`);
        setService(servicesRawData);

        // Check if user allowed to manage
        if (isAuth) {
            const isAllowed = isAllowedToManageChecker(
                userDetails,
                servicesRawData
            );
            setIsAllowedToManage(isAllowed);
        }
        console.log("isAllowedToManage");
        console.log(isAllowedToManage);

        setIsLoading(false);
    };

    useEffect(() => {
        if (slug && area) {
            (async () => {
                await checkToken();
                await checkUser();
                // if (isAuth) {
                await getAllData(slug);
                // }
            })();
        }
    }, [area, isAuth, slug]);

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    // - - - - - - - - -
    // Image URL adjustment
    // - - - - - - - - -
    let imageUrl = "/assets/images/TribeHood-better-connected-community.webp";
    if (
        service?.featuredImageUrl &&
        service?.featuredImageUrl.indexOf("http") > -1
    ) {
        imageUrl = service?.featuredImageUrl;
    } else if (service?.featuredImageUrl) {
        imageUrl = serverUrl + service?.featuredImageUrl;
    }

    console.log("Single Service");
    console.log(service);

    return (
        <>
            {/* {!isLoading && (
                <HeadShared
                    title={
                        service.name +
                        " - Service in " +
                        service.address[0].city +
                        ", " +
                        (service.areas[0].name !== "all"
                            ? pascalCaseFormatter(service.areas[0].name)
                            : "") +
                        " area"
                    }
                    description={
                        service.shortDescription +
                        " || Service in the " +
                        (service.areas[0].name !== "all"
                            ? pascalCaseFormatter(service.areas[0].name) + " "
                            : "") +
                        "area"
                    }
                    image={imageUrl}
                ></HeadShared>
            )} */}

            {isAuth && isAllowedToManage && (
                <FullscreenModal
                    title={modalTitle}
                    showModal={showModal}
                    setShowModal={setShowModal}
                >
                    {!isLoading && (
                        <ServicesForm
                            recordForEdit={recordForEdit}
                            userId={userDetails?.id}
                            userDetails={userDetails}
                        />
                    )}
                </FullscreenModal>
            )}

            <IonContent>
                <HeaderSecondary
                    addNew="service"
                    item={service}
                    isBack={true}
                    isSingleView={true}
                    isAuth={isAuth}
                    userDetails={userDetails}
                    image={imageUrl}
                ></HeaderSecondary>

                {!isLoading ? (
                    <Box sx={{ mt: 4, pb: 4 }}>
                        {/* <Box className="single-view-header">
                            <CardMedia
                                component="img"
                                height="240"
                                image={imageUrl}
                                alt={service?.name}
                            />
                            <Container sx={{ mt: 4, pb: 4 }}>
                                <Box className="single-view-header-content">
                                    {service?.name}
                                </Box>
                            </Container>
                        </Box> */}

                        <Container sx={{ mt: 4, pb: 4 }}>
                            <Card sx={{ mb: 4 }}>
                                <CardHeader
                                    title="Details"
                                    // subheader=""
                                    action={
                                        <Box>
                                            {isAllowedToManage && (
                                                <Grid
                                                    item
                                                    sx={{
                                                        display: "flex",
                                                        alignItems: "center",
                                                        justifyContent: "end",
                                                    }}
                                                >
                                                    <Button
                                                        sx={{
                                                            display: "flex",
                                                            mr: 2,
                                                        }}
                                                        variant="contained"
                                                        size="small"
                                                        color="primary"
                                                        onClick={() => {
                                                            setShowModal(true);
                                                            setRecordForEdit(
                                                                service
                                                            );
                                                            setModalTitle(
                                                                "Edit Service"
                                                            );
                                                        }}
                                                    >
                                                        Edit
                                                    </Button>
                                                    <Button
                                                        sx={{
                                                            display: "flex",
                                                        }}
                                                        variant="contained"
                                                        size="small"
                                                        color="primary"
                                                        onClick={() => {
                                                            setShowModal(true);
                                                            setRecordForEdit(
                                                                null
                                                            );
                                                            setModalTitle(
                                                                "Add New Service"
                                                            );
                                                        }}
                                                    >
                                                        Add New
                                                    </Button>
                                                </Grid>
                                            )}
                                        </Box>
                                    }
                                ></CardHeader>

                                <CardContent sx={{ pt: 0 }}>
                                    {/* {isAuth ? ( */}

                                    <Grid container spacing={2}>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={4}
                                        >
                                            <CardMedia
                                                component="img"
                                                // height="240"
                                                image={imageUrl}
                                                alt={service?.name}
                                            />
                                        </Grid>
                                        {!isLoading ? (
                                            <Grid
                                                item
                                                xs={12}
                                                sm={12}
                                                md={6}
                                                lg={8}
                                            >
                                                <Grid
                                                    container
                                                    spacing={2}
                                                    sx={{
                                                        width: "100%",
                                                        display: "flex",
                                                        mb: 6,
                                                    }}
                                                >
                                                    <Grid item xs={12} sm={6}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Service Name
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {service.name}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Category
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {service?.categories
                                                                .length > 0 ? (
                                                                service.categories.map(
                                                                    (
                                                                        item,
                                                                        i
                                                                    ) => (
                                                                        <span
                                                                            key={
                                                                                i
                                                                            }
                                                                        >
                                                                            {
                                                                                item.name
                                                                            }
                                                                        </span>
                                                                    )
                                                                )
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={12}
                                                        md={12}
                                                        lg={12}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Address:
                                                        </Typography>
                                                        {service?.address
                                                            .length > 0 ? (
                                                            <Typography variant="body1">
                                                                {service
                                                                    ?.address[0]
                                                                    ?.addressLine1 && (
                                                                    <span>
                                                                        {
                                                                            service
                                                                                ?.address[0]
                                                                                ?.addressLine1
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {service
                                                                    ?.address[0]
                                                                    ?.addressLine2 && (
                                                                    <span>
                                                                        {
                                                                            service
                                                                                ?.address[0]
                                                                                ?.addressLine2
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {service
                                                                    ?.address[0]
                                                                    ?.addressLine3 && (
                                                                    <span>
                                                                        {
                                                                            service
                                                                                ?.address[0]
                                                                                ?.addressLine3
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {service
                                                                    ?.address[0]
                                                                    ?.city && (
                                                                    <span>
                                                                        {
                                                                            service
                                                                                ?.address[0]
                                                                                ?.city
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {service
                                                                    ?.address[0]
                                                                    ?.postcode && (
                                                                    <span>
                                                                        {
                                                                            service
                                                                                ?.address[0]
                                                                                ?.postcode
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {service
                                                                    ?.address[0]
                                                                    ?.county && (
                                                                    <span>
                                                                        {
                                                                            service
                                                                                ?.address[0]
                                                                                ?.county
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {service
                                                                    ?.address[0]
                                                                    ?.region && (
                                                                    <span>
                                                                        {
                                                                            service
                                                                                ?.address[0]
                                                                                ?.region
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}
                                                                {service
                                                                    ?.address[0]
                                                                    ?.country
                                                                    .length >
                                                                    0 && (
                                                                    <span>
                                                                        {
                                                                            service
                                                                                ?.address[0]
                                                                                ?.country[0]
                                                                                .name
                                                                        }
                                                                    </span>
                                                                )}
                                                            </Typography>
                                                        ) : (
                                                            <span>N/A</span>
                                                        )}
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Short Description
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {service.shortDescription ? (
                                                                <span>
                                                                    {
                                                                        service.shortDescription
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Description
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {service.description ? (
                                                                <span>
                                                                    {
                                                                        service.description
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={6}
                                                        lg={6}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Created By:
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {
                                                                service
                                                                    .createdBy
                                                                    ?.firstName
                                                            }{" "}
                                                            {
                                                                service
                                                                    .createdBy
                                                                    ?.lastName
                                                            }
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={6}
                                                        lg={6}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Updated By:
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {
                                                                service
                                                                    .updatedBy
                                                                    ?.firstName
                                                            }{" "}
                                                            {
                                                                service
                                                                    .updatedBy
                                                                    ?.lastName
                                                            }
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        ) : (
                                            <Loading />
                                        )}
                                    </Grid>
                                    {/* ) : (
                                        <LoginOrRegister />
                                    )} */}
                                </CardContent>
                            </Card>

                            {/* Contact Details */}
                            <Card>
                                <CardHeader title="Contact"></CardHeader>
                                <CardContent sx={{ pt: 0 }}>
                                    <Grid container>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Contact Person:
                                            </Typography>
                                            <Typography variant="body1">
                                                {service.contactPersonName ? (
                                                    <span>
                                                        {
                                                            service.contactPersonName
                                                        }
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Email:
                                            </Typography>
                                            <Typography variant="body1">
                                                {service.contactEmail ? (
                                                    <span>
                                                        {service.contactEmail}
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Mobile:
                                            </Typography>
                                            <Typography variant="body1">
                                                {service.contactMobile ? (
                                                    <span>
                                                        {service.contactMobile}
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Phone:
                                            </Typography>
                                            <Typography variant="body1">
                                                {service.contactPhone ? (
                                                    <span>
                                                        {service.contactPhone}
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={6}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                URL 1:
                                            </Typography>
                                            <Typography variant="body1"></Typography>
                                            {service.url1 ? (
                                                <span>{service.url1}</span>
                                            ) : (
                                                <span>N/A</span>
                                            )}
                                        </Grid>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={6}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                URL 2:
                                            </Typography>
                                            <Typography variant="body1"></Typography>
                                            {service.url2 ? (
                                                <span>{service.url2}</span>
                                            ) : (
                                                <span>N/A</span>
                                            )}
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Container>
                    </Box>
                ) : (
                    <Loading />
                )}
            </IonContent>
        </>
    );
}

export const getServerSideProps = async (context) => {
    const { slug, area } = context.params;
    console.log("service getServerSideProps slug");
    console.log(slug);
    console.log(area);

    const apiUrlParams = `services/${slug}`;
    const rawData = await getPublicData(apiUrlParams);

    console.log("service getServerSideProps data");
    console.log(rawData);

    const headMetaTitle = `${rawData.name} - Service in ${
        rawData.address[0].city
    }, ${
        rawData.areas[0].name !== "all"
            ? pascalCaseFormatter(rawData.areas[0].name)
            : ""
    } area`;

    const headMetaDescription = `${
        rawData.shortDescription
    } || Service in the ${
        rawData.areas[0].name !== "all"
            ? pascalCaseFormatter(rawData.areas[0].name)
            : ""
    } area`;
    const headMetaImage = rawData?.featuredImageUrl;
    const headMetaType = "article";

    return {
        props: {
            // data: rawData,
            head: {
                title: headMetaTitle,
                description: headMetaDescription,
                image: headMetaImage,
                type: headMetaType,
            },
        },
    };
};
