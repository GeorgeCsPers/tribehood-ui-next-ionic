import { useRouter } from "next/router";
import HeadShared from "../../../../components/head-shared";
import HeaderSecondary from "../../../../components/header/header-secondary";
import FullscreenModal from "../../../../components/modals/modal-fullscreen";
import { useEffect, useState } from "react";
import authService from "../../../../services/auth.service";
import { IonContent, IonItem } from "@ionic/react";
import {
    Container,
    Button,
    Grid,
    Card,
    CardContent,
    Box,
    CardMedia,
    CardHeader,
    Typography,
} from "@mui/material";
import { getData, getPublicData } from "../../../../services/data.service";
import Loading from "../../../../components/common/loading";
import LoginOrRegister from "../../../../components/login-or-register";
import ActivitiesForm from "../../../../components/forms/activities-form";
import isAllowedToManageChecker from "../../../../components/hooks/is-allowed-to-manage";
import { pascalCaseFormatter } from "../../../../components/hooks/text-formatter";

// Assets URL
const serverUrl = process.env.NEXT_PUBLIC_ASSETS_URL;

export default function ActivityPage() {
    const router = useRouter();
    const { query } = router || {};
    const { area = query.area || "all" } = query || {};
    // const area = query.area || "all";
    const { slug } = query || {};
    console.log("ActivityPage query.area");
    console.log(query.area);
    console.log(area);
    console.log(slug);

    const [isLoading, setIsLoading] = useState<boolean>(true);

    // - - - - - - - -
    // Check if user logged in and token is valid
    // - - - - - - - -
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        // if (!isToken) {
        //     router.push("/users/login");
        // }
    };

    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
    };

    // - - - - - - -
    // GET APIs Data
    // - - - - - - -
    const [activity, setActivity] = useState<any>({});
    const [isAllowedToManage, setIsAllowedToManage] = useState<boolean>(false);
    const getAllData = async (slug) => {
        const activitiesRawData = await getPublicData(`activities/${slug}`);
        setActivity(activitiesRawData);

        // Check if user allowed to manage
        if (isAuth) {
            const isAllowed = isAllowedToManageChecker(
                userDetails,
                activitiesRawData
            );
            setIsAllowedToManage(isAllowed);
        }
        console.log("isAllowedToManage");
        console.log(isAllowedToManage);

        setIsLoading(false);
    };

    useEffect(() => {
        if (slug && area) {
            (async () => {
                await checkToken();
                await checkUser();
                // if (isAuth) {
                await getAllData(slug);
                // }
            })();
        }
    }, [area, isAuth, slug]);

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    // - - - - - - - - -
    // Image URL adjustment
    // - - - - - - - - -
    let imageUrl = "/assets/images/TribeHood-better-connected-community.webp";
    if (
        activity?.featuredImageUrl &&
        activity?.featuredImageUrl.indexOf("http") > -1
    ) {
        imageUrl = activity?.featuredImageUrl;
    } else if (activity?.featuredImageUrl) {
        imageUrl = serverUrl + activity?.featuredImageUrl;
    }

    console.log("Single Activity");
    console.log(activity);

    return (
        <>
            {/* {!isLoading && (
                <HeadShared
                    title={
                        activity.name +
                        " - Activity in " +
                        activity.address[0].city +
                        ", " +
                        (activity.areas[0].name !== "all"
                            ? pascalCaseFormatter(activity.areas[0].name)
                            : "") +
                        " area"
                    }
                    description={
                        activity.shortDescription +
                        " || Activity in the " +
                        (activity.areas[0].name !== "all"
                            ? pascalCaseFormatter(activity.areas[0].name) + " "
                            : "") +
                        "area"
                    }
                    image={imageUrl}
                ></HeadShared>
            )} */}

            {isAuth && isAllowedToManage && (
                <FullscreenModal
                    title={modalTitle}
                    showModal={showModal}
                    setShowModal={setShowModal}
                >
                    {!isLoading && (
                        <ActivitiesForm
                            recordForEdit={recordForEdit}
                            userId={userDetails?.id}
                            userDetails={userDetails}
                        />
                    )}
                </FullscreenModal>
            )}

            <IonContent>
                <HeaderSecondary
                    addNew="activity"
                    item={activity}
                    isBack={true}
                    isSingleView={true}
                    isAuth={isAuth}
                    userDetails={userDetails}
                    image={imageUrl}
                ></HeaderSecondary>

                {!isLoading ? (
                    <Box sx={{ mt: 4, pb: 4 }}>
                        <Container sx={{ mt: 4, pb: 4 }}>
                            <Card sx={{ mb: 4 }}>
                                <CardHeader
                                    title="Details"
                                    // subheader=""
                                    action={
                                        <Box>
                                            {isAllowedToManage && (
                                                <Grid
                                                    item
                                                    sx={{
                                                        display: "flex",
                                                        alignItems: "center",
                                                        justifyContent: "end",
                                                    }}
                                                >
                                                    <Button
                                                        sx={{
                                                            display: "flex",
                                                            mr: 2,
                                                        }}
                                                        variant="contained"
                                                        size="small"
                                                        color="primary"
                                                        onClick={() => {
                                                            setShowModal(true);
                                                            setRecordForEdit(
                                                                activity
                                                            );
                                                            setModalTitle(
                                                                "Edit Activity"
                                                            );
                                                        }}
                                                    >
                                                        Edit
                                                    </Button>
                                                    <Button
                                                        sx={{
                                                            display: "flex",
                                                        }}
                                                        variant="contained"
                                                        size="small"
                                                        color="primary"
                                                        onClick={() => {
                                                            setShowModal(true);
                                                            setRecordForEdit(
                                                                null
                                                            );
                                                            setModalTitle(
                                                                "Create Activity"
                                                            );
                                                        }}
                                                    >
                                                        Add New
                                                    </Button>
                                                </Grid>
                                            )}
                                        </Box>
                                    }
                                ></CardHeader>

                                <CardContent sx={{ pt: 0 }}>
                                    {/* {isAuth ? ( */}

                                    <Grid container spacing={2}>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={4}
                                        >
                                            <CardMedia
                                                component="img"
                                                // height="240"
                                                image={imageUrl}
                                                alt={activity?.name}
                                            />
                                        </Grid>
                                        {!isLoading ? (
                                            <Grid
                                                item
                                                xs={12}
                                                sm={12}
                                                md={6}
                                                lg={8}
                                            >
                                                <Grid
                                                    container
                                                    spacing={2}
                                                    sx={{
                                                        width: "100%",
                                                        display: "flex",
                                                        mb: 6,
                                                    }}
                                                >
                                                    <Grid item xs={12} sm={6}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Activity Name
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {activity.name}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Category
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {activity
                                                                ?.categories
                                                                .length > 0 ? (
                                                                activity.categories.map(
                                                                    (
                                                                        item,
                                                                        i
                                                                    ) => (
                                                                        <span
                                                                            key={
                                                                                i
                                                                            }
                                                                        >
                                                                            {
                                                                                item.name
                                                                            }
                                                                        </span>
                                                                    )
                                                                )
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={12}
                                                        md={12}
                                                        lg={12}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Address:
                                                        </Typography>
                                                        {activity?.address ? (
                                                            <Typography variant="body1">
                                                                {activity
                                                                    ?.address[0]
                                                                    ?.addressLine1 && (
                                                                    <span>
                                                                        {
                                                                            activity
                                                                                ?.address[0]
                                                                                ?.addressLine1
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {activity
                                                                    ?.address[0]
                                                                    ?.addressLine2 && (
                                                                    <span>
                                                                        {
                                                                            activity
                                                                                ?.address[0]
                                                                                ?.addressLine2
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {activity
                                                                    ?.address[0]
                                                                    ?.addressLine3 && (
                                                                    <span>
                                                                        {
                                                                            activity
                                                                                ?.address[0]
                                                                                ?.addressLine3
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {activity
                                                                    ?.address[0]
                                                                    ?.city && (
                                                                    <span>
                                                                        {
                                                                            activity
                                                                                ?.address[0]
                                                                                ?.city
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {activity
                                                                    ?.address[0]
                                                                    ?.postcode && (
                                                                    <span>
                                                                        {
                                                                            activity
                                                                                ?.address[0]
                                                                                ?.postcode
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {activity
                                                                    ?.address[0]
                                                                    ?.county && (
                                                                    <span>
                                                                        {
                                                                            activity
                                                                                ?.address[0]
                                                                                ?.county
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {activity
                                                                    ?.address[0]
                                                                    ?.region && (
                                                                    <span>
                                                                        {
                                                                            activity
                                                                                ?.address[0]
                                                                                ?.region
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}
                                                                {activity
                                                                    ?.address[0]
                                                                    ?.country
                                                                    .length >
                                                                    0 && (
                                                                    <span>
                                                                        {
                                                                            activity
                                                                                ?.address[0]
                                                                                ?.country[0]
                                                                                .name
                                                                        }
                                                                    </span>
                                                                )}
                                                            </Typography>
                                                        ) : (
                                                            <span>N/A</span>
                                                        )}
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={4}
                                                        lg={3}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Start Date
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {activity.activityStartDate ? (
                                                                <span>
                                                                    {
                                                                        activity.activityStartDate
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>
                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={4}
                                                        lg={3}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Start Time
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {activity.acticityStartTime ? (
                                                                <span>
                                                                    {
                                                                        activity.acticityStartTime
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={4}
                                                        lg={3}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            End Date
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {activity.activityEndDate ? (
                                                                <span>
                                                                    {
                                                                        activity.activityEndDate
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>
                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={4}
                                                        lg={3}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            End Time
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {activity.activityEndTime ? (
                                                                <span>
                                                                    {
                                                                        activity.activityEndTime
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Short Description
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {activity.shortDescription ? (
                                                                <span>
                                                                    {
                                                                        activity.shortDescription
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Description
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {activity.description ? (
                                                                <span>
                                                                    {
                                                                        activity.description
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={6}
                                                        lg={6}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Created By:
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {
                                                                activity
                                                                    .createdBy
                                                                    ?.firstName
                                                            }{" "}
                                                            {
                                                                activity
                                                                    .createdBy
                                                                    ?.lastName
                                                            }
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={6}
                                                        lg={6}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Updated By:
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {
                                                                activity
                                                                    .updatedBy
                                                                    ?.firstName
                                                            }{" "}
                                                            {
                                                                activity
                                                                    .updatedBy
                                                                    ?.lastName
                                                            }
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        ) : (
                                            <Loading />
                                        )}
                                    </Grid>
                                    {/* ) : (
                                        <LoginOrRegister />
                                    )} */}
                                </CardContent>
                            </Card>

                            {/* Contact Details */}
                            <Card>
                                <CardHeader title="Contact"></CardHeader>
                                <CardContent sx={{ pt: 0 }}>
                                    <Grid container>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Contact Person:
                                            </Typography>
                                            <Typography variant="body1">
                                                {activity.contactPersonName ? (
                                                    <span>
                                                        {
                                                            activity.contactPersonName
                                                        }
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Email:
                                            </Typography>
                                            <Typography variant="body1">
                                                {activity.contactEmail ? (
                                                    <span>
                                                        {activity.contactEmail}
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Mobile:
                                            </Typography>
                                            <Typography variant="body1">
                                                {activity.contactMobile ? (
                                                    <span>
                                                        {activity.contactMobile}
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Phone:
                                            </Typography>
                                            <Typography variant="body1">
                                                {activity.contactPhone ? (
                                                    <span>
                                                        {activity.contactPhone}
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={6}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                URL 1:
                                            </Typography>
                                            <Typography variant="body1"></Typography>
                                            {activity.url1 ? (
                                                <span>{activity.url1}</span>
                                            ) : (
                                                <span>N/A</span>
                                            )}
                                        </Grid>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={6}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                URL 2:
                                            </Typography>
                                            <Typography variant="body1"></Typography>
                                            {activity.url2 ? (
                                                <span>{activity.url2}</span>
                                            ) : (
                                                <span>N/A</span>
                                            )}
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Container>
                    </Box>
                ) : (
                    <Loading />
                )}
            </IonContent>
        </>
    );
}

export const getServerSideProps = async (context) => {
    const { slug, area } = context.params;
    console.log("activity getServerSideProps slug");
    console.log(slug);
    console.log(area);

    const apiUrlParams = `activities/${slug}`;
    const rawData = await getPublicData(apiUrlParams);

    console.log("activity getServerSideProps data");
    console.log(rawData);

    const headMetaTitle = `${rawData.name} - Activity in ${
        rawData.address[0].city
    }, ${
        rawData.areas[0].name !== "all"
            ? pascalCaseFormatter(rawData.areas[0].name)
            : ""
    } area`;

    const headMetaDescription = `${
        rawData.shortDescription
    } || Activity in the ${
        rawData.areas[0].name !== "all"
            ? pascalCaseFormatter(rawData.areas[0].name)
            : ""
    } area`;
    const headMetaImage = rawData?.featuredImageUrl;
    const headMetaType = "article";

    return {
        props: {
            // data: rawData,
            head: {
                title: headMetaTitle,
                description: headMetaDescription,
                image: headMetaImage,
                type: headMetaType,
            },
        },
    };
};
