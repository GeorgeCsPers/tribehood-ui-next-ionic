import { IonRow, IonCol, IonContent, ScrollDetail } from "@ionic/react";
import { useRouter } from "next/router";
import { Container, Grid, Box } from "@mui/material";
import { useEffect, useState } from "react";
import { getData, getPublicData } from "../../../services/data.service";
import authService from "../../../services/auth.service";

import HeadShared from "../../../components/head-shared";
import FullscreenModal from "../../../components/modals/modal-fullscreen";
// import HeaderSecondary from "../../../components/header/header-secondary";
import Loading from "../../../components/common/loading";
import RightSide from "../../../components/layout/right-side";
import LeftSide from "../../../components/layout/left-side";
import useDebounce from "../../../components/hooks/use-debounce";
import SearchComponent from "../../../components/inputs/search";

import InfiniteScroll from "react-infinite-scroller";
import PlacesForm from "../../../components/forms/places-form";
import CardPlacesComponent from "../../../components/surface/card-places";
import NoData from "../../../components/common/no-data";
import { pascalCaseFormatter } from "../../../components/hooks/text-formatter";

export default function PlacesPage(props: any) {
    const router = useRouter();
    const { query } = router || {};

    console.log("PlacesPage query.area");
    console.log(query.area);
    const area = query.area || "all";

    console.log("PlacesPage query.page");
    console.log(Number(query.page));
    const page = Number(query.page) || 1;
    const itemsToTake = 10;

    const [isLoading, setIsLoading] = useState<boolean>(true);

    // - - - - - - - -
    // Check if user logged in and token is valid
    // - - - - - - - -
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
    };

    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
    };

    // - - - - - - -
    // GET APIs Data
    // - - - - - - -
    const [places, setPlaces] = useState<any | null>([]);
    const [isMoreToScroll, setIsMoreToScroll] = useState(false);
    const getAllData = async (
        areaParam: string | string[],
        // pageParam?: number,
        takeNew?: number
    ) => {
        console.log("getAllData");
        console.log(`areaParam: ${areaParam}`);
        // console.log(`pageParam: ${pageParam}`);
        //         console.log(`skipNew: ${skipNew}`);
        console.log(`takeNew: ${takeNew}`);
        // console.log(`activities: ${activities}`);

        let newPlacesRawData = null;
        let itemsPerPage = itemsToTake;
        let skip = places.length;
        let paging: any = page;
        let take = 0;
        if (page > 0) {
            take = itemsPerPage * paging;
        } else {
            take = itemsPerPage;
        }
        // let take = itemsPerPage;
        // let take = takeNew;
        console.log(`skip: ${skip}`);
        console.log(`take: ${take}`);
        console.log(`takeNew: ${takeNew}`);
        console.log(`page: ${page}`);
        console.log(`paging: ${paging}`);

        // Check and add search query if exist
        let searchQuery = "";
        if (searchText != null) {
            searchQuery = `&search=${searchText}`;
        }
        console.log("searchQuery");
        console.log(searchQuery);

        // Check if area selected
        let apiUrlParams: string;
        if (areaParam && areaParam !== "all") {
            apiUrlParams = `places?take=${take}&skip=${skip}&sortBy=createdAt&sortOrder=desc&area=${areaParam}${searchQuery}`;
        } else {
            apiUrlParams = `places?take=${take}&skip=${skip}&sortBy=createdAt&sortOrder=desc${searchQuery}`;
        }
        newPlacesRawData = await getPublicData(apiUrlParams);
        console.log("newPlacesRawData");
        console.log(newPlacesRawData);

        // Check if all content scrolled and disable infinite scroll (also hide loading symbol)
        if (places.length === newPlacesRawData.total || searchQuery) {
            setIsMoreToScroll(false);
        } else {
            setTimeout(() => {
                setIsMoreToScroll(true);
            }, 500);
        }
        setPlaces([...places, ...newPlacesRawData.data]);
        setIsLoading(false);

        console.log("isMoreToScroll");
        console.log(isMoreToScroll);
    };

    // - - - - - - - - -
    // Check search if exist
    // - - - - - - - - -
    const [searchText, setSearchText] = useState(null);
    const handleSearch = (e: any) => {
        console.log("handleSearch");
        console.log(e);
        setIsLoading(true);
        setPlaces([]);

        if (e) {
            setSearchText(e);
        } else {
            setSearchText(null);
        }
    };
    const searchDebounce = useDebounce(searchText, 500);

    useEffect(() => {
        console.log("useEffect centre");
        console.log("async useEffect centre");

        if (area || page || isAuth) {
            (async () => {
                await checkToken();
                await checkUser();
                setPlaces([]);
                await getAllData(area, itemsToTake);
            })();
        }
        if (searchDebounce) {
            console.log("searchDebounce");
            getAllData(area, itemsToTake);
        }
    }, [area, page, isAuth, searchDebounce]);

    console.log("isAuth Places");
    console.log(isAuth);

    // - - - - - - - - -
    // Check Area change and default the data for infinite scroll
    // - - - - - - - - -
    const handleAreaChange = (e) => {
        console.log("handleAreaChange");
        console.log(e);
        if (e) {
            setPlaces([]);
            getAllData(area, itemsToTake);
        }
    };

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    // - - - - - - - - -
    // Check scroll and on scroll stick Menu + Latest cards
    // - - - - - - - - -
    const [fixedClass, setFixedClass] = useState(false);
    const handleScrollStart = () => {
        console.log("scroll start");
    };

    const handleScroll = (ev: CustomEvent<ScrollDetail>) => {
        // console.log("scroll", ev.detail);
        if (ev.detail.scrollTop > 4) {
            setFixedClass(true);
        } else {
            setFixedClass(false);
        }
    };

    const handleScrollEnd = () => {
        console.log("scroll end");
    };

    return (
        <>
            {/* {!isLoading && (
                <HeadShared
                    title={pascalCaseFormatter(area) + " places"}
                    description={`Find all the greatest places in the ${
                        area !== "all" ? pascalCaseFormatter(area) + " " : ""
                    }area`}
                    // image={area + " activities"}
                ></HeadShared>
            )} */}

            {isAuth && (
                <FullscreenModal
                    title={modalTitle}
                    showModal={showModal}
                    setShowModal={setShowModal}
                >
                    {!isLoading && (
                        <PlacesForm
                            recordForEdit={recordForEdit}
                            // dataCategoryTypes={categoryTypes}
                            // dataCategories={categories}
                            userId={userDetails?.id}
                            userDetails={userDetails}
                        />
                    )}
                </FullscreenModal>
            )}

            <IonContent
                scrollEvents={true}
                onIonScrollStart={handleScrollStart}
                onIonScroll={handleScroll}
                onIonScrollEnd={handleScrollEnd}
            >
                <Box className="infinite-scroll">
                    <InfiniteScroll
                        pageStart={0}
                        // initialLoad={true}
                        loadMore={() =>
                            setTimeout(() => {
                                getAllData(area, itemsToTake);
                            }, 100)
                        }
                        hasMore={isMoreToScroll}
                        threshold={50}
                        useWindow={false}
                        loader={
                            <div className="loader" key={0}>
                                Loading ...
                            </div>
                        }
                    >
                        <Container sx={{ mt: 4, pb: 4 }}>
                            <IonRow>
                                <IonCol
                                    size-lg="3"
                                    size-xl="3"
                                    className="ion-hide-lg-down pos-rel"
                                >
                                    <Box
                                        sx={{ display: "flex" }}
                                        className="fixed left-side"
                                    >
                                        <LeftSide
                                            // fixedClass={fixedClass}
                                            handleAreaChange={handleAreaChange}
                                        />
                                    </Box>
                                </IonCol>
                                <IonCol
                                    size-xs="12"
                                    size-lg="6"
                                    size-xl="6"
                                    className="pos-rel"
                                >
                                    <Grid
                                        container
                                        spacing={2}
                                        className={
                                            fixedClass
                                                ? "sticky search-box"
                                                : ""
                                        }
                                    >
                                        <Grid
                                            item
                                            xs={12}
                                            sx={
                                                fixedClass
                                                    ? { pl: 0, pr: 0 }
                                                    : {
                                                          display: "flex",
                                                          alignItems: "center",
                                                          justifyContent:
                                                              "space-around",
                                                          mb: 2,
                                                      }
                                            }
                                        >
                                            <SearchComponent
                                                handleSearch={(e: string) =>
                                                    handleSearch(e)
                                                }
                                            />
                                        </Grid>
                                    </Grid>

                                    {!isLoading ? (
                                        <Box>
                                            <Grid container spacing={2}>
                                                {places.length > 0 ? (
                                                    places?.map((item, i) => (
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            key={i}
                                                            sx={{
                                                                display: "flex",
                                                                alignItems:
                                                                    "stretch",
                                                                mb: 2,
                                                            }}
                                                        >
                                                            <CardPlacesComponent
                                                                currentUserId={
                                                                    userDetails?.id
                                                                }
                                                                isAuth={isAuth}
                                                                area={area}
                                                                data={item}
                                                                showModal={
                                                                    showModal
                                                                }
                                                                setShowModal={
                                                                    setShowModal
                                                                }
                                                                setRecordForEdit={
                                                                    setRecordForEdit
                                                                }
                                                                setModalTitle={
                                                                    setModalTitle
                                                                }
                                                            />
                                                        </Grid>
                                                    ))
                                                ) : (
                                                    <Grid item xs={12}>
                                                        <NoData type="card" />
                                                    </Grid>
                                                )}
                                            </Grid>
                                        </Box>
                                    ) : (
                                        <Loading />
                                    )}
                                </IonCol>
                                <IonCol
                                    size-lg="3"
                                    size-xl="3"
                                    className="ion-hide-lg-down pos-rel"
                                >
                                    <Box
                                        sx={{ display: "flex" }}
                                        className="fixed right-side"
                                    >
                                        <RightSide />
                                    </Box>
                                </IonCol>
                            </IonRow>
                        </Container>
                    </InfiniteScroll>
                </Box>
            </IonContent>
        </>
    );
}

export const getServerSideProps = async (context) => {
    const { slug, area } = context.params;
    console.log("places getServerSideProps slug");
    console.log(slug);
    console.log(area);

    // const apiUrlParams = `places?take=100&skip=0&sortBy=createdAt&sortOrder=desc&area=${area}`;
    // const rawData = await getPublicData(apiUrlParams);

    // console.log("places getServerSideProps data");
    // console.log(rawData);

    const headMetaTitle = `${pascalCaseFormatter(area)} places`;
    const headMetaDescription = `Find all the latest places in the ${
        area !== "all" ? pascalCaseFormatter(area) + " " : ""
    }area`;

    const headMetaImage = ""; // rawData?.featuredImageUrl
    const headMetaType = "article";

    return {
        props: {
            // data: rawData,
            head: {
                title: headMetaTitle,
                description: headMetaDescription,
                image: headMetaImage,
                type: headMetaType,
            },
        },
    };
};

// // This function gets called at build time
// export async function getStaticProps() {
//     // Call an external API endpoint to get posts
//     // const rawData = await getData(null, "commands");
//     const commands = await getPublicData("commands");

//     // By returning { props: { posts } }, the Blog component
//     // will receive `posts` as a prop at build time
//     return {
//         props: {
//             commands,
//         },
//     };
// }
