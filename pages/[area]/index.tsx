import {
    IonContent,
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    ScrollDetail,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
} from "@ionic/react";
import Image from "next/image";
import HeaderSecondary from "../../components/header/header-secondary";
import { Box, Container, Grid } from "@mui/material";
import Loading from "../../components/common/loading";
import LeftSide from "../../components/layout/left-side";
import RightSide from "../../components/layout/right-side";
import CardEventComponent from "../../components/surface/card-event";
import { useRouter } from "next/router";
import { useState, useEffect, useCallback } from "react";
import authService from "../../services/auth.service";
import { getPublicData } from "../../services/data.service";
import HeadShared from "../../components/head-shared";
import useDebounce from "../../components/hooks/use-debounce";
// import EventsForm from "../../components/forms/events-form";
// import FullscreenModal from "../../components/modals/modal-fullscreen";
import SearchComponent from "../../components/inputs/search";

import InfiniteScroll from "react-infinite-scroller";
import {
    camelCaseFormatter,
    pascalCaseFormatter,
} from "../../components/hooks/text-formatter";

export default function HomePage() {
    const router = useRouter();
    const { query = {} } = router || {};

    console.log("HomePage query.area");
    console.log(query.area);
    const area = query.area || "all";

    console.log("HomePage query.page");
    console.log(Number(query.page));
    const page = Number(query.page) || 1;
    const itemsToTake = 50;

    const [isLoading, setIsLoading] = useState<boolean>(true);

    // - - - - - - - -
    // Check if user logged in and token is valid
    // - - - - - - - -
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = useCallback(async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        // if (!isToken) {
        //     router.push("/users/login");
        // }
        // checkUser();
    }, []);

    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userDetails, setUserDetails] = useState<any>();
    const checkUser = useCallback(async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
        // getData(user);
    }, []);

    // - - - - - - -
    // GET APIs Data
    // - - - - - - -
    const [feeds, setFeeds] = useState([]);
    const [isMoreToScroll, setIsMoreToScroll] = useState(false);
    const getAllData = async (
        areaParam: string | string[],
        // pageParam?: number,
        takeNew?: number
    ) => {
        console.log("getAllData");
        console.log(`areaParam: ${areaParam}`);
        // console.log(`pageParam: ${pageParam}`);
        //         console.log(`skipNew: ${skipNew}`);
        console.log(`takeNew: ${takeNew}`);
        // console.log(`feeds: ${feeds}`);

        let newFeedsRawData = null;
        let skip = feeds.length;
        // let skip = 10;
        let paging: any = page;
        let take = 0;
        if (page > 0) {
            take = itemsToTake * paging;
        } else {
            take = itemsToTake;
        }
        // let take = itemsPerPage;
        // let take = takeNew;
        console.log(`skip: ${skip}`);
        console.log(`take: ${take}`);
        console.log(`takeNew: ${takeNew}`);
        console.log(`page: ${page}`);
        console.log(`paging: ${paging}`);

        // Check and add search query if exist
        let searchQuery = "";
        if (searchText != null) {
            searchQuery = `&search=${searchText}`;
        }
        console.log("searchQuery");
        console.log(searchQuery);

        // Check if area selected
        let apiUrlParams: string;
        if (areaParam && areaParam !== "all") {
            apiUrlParams = `feeds?take=${take}&skip=${skip}&sortBy=createdAt&sortOrder=desc&area=${areaParam}${searchQuery}`;
        } else {
            apiUrlParams = `feeds?take=${take}&skip=${skip}&sortBy=createdAt&sortOrder=desc${searchQuery}`;
        }
        newFeedsRawData = await getPublicData(apiUrlParams);
        console.log("newFeedsRawData");
        console.log(newFeedsRawData);

        // Check if all content scrolled and disable infinite scroll
        if (feeds.length === newFeedsRawData.total) {
            setIsMoreToScroll(false);
        } else {
            setTimeout(() => {
                setIsMoreToScroll(true);
            }, 500);
        }

        setFeeds([...feeds, ...newFeedsRawData.data]);
        setIsLoading(false);

        console.log("isMoreToScroll");
        console.log(isMoreToScroll);
    };

    // - - - - - - - - -
    // Check search if exist
    // - - - - - - - - -
    const [searchText, setSearchText] = useState(null);
    const handleSearch = (e: any) => {
        console.log("handleSearch");
        console.log(e);
        setIsLoading(true);
        setFeeds([]);

        if (e) {
            setSearchText(e);
        } else {
            setSearchText(null);
        }
    };
    const searchDebounce = useDebounce(searchText, 500);

    useEffect(() => {
        console.log("useEffect centre");
        console.log("async useEffect centre");

        if (area || isAuth) {
            (async () => {
                await checkToken();
                await checkUser();
                await getAllData(area, itemsToTake);
            })();
        }
        if (searchDebounce) {
            console.log("searchDebounce");
            getAllData(area, itemsToTake);
        }
    }, [area, page, isAuth, searchDebounce]);

    console.log("isAuth Feeds Main");
    console.log(isAuth);

    console.log("userDetails");
    console.log(userDetails);

    console.log("feeds");
    console.log(feeds);

    // - - - - - - - - -
    // Check Area change and default the data for infinite scroll
    // - - - - - - - - -
    const handleAreaChange = (e) => {
        console.log("handleAreaChange");
        console.log(e);
        if (e) {
            setFeeds([]);
            getAllData(area, itemsToTake);
        }
    };

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    return (
        <>
            {!isLoading && (
                <HeadShared
                    title={`Tribehood - Better Connected Community ${
                        area !== "all"
                            ? " || " + pascalCaseFormatter(area) + " area"
                            : ""
                    }`}
                    description={`All the greatest and latest events, activities, places, projects, public notices and services from the community to the community in the ${
                        area !== "all" ? pascalCaseFormatter(area) + " " : ""
                    }area`}
                ></HeadShared>
            )}

            {/* <HeaderSecondary title="Centre"></HeaderSecondary> */}

            <IonContent
                className="home-page infinite-scroll"
                // scrollEvents={true}
                // onIonScrollStart={handleScrollStart}
                // onIonScroll={handleScroll}
                // onIonScrollEnd={handleScrollEnd}
                // forceOverscroll={true}
                // scrollY={true}
                // overflow-scroll={true}
            >
                <Box className="infinite-scroll">
                    <InfiniteScroll
                        pageStart={0}
                        // initialLoad={true}
                        loadMore={() =>
                            setTimeout(() => {
                                getAllData(area, itemsToTake);
                            }, 100)
                        }
                        hasMore={isMoreToScroll}
                        threshold={50}
                        useWindow={false}
                        loader={
                            <div className="loader" key={0}>
                                Loading ...
                            </div>
                        }
                    >
                        <Container sx={{ mt: 4, pb: 4 }}>
                            <IonRow>
                                <IonCol
                                    size-lg="3"
                                    size-xl="3"
                                    className="ion-hide-lg-down pos-rel"
                                >
                                    <Box
                                        sx={{ display: "flex" }}
                                        className="fixed left-side"
                                    >
                                        <LeftSide
                                            // fixedClass={fixedClass}
                                            handleAreaChange={handleAreaChange}
                                        />
                                    </Box>
                                </IonCol>
                                <IonCol size-xs="12" size-lg="6" size-xl="6">
                                    <Grid container spacing={2}>
                                        <Grid
                                            item
                                            xs={12}
                                            sx={{
                                                display: "flex",
                                                alignItems: "center",
                                                justifyContent: "space-around",
                                                mb: 2,
                                            }}
                                        >
                                            <SearchComponent
                                                handleSearch={(e: string) =>
                                                    handleSearch(e)
                                                }
                                            />
                                        </Grid>
                                    </Grid>

                                    {!isLoading ? (
                                        <Box>
                                            <Grid container spacing={2}>
                                                {feeds?.map((item, i) => (
                                                    <Grid
                                                        item
                                                        xs={12}
                                                        key={i}
                                                        sx={{
                                                            display: "flex",
                                                            alignItems:
                                                                "stretch",
                                                        }}
                                                    >
                                                        <CardEventComponent
                                                            // currentUserId={
                                                            //     userDetails?.id
                                                            // }
                                                            // isAuth={isAuth}
                                                            // area={area}
                                                            data={item}
                                                            // showModal={showModal}
                                                            // setShowModal={setShowModal}
                                                            // setRecordForEdit={
                                                            //     setRecordForEdit
                                                            // }
                                                            // setModalTitle={
                                                            //     setModalTitle
                                                            // }
                                                        />
                                                    </Grid>
                                                ))}
                                            </Grid>
                                        </Box>
                                    ) : (
                                        <Loading />
                                    )}
                                </IonCol>
                                <IonCol
                                    size-lg="3"
                                    size-xl="3"
                                    className="ion-hide-lg-down pos-rel"
                                >
                                    <Box
                                        sx={{ display: "flex" }}
                                        className="fixed right-side"
                                    >
                                        <RightSide />
                                    </Box>
                                    {/* <RightSide fixedClass={fixedClass} /> */}
                                </IonCol>
                            </IonRow>
                        </Container>
                    </InfiniteScroll>
                </Box>
            </IonContent>
        </>
    );
}
