import {
    IonRow,
    IonCol,
    IonContent,
    ScrollDetail,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
} from "@ionic/react";
import { useRouter } from "next/router";
import { Container, Grid, Box } from "@mui/material";
import { useEffect, useState } from "react";
import { getData, getPublicData } from "../../../services/data.service";
import authService from "../../../services/auth.service";

import HeadShared from "../../../components/head-shared";
import FullscreenModal from "../../../components/modals/modal-fullscreen";
import EventsForm from "../../../components/forms/events-form";
// import HeaderSecondary from "../../../components/header/header-secondary";
import Loading from "../../../components/common/loading";
import CardEventComponent from "../../../components/surface/card-event";
import RightSide from "../../../components/layout/right-side";
import LeftSide from "../../../components/layout/left-side";
import useDebounce from "../../../components/hooks/use-debounce";
import SearchComponent from "../../../components/inputs/search";

import InfiniteScroll from "react-infinite-scroller";
import NoData from "../../../components/common/no-data";
import { pascalCaseFormatter } from "../../../components/hooks/text-formatter";
import { NextSeo } from "next-seo";
import { Metadata } from "next";
import Head from "next/head";

export const metadata: Metadata = {
    generator: "Next.js",
    applicationName: "Next.js",
    referrer: "origin-when-cross-origin",
    keywords: ["Next.js", "React", "JavaScript"],
    authors: [{ name: "Seb" }, { name: "Josh", url: "https://nextjs.org" }],
    colorScheme: "dark",
    creator: "Jiachi Liu",
    publisher: "Sebastian Markbåge",
    formatDetection: {
        email: false,
        address: false,
        telephone: false,
    },
};

export default function EventsPage(props: any) {
    const router = useRouter();
    const { query } = router || {};
    // const { area = "wallingford" } = query || {};
    console.log("EventsPage query.area");
    console.log(query.area);
    const area = query.area || "all";

    // import { useErrorBoundary } from "react-error-boundary";
    //     const { showBoundary } = useErrorBoundary();

    console.log("EventsPage query.page");
    console.log(Number(query.page));
    const page = Number(query.page) || 1; // https://developers.google.com/search/blog/2014/02/infinite-scroll-search-friendly
    const itemsToTake = 100;
    // const isAuth = authService.checkToken();

    console.log("EventsPage props");
    console.log(props);
    // console.log(props.events);
    // console.log(Number(props));

    const [isLoading, setIsLoading] = useState<boolean>(true);

    // - - - - - - - -
    // Check if user logged in and token is valid
    // - - - - - - - -
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
    };

    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
    };

    // - - - - - - -
    // GET APIs Data
    // - - - - - - -
    const [events, setEvents] = useState<any | null>([]);
    const [isMoreToScroll, setIsMoreToScroll] = useState(false);
    const getAllData = async (
        areaParam: string | string[],
        // pageParam?: number,
        takeNew?: number
    ) => {
        console.log("getAllData");
        console.log(`areaParam: ${areaParam}`);
        // console.log(`pageParam: ${pageParam}`);
        //         console.log(`skipNew: ${skipNew}`);
        console.log(`takeNew: ${takeNew}`);

        let newEventsRawData = null;
        let itemsPerPage = itemsToTake;
        let skip = events.length;
        let paging: any = page;
        let take = 0;
        if (page > 0) {
            take = itemsPerPage * paging;
        } else {
            take = itemsPerPage;
        }
        // let take = itemsPerPage;
        // let take = takeNew;
        console.log(`skip: ${skip}`);
        console.log(`take: ${take}`);
        console.log(`takeNew: ${takeNew}`);
        console.log(`page: ${page}`);
        console.log(`paging: ${paging}`);

        // Check and add search query if exist
        let searchQuery = "";
        if (searchText != null) {
            searchQuery = `&search=${searchText}`;
        }
        console.log("searchQuery");
        console.log(searchQuery);

        // Check if area selected
        let apiUrlParams: string;
        if (areaParam && areaParam !== "all") {
            apiUrlParams = `events?take=${take}&skip=${skip}&sortBy=startDate&sortOrder=desc&area=${areaParam}${searchQuery}`;
        } else {
            apiUrlParams = `events?take=${take}&skip=${skip}&sortBy=startDate&sortOrder=desc${searchQuery}`;
            // apiUrlParams = `events?take=20&skip=0&sortBy=createdAt&sortOrder=desc`;
        }
        newEventsRawData = await getPublicData(apiUrlParams);
        console.log("newEventsRawData");
        console.log(newEventsRawData);

        // Check if all content scrolled and disable infinite scroll (also hide loading symbol)
        if (
            events.length === newEventsRawData.total ||
            searchQuery.length > 0
        ) {
            setIsMoreToScroll(false);
        } else {
            setTimeout(() => {
                setIsMoreToScroll(true);
            }, 500);
        }
        setEvents([...events, ...newEventsRawData.data]);
        setIsLoading(false);

        console.log("isMoreToScroll");
        console.log(isMoreToScroll);
    };

    // - - - - - - - - -
    // Check search if exist
    // - - - - - - - - -
    const [searchText, setSearchText] = useState(null);
    const handleSearch = (e: any) => {
        console.log("handleSearch");
        console.log(e);
        setIsLoading(true);
        setEvents([]);

        if (e.length > 0) {
            setSearchText(e);
        } else {
            setSearchText(null);
        }
    };
    const searchDebounce = useDebounce(searchText, 500);

    useEffect(() => {
        console.log("useEffect events");
        console.log("async useEffect events");

        if (area || page || isAuth) {
            (async () => {
                await checkToken();
                await checkUser();
                // if (!props && !isMoreToScroll) {
                //     console.log("getAllData(area)");
                // setEvents([]);
                await getAllData(area);
                // } else {
                // console.log("useEffect props");
                // setEvents(props.events.data);
                // setIsLoading(false);
                // }
            })();
        }

        // Watch and adjust search text
        // console.log("props");
        // console.log(props);
        // console.log(props.handleSearchNav);
        // if (props.handleSearchNav || props.handleSearchNav === "") {
        //     handleSearch(props.handleSearchNav);
        // }
        if (searchDebounce) {
            console.log("searchDebounce");
            getAllData(area);
        }
    }, [area, page, isAuth, searchDebounce]);

    console.log("Events");
    console.log(events);
    console.log("isAuth");
    console.log(isAuth);

    // - - - - - - - - -
    // Check Area change and default the data for infinite scroll
    // - - - - - - - - -
    const handleAreaChange = (e) => {
        console.log("handleAreaChange");
        console.log(e);
        if (e) {
            setEvents([]);
            getAllData(area);
        }
    };

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    // - - - - - - - - -
    // Check scroll and on scroll stick Menu + Latest cards
    // - - - - - - - - -
    const [fixedClass, setFixedClass] = useState(false);
    const handleScrollStart = () => {
        console.log("scroll start");
    };

    const handleScroll = (ev: CustomEvent<ScrollDetail>) => {
        // console.log("scroll", ev.detail);
        if (ev.detail.scrollTop > 4) {
            setFixedClass(true);
        } else {
            setFixedClass(false);
        }
    };

    const handleScrollEnd = () => {
        console.log("scroll end");
        getAllData(area);
    };

    return (
        <>
            {/* <NextSeo
                title={pascalCaseFormatter(area) + " events"}
                description={`Find all the latest events in the ${
                    area !== "all" ? pascalCaseFormatter(area) + " " : ""
                }area`}
                canonical={window.location.href}
                openGraph={{
                    url: window.location.href,
                    title: pascalCaseFormatter(area) + " events",
                    description: `Find all the latest events in the ${
                        area !== "all" ? pascalCaseFormatter(area) + " " : ""
                    }area`,
                    images: [
                        {
                            url: "https://www.example.com/og-image01.jpg",
                            width: 800,
                            height: 600,
                            alt: "Og Image Alt",
                            type: "image/jpeg",
                        },
                        {
                            url: "https://www.example.com/og-image02.jpg",
                            width: 900,
                            height: 800,
                            alt: "Og Image Alt Second",
                            type: "image/jpeg",
                        },
                        { url: "https://www.example.com/og-image03.jpg" },
                        { url: "https://www.example.com/og-image04.jpg" },
                    ],
                    site_name: `TribeHood || ${pascalCaseFormatter(
                        area
                    )} events`,
                }}
                twitter={{
                    handle: "@handle",
                    site: "@site",
                    cardType: "summary_large_image",
                }}
            /> */}
            {/* {!isLoading && ( */}
            {/* <HeadShared
                title={pascalCaseFormatter(area) + " events"}
                description={`Find all the latest events in the ${
                    area !== "all" ? pascalCaseFormatter(area) + " " : ""
                }area`}
                type="article"
                // image={area + " events"}
            ></HeadShared> */}
            {/* )} */}

            {/* <HeaderSecondary title="Events"></HeaderSecondary> */}

            {isAuth && (
                <FullscreenModal
                    title={modalTitle}
                    showModal={showModal}
                    setShowModal={setShowModal}
                >
                    {!isLoading && (
                        <EventsForm
                            recordForEdit={recordForEdit}
                            // dataCategoryTypes={categoryTypes}
                            // dataCategories={categories}
                            userId={userDetails?.id}
                            userDetails={userDetails}
                        />
                    )}
                </FullscreenModal>
            )}

            <IonContent
                className="events-page infinite-scroll"
                scrollEvents={true}
                onIonScrollStart={handleScrollStart}
                onIonScroll={handleScroll}
                onIonScrollEnd={handleScrollEnd}
                forceOverscroll={true}
                scrollY={true}
                overflow-scroll={true}
            >
                <Box className="infinite-scroll">
                    <InfiniteScroll
                        pageStart={0}
                        // initialLoad={true}
                        loadMore={() =>
                            setTimeout(() => {
                                getAllData(area, itemsToTake);
                            }, 100)
                        }
                        hasMore={isMoreToScroll}
                        threshold={50}
                        useWindow={false}
                        loader={
                            <div className="loader" key={0}>
                                Loading ...
                            </div>
                        }
                    >
                        <Container sx={{ mt: 4, pb: 4 }}>
                            <IonRow>
                                <IonCol
                                    size-lg="3"
                                    size-xl="3"
                                    className="ion-hide-lg-down pos-rel"
                                >
                                    <Box
                                        sx={{ display: "flex" }}
                                        className="fixed left-side"
                                    >
                                        <LeftSide
                                            // fixedClass={fixedClass}
                                            handleAreaChange={handleAreaChange}
                                        />
                                    </Box>
                                </IonCol>
                                <IonCol
                                    size-xs="12"
                                    size-lg="6"
                                    size-xl="6"
                                    className="pos-rel"
                                >
                                    <Grid
                                        container
                                        spacing={2}
                                        className={
                                            fixedClass
                                                ? "sticky search-box"
                                                : ""
                                        }
                                    >
                                        <Grid
                                            item
                                            xs={12}
                                            sx={
                                                fixedClass
                                                    ? { pl: 0, pr: 0 }
                                                    : {
                                                          display: "flex",
                                                          alignItems: "center",
                                                          justifyContent:
                                                              "space-around",
                                                          mb: 2,
                                                      }
                                            }
                                        >
                                            <SearchComponent
                                                handleSearch={(e: string) =>
                                                    handleSearch(e)
                                                }
                                            />
                                            {/* <TextField
                                        sx={{ display: "flex" }}
                                        fullWidth
                                        label="Search..."
                                        type="text"
                                        onChange={(e) => handleSearch(e)}
                                    /> */}
                                            {/* {isAuth && (
                                        <Button
                                            sx={{ display: "flex" }}
                                            variant="contained"
                                            size="small"
                                            color="primary"
                                            onClick={() => {
                                                setShowModal(true);
                                                setRecordForEdit(null);
                                                setModalTitle("Create Event");
                                            }}
                                        >
                                            Add New
                                        </Button>
                                    )} */}
                                        </Grid>
                                    </Grid>

                                    {!isLoading ? (
                                        <Box>
                                            <Grid container spacing={2}>
                                                {events.length > 0 ? (
                                                    events?.map((item, i) => (
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            key={i}
                                                            sx={{
                                                                display: "flex",
                                                                alignItems:
                                                                    "stretch",
                                                                mb: 2,
                                                            }}
                                                        >
                                                            <CardEventComponent
                                                                currentUserId={
                                                                    userDetails?.id
                                                                }
                                                                userDetails={
                                                                    userDetails
                                                                }
                                                                isAuth={isAuth}
                                                                area={area}
                                                                data={item}
                                                                showModal={
                                                                    showModal
                                                                }
                                                                setShowModal={
                                                                    setShowModal
                                                                }
                                                                setRecordForEdit={
                                                                    setRecordForEdit
                                                                }
                                                                setModalTitle={
                                                                    setModalTitle
                                                                }
                                                            />
                                                        </Grid>
                                                    ))
                                                ) : (
                                                    <Grid item xs={12}>
                                                        <NoData type="card" />
                                                    </Grid>
                                                )}
                                            </Grid>
                                            {/* <IonInfiniteScroll
                                        threshold="30%"
                                        disabled={isScrollDisabled}
                                        onIonInfinite={(ev) => {
                                            getAllData(area);
                                            setTimeout(
                                                () => ev.target.complete(),
                                                500
                                            );
                                        }}
                                    >
                                        {!isScrollDisabled && (
                                            <div className="infinite-scroll-content">
                                                Scroll loading...
                                                <Loading />
                                                <IonInfiniteScrollContent loadingText="Please wait..." loadingSpinner="bubbles"></IonInfiniteScrollContent>
                                            </div>
                                        )}
                                    </IonInfiniteScroll> */}
                                        </Box>
                                    ) : (
                                        <Loading />
                                    )}
                                </IonCol>
                                <IonCol
                                    size-lg="3"
                                    size-xl="3"
                                    className="ion-hide-lg-down pos-rel"
                                >
                                    <Box
                                        sx={{ display: "flex" }}
                                        className="fixed right-side"
                                    >
                                        <RightSide />
                                    </Box>
                                    {/* <RightSide fixedClass={fixedClass} /> */}
                                </IonCol>
                            </IonRow>
                        </Container>
                    </InfiniteScroll>
                </Box>
            </IonContent>
        </>
    );
}

export const getServerSideProps = async (context) => {
    const { slug, area } = context.params;
    console.log("events getServerSideProps slug");
    console.log(slug);
    console.log(area);

    // const apiUrlParams = `events?take=100&skip=0&sortBy=createdAt&sortOrder=desc&area=${area}`;
    // const rawData = await getPublicData(apiUrlParams);

    // console.log("events getServerSideProps data");
    // console.log(rawData);

    const headMetaTitle = `${pascalCaseFormatter(area)} events`;
    const headMetaDescription = `Find all the latest events in the ${
        area !== "all" ? pascalCaseFormatter(area) + " " : ""
    }area`;

    const headMetaImage = ""; // rawData?.featuredImageUrl
    const headMetaType = "article";

    return {
        props: {
            // data: rawData,
            head: {
                title: headMetaTitle,
                description: headMetaDescription,
                image: headMetaImage,
                type: headMetaType,
            },
        },
    };
};

// This gets called on every request
// export async function getServerSideProps(context) {
//     const { area } = context.params;
//     console.log("getServerSideProps area");
//     console.log(area);

//     // Fetch data from external API
//     const apiUrlParams = `events?take=10&skip=0&sortBy=createdAt&sortOrder=desc&area=${area}`;
//     const data = await getPublicData(apiUrlParams);

//     // Pass data to the page via props
//     return { props: { events: data } };
// }

// export async function getStaticPaths() {
//     const areasRawData = await getPublicData("location/areas/selection");

//     console.log("getStaticPaths areasRawData");
//     console.log(areasRawData);

//     // Get the paths we want to pre-render based on areas
//     const areas = areasRawData.map((item) => ({
//         params: { area: item.slug },
//     }));
//     console.log("areas");
//     console.log(areas);

//     // Pass present information on the page through props
//     return {
//         paths: areas,
//         fallback: false,
//     };
// }

// // {params} || context
// export async function getStaticProps({ params }) {
//     console.log("getStaticProps params");
//     console.log(params);

//     const { area } = params;
//     console.log("getStaticProps area");
//     console.log(area);

//     // const apiUrlParams = `events?take=10&skip=0&sortBy=createdAt&sortOrder=desc&area=${area}`;
//     // const rawData = await getPublicData(apiUrlParams);

//     // console.log("getStaticProps data");
//     // console.log(rawData);

//     const headMetaContentTitle = `${pascalCaseFormatter(area)} events`;
//     const headMetaContentDescription = `Find all the latest events in the ${
//         area !== "all" ? pascalCaseFormatter(area) + " " : ""
//     }area`;

//     return {
//         props: {
//             // data: data,
//             head: {
//                 title: headMetaContentTitle,
//                 description: headMetaContentDescription,
//             },
//         },
//     };
// }
