import { useRouter } from "next/router";
import EventsForm from "../../../../components/forms/events-form";
import HeadShared from "../../../../components/head-shared";
import HeaderSecondary from "../../../../components/header/header-secondary";
import FullscreenModal from "../../../../components/modals/modal-fullscreen";
import { useEffect, useState } from "react";
import authService from "../../../../services/auth.service";
import { IonContent, IonItem } from "@ionic/react";
import {
    Container,
    Button,
    Grid,
    Card,
    CardContent,
    Box,
    CardMedia,
    CardHeader,
    Typography,
} from "@mui/material";
import { getData, getPublicData } from "../../../../services/data.service";
import Loading from "../../../../components/common/loading";
import LoginOrRegister from "../../../../components/login-or-register";
import isAllowedToManageChecker from "../../../../components/hooks/is-allowed-to-manage";
import { pascalCaseFormatter } from "../../../../components/hooks/text-formatter";
import dayjs from "dayjs";

// Assets URL
const serverUrl = process.env.NEXT_PUBLIC_ASSETS_URL;

export default function EventPage() {
    const router = useRouter();
    const { query } = router || {};
    const { area = query.area || "all" } = query || {};
    // const area = query.area || "all";
    const { slug } = query || {};
    console.log("EventPage query.area");
    console.log(query.area);
    console.log(area);
    console.log(slug);

    const [isLoading, setIsLoading] = useState<boolean>(true);

    // - - - - - - - -
    // Check if user logged in and token is valid
    // - - - - - - - -
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        // if (!isToken) {
        //     router.push("/users/login");
        // }
    };

    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
    };

    // - - - - - - -
    // GET APIs Data
    // - - - - - - -
    const [event, setEvent] = useState<any>({});
    const [isAllowedToManage, setIsAllowedToManage] = useState<boolean>(false);
    const getAllData = async (slug) => {
        // const eventsRawData = await getData(`events/${id}`);
        const eventsRawData = await getPublicData(`events/${slug}`);
        setEvent(eventsRawData);

        // Check if user allowed to manage
        if (isAuth) {
            const isAllowed = isAllowedToManageChecker(
                userDetails,
                eventsRawData
            );
            setIsAllowedToManage(isAllowed);
        }
        console.log("isAllowedToManage");
        console.log(isAllowedToManage);

        setIsLoading(false);
    };

    useEffect(() => {
        if (slug && area) {
            (async () => {
                await checkToken();
                await checkUser();
                // if (isAuth) {
                await getAllData(slug);
                // }
            })();
        }
    }, [area, isAuth, slug]);

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    // - - - - - - - - -
    // Image URL adjustment
    // - - - - - - - - -
    let imageUrl = "/assets/images/TribeHood-better-connected-community.webp";
    if (
        event?.featuredImageUrl &&
        event?.featuredImageUrl.indexOf("http") > -1
    ) {
        imageUrl = event?.featuredImageUrl;
    } else if (event?.featuredImageUrl) {
        imageUrl = serverUrl + event?.featuredImageUrl;
    }

    console.log("Single Event");
    console.log(event);

    return (
        <>
            {/* {!isLoading && (
                <HeadShared
                    title={
                        event.name +
                        " - Event in " +
                        event.address[0].city +
                        ", " +
                        (event.areas[0].name !== "all"
                            ? pascalCaseFormatter(event.areas[0].name)
                            : "") +
                        " area"
                    }
                    description={
                        event.shortDescription +
                        " || Event in the " +
                        (event.areas[0].name !== "all"
                            ? pascalCaseFormatter(event.areas[0].name) + " "
                            : "") +
                        "area"
                    }
                    image={imageUrl}
                    type="article"
                ></HeadShared>
            )} */}

            {isAuth && isAllowedToManage && (
                <FullscreenModal
                    title={modalTitle}
                    showModal={showModal}
                    setShowModal={setShowModal}
                >
                    {!isLoading && (
                        <EventsForm
                            recordForEdit={recordForEdit}
                            userId={userDetails?.id}
                            userDetails={userDetails}
                        />
                    )}
                </FullscreenModal>
            )}

            <IonContent>
                <HeaderSecondary
                    addNew="event"
                    item={event}
                    isBack={true}
                    isSingleView={true}
                    isAuth={isAuth}
                    userDetails={userDetails}
                    image={imageUrl}
                ></HeaderSecondary>

                {!isLoading ? (
                    <Box sx={{ mt: 4, pb: 4 }}>
                        {/* <Box className="single-view-header">
                            <CardMedia
                                component="img"
                                height="240"
                                image={imageUrl}
                                alt={event?.name}
                            />
                            <Container sx={{ mt: 4, pb: 4 }}>
                                <Box className="single-view-header-content">
                                    {event?.name}
                                </Box>
                            </Container>
                        </Box> */}
                        <Container sx={{ mt: 4, pb: 4 }}>
                            <Card sx={{ mb: 4 }} className="large-card">
                                <CardHeader
                                    title="Details"
                                    // subheader=""
                                    action={
                                        <Box>
                                            {isAllowedToManage && (
                                                <Grid
                                                    item
                                                    sx={{
                                                        display: "flex",
                                                        alignItems: "center",
                                                        justifyContent: "end",
                                                    }}
                                                >
                                                    <Button
                                                        sx={{
                                                            display: "flex",
                                                            mr: 2,
                                                        }}
                                                        variant="contained"
                                                        size="small"
                                                        color="primary"
                                                        onClick={() => {
                                                            setShowModal(true);
                                                            setRecordForEdit(
                                                                event
                                                            );
                                                            setModalTitle(
                                                                "Edit Event"
                                                            );
                                                        }}
                                                    >
                                                        Edit
                                                    </Button>
                                                    <Button
                                                        sx={{
                                                            display: "flex",
                                                        }}
                                                        variant="contained"
                                                        size="small"
                                                        color="primary"
                                                        onClick={() => {
                                                            setShowModal(true);
                                                            setRecordForEdit(
                                                                null
                                                            );
                                                            setModalTitle(
                                                                "Create Event"
                                                            );
                                                        }}
                                                    >
                                                        Add New
                                                    </Button>
                                                </Grid>
                                            )}
                                        </Box>
                                    }
                                ></CardHeader>

                                <CardContent sx={{ pt: 0 }}>
                                    {/* {isAuth ? ( */}

                                    <Grid container spacing={2}>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={4}
                                        >
                                            {/* <CardMedia
                                                component="img"
                                                // height="240"
                                                image={imageUrl}
                                                alt={event?.name}
                                            /> */}
                                            <CardMedia
                                                component="div"
                                                sx={{
                                                    height: {
                                                        xs: "404px",
                                                        sm: "480px",
                                                    },
                                                }}
                                                children={
                                                    <div
                                                        dangerouslySetInnerHTML={{
                                                            __html: `
                                                                <Image
                                                                    fill=${true}
                                                                    src=${encodeURI(
                                                                        imageUrl
                                                                    )}
                                                                    alt=${
                                                                        event?.name
                                                                    }
                                                                    quality=${80}
                                                                />
                                                            `,
                                                        }}
                                                    ></div>
                                                }
                                            />
                                        </Grid>
                                        {!isLoading ? (
                                            <Grid
                                                item
                                                xs={12}
                                                sm={12}
                                                md={6}
                                                lg={8}
                                            >
                                                <Grid
                                                    container
                                                    spacing={2}
                                                    sx={{
                                                        width: "100%",
                                                        display: "flex",
                                                        mb: 6,
                                                    }}
                                                >
                                                    <Grid item xs={12} sm={6}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Event Name
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {event.name}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Category
                                                        </Typography>

                                                        {event?.categories
                                                            .length > 0 ? (
                                                            event.categories.map(
                                                                (item, i) => (
                                                                    <Typography
                                                                        key={i}
                                                                        variant="body1"
                                                                    >
                                                                        {
                                                                            item.name
                                                                        }
                                                                    </Typography>
                                                                )
                                                            )
                                                        ) : (
                                                            <Typography variant="body1">
                                                                N/A{" "}
                                                            </Typography>
                                                        )}
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={12}
                                                        md={12}
                                                        lg={12}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Address:
                                                        </Typography>
                                                        {event?.address ? (
                                                            <Box
                                                                sx={{
                                                                    displa: "flex",
                                                                }}
                                                            >
                                                                {event
                                                                    ?.address[0]
                                                                    ?.addressLine1 && (
                                                                    <Typography variant="body1">
                                                                        {
                                                                            event
                                                                                ?.address[0]
                                                                                ?.addressLine1
                                                                        }
                                                                        ,{" "}
                                                                    </Typography>
                                                                )}

                                                                {event
                                                                    ?.address[0]
                                                                    ?.addressLine2 && (
                                                                    <Typography variant="body1">
                                                                        {
                                                                            event
                                                                                ?.address[0]
                                                                                ?.addressLine2
                                                                        }
                                                                        ,{" "}
                                                                    </Typography>
                                                                )}

                                                                {event
                                                                    ?.address[0]
                                                                    ?.addressLine3 && (
                                                                    <Typography variant="body1">
                                                                        {
                                                                            event
                                                                                ?.address[0]
                                                                                ?.addressLine3
                                                                        }
                                                                        ,{" "}
                                                                    </Typography>
                                                                )}

                                                                {event
                                                                    ?.address[0]
                                                                    ?.city && (
                                                                    <Typography variant="body1">
                                                                        {
                                                                            event
                                                                                ?.address[0]
                                                                                ?.city
                                                                        }
                                                                        ,{" "}
                                                                    </Typography>
                                                                )}

                                                                {event
                                                                    ?.address[0]
                                                                    ?.postcode && (
                                                                    <Typography variant="body1">
                                                                        {
                                                                            event
                                                                                ?.address[0]
                                                                                ?.postcode
                                                                        }
                                                                        ,{" "}
                                                                    </Typography>
                                                                )}

                                                                {event
                                                                    ?.address[0]
                                                                    ?.county && (
                                                                    <Typography variant="body1">
                                                                        {
                                                                            event
                                                                                ?.address[0]
                                                                                ?.county
                                                                        }
                                                                        ,{" "}
                                                                    </Typography>
                                                                )}

                                                                {event
                                                                    ?.address[0]
                                                                    ?.region && (
                                                                    <Typography variant="body1">
                                                                        {
                                                                            event
                                                                                ?.address[0]
                                                                                ?.region
                                                                        }
                                                                        ,{" "}
                                                                    </Typography>
                                                                )}
                                                                {event
                                                                    ?.address[0]
                                                                    ?.country
                                                                    .length >
                                                                    0 && (
                                                                    <Typography variant="body1">
                                                                        {
                                                                            event
                                                                                ?.address[0]
                                                                                ?.country[0]
                                                                                .name
                                                                        }
                                                                    </Typography>
                                                                )}
                                                            </Box>
                                                        ) : (
                                                            <Typography variant="body1">
                                                                N/A
                                                            </Typography>
                                                        )}
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={12}
                                                        md={12}
                                                        lg={12}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Date and Time
                                                        </Typography>
                                                        <Box
                                                            sx={{
                                                                display: "flex",
                                                            }}
                                                        >
                                                            <Box
                                                                sx={{
                                                                    display:
                                                                        "flex",
                                                                }}
                                                            >
                                                                {event.startDate ? (
                                                                    <Typography variant="body1">
                                                                        {dayjs(
                                                                            event.startDate
                                                                        ).format(
                                                                            "D MMM YYYY"
                                                                        )}
                                                                    </Typography>
                                                                ) : (
                                                                    <Typography variant="body1">
                                                                        N/A
                                                                    </Typography>
                                                                )}

                                                                {event.endDate && (
                                                                    <Typography variant="body1">
                                                                        &nbsp;
                                                                        -&nbsp;
                                                                        {dayjs(
                                                                            event.endDate
                                                                        ).format(
                                                                            "D MMM YYYY"
                                                                        )}
                                                                    </Typography>
                                                                )}
                                                            </Box>

                                                            <Box
                                                                sx={{
                                                                    display:
                                                                        "flex",
                                                                }}
                                                            >
                                                                <Typography variant="body1">
                                                                    {event.startTime && (
                                                                        <span>
                                                                            &nbsp;@&nbsp;
                                                                            {dayjs(
                                                                                event.startTime
                                                                            ).format(
                                                                                "h:mm A"
                                                                            )}
                                                                        </span>
                                                                    )}
                                                                </Typography>

                                                                {event.endTime && (
                                                                    <Typography variant="body1">
                                                                        &nbsp;-{" "}
                                                                        {dayjs(
                                                                            event.endTime
                                                                        ).format(
                                                                            "h:mm A"
                                                                        )}
                                                                    </Typography>
                                                                )}
                                                            </Box>
                                                        </Box>
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Short Description
                                                        </Typography>

                                                        {event.shortDescription ? (
                                                            <Typography variant="body1">
                                                                {
                                                                    event.shortDescription
                                                                }
                                                            </Typography>
                                                        ) : (
                                                            <Typography variant="body1">
                                                                N/A
                                                            </Typography>
                                                        )}
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Description
                                                        </Typography>

                                                        {event.description ? (
                                                            <div
                                                                dangerouslySetInnerHTML={{
                                                                    __html: event.description,
                                                                }}
                                                            />
                                                        ) : (
                                                            <Typography variant="body1">
                                                                N/A
                                                            </Typography>
                                                        )}
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={6}
                                                        lg={6}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Created By:
                                                        </Typography>
                                                        <Typography
                                                            component="p"
                                                            variant="body1"
                                                        >
                                                            {
                                                                event.createdBy
                                                                    ?.firstName
                                                            }{" "}
                                                            {
                                                                event.createdBy
                                                                    ?.lastName
                                                            }
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={6}
                                                        lg={6}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Updated By:
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {
                                                                event.updatedBy
                                                                    ?.firstName
                                                            }{" "}
                                                            {
                                                                event.updatedBy
                                                                    ?.lastName
                                                            }
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        ) : (
                                            <Loading />
                                        )}
                                    </Grid>
                                    {/* ) : (
                                        <LoginOrRegister />
                                    )} */}
                                </CardContent>
                            </Card>

                            {/* Contact Details */}
                            <Card>
                                <CardHeader title="Contact"></CardHeader>
                                <CardContent sx={{ pt: 0 }}>
                                    <Grid container>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Contact Person:
                                            </Typography>

                                            {event.contactPersonName ? (
                                                <Typography variant="body1">
                                                    {event.contactPersonName}
                                                </Typography>
                                            ) : (
                                                <Typography variant="body1">
                                                    N/A
                                                </Typography>
                                            )}
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Email:
                                            </Typography>

                                            {event.contactEmail ? (
                                                <Typography variant="body1">
                                                    {event.contactEmail}
                                                </Typography>
                                            ) : (
                                                <Typography variant="body1">
                                                    N/A
                                                </Typography>
                                            )}
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Mobile:
                                            </Typography>

                                            {event.contactMobile ? (
                                                <Typography variant="body1">
                                                    {event.contactMobile}
                                                </Typography>
                                            ) : (
                                                <Typography variant="body1">
                                                    N/A
                                                </Typography>
                                            )}
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Phone:
                                            </Typography>

                                            {event.contactPhone ? (
                                                <Typography variant="body1">
                                                    {event.contactPhone}
                                                </Typography>
                                            ) : (
                                                <Typography variant="body1">
                                                    N/A
                                                </Typography>
                                            )}
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={6}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                URL 1:
                                            </Typography>

                                            {event.url1 ? (
                                                <Typography variant="body1">
                                                    {event.url1}
                                                </Typography>
                                            ) : (
                                                <Typography variant="body1">
                                                    N/A
                                                </Typography>
                                            )}
                                        </Grid>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={6}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                URL 2:
                                            </Typography>

                                            {event.url2 ? (
                                                <Typography variant="body1">
                                                    {event.url2}
                                                </Typography>
                                            ) : (
                                                <Typography variant="body1">
                                                    N/A
                                                </Typography>
                                            )}
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Container>
                    </Box>
                ) : (
                    <Loading />
                )}
            </IonContent>
        </>
    );
}

// export async function getStaticPaths(props) {
//     console.log("getStaticPaths props");
//     console.log(props.locales);
//     console.log(props.defaultLocale);
//     const eventsRawData = await getPublicData(
//         "events?take=10&skip=0&sortBy=createdAt&sortOrder=desc"
//     );

//     console.log("getStaticPaths eventsRawData");
//     // console.log(eventsRawData);

//     // Get the paths we want to pre-render based on areas
//     const slugs = eventsRawData.data.map((item) => ({
//         params: {
//             slug: item.slug,
//             area: item?.areas[0]?.slug || item?.areas[1]?.slug || "all",
//         },
//     }));
//     console.log("slugs");
//     console.log(slugs);

//     // Pass present information on the page through props
//     return {
//         paths: slugs,
//         fallback: false,
//     };
// }

// // {params} || context
// export async function getStaticProps({ params }) {
//     console.log("getStaticProps params");
//     console.log(params);

//     const { slug } = params;
//     const { area } = params;
//     console.log("getStaticProps slug");
//     console.log(slug);
//     console.log(area);

//     const apiUrlParams = `events/${slug}`;
//     const event = await getPublicData(apiUrlParams);

//     console.log("getStaticProps data");
//     console.log(event);

//     const headMetaTitle = `${event.name} - Event in ${event.address[0].city}, ${
//         event.areas[0].name !== "all"
//             ? pascalCaseFormatter(event.areas[0].name)
//             : ""
//     } area`;

//     const headMetaDescription = `${event.shortDescription} || Event in the ${
//         event.areas[0].name !== "all"
//             ? pascalCaseFormatter(event.areas[0].name) + " "
//             : ""
//     } area`;
//     const headMetaImage = event?.featuredImageUrl;
//     const headMetaType = "article";

//     return {
//         props: {
//             // event: event,
//             head: {
//                 title: headMetaTitle,
//                 description: headMetaDescription,
//                 image: headMetaImage,
//                 type: headMetaType,
//             },
//         },
//     };
// }

export const getServerSideProps = async (context) => {
    const { slug, area } = context.params;
    console.log("event getServerSideProps slug");
    console.log(slug);
    console.log(area);

    const apiUrlParams = `events/${slug}`;
    const rawData = await getPublicData(apiUrlParams);

    console.log("event getServerSideProps data");
    console.log(rawData);

    const headMetaTitle = `${rawData.name} - Event in ${
        rawData.address[0].city
    }, ${
        rawData.areas[0].name !== "all"
            ? pascalCaseFormatter(rawData.areas[0].name)
            : ""
    } area`;

    const headMetaDescription = `${rawData.shortDescription} || Event in the ${
        rawData.areas[0].name !== "all"
            ? pascalCaseFormatter(rawData.areas[0].name)
            : ""
    } area`;
    const headMetaImage = rawData?.featuredImageUrl;
    const headMetaType = "article";

    return {
        props: {
            // data: rawData,
            head: {
                title: headMetaTitle,
                description: headMetaDescription,
                image: headMetaImage,
                type: headMetaType,
            },
        },
    };
};
