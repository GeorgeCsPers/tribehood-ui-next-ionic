import { useRouter } from "next/router";
import HeadShared from "../../../../components/head-shared";
import HeaderSecondary from "../../../../components/header/header-secondary";
import FullscreenModal from "../../../../components/modals/modal-fullscreen";
import { useEffect, useState } from "react";
import authService from "../../../../services/auth.service";
import { IonContent, IonItem } from "@ionic/react";
import {
    Container,
    Button,
    Grid,
    Card,
    CardContent,
    Box,
    CardMedia,
    CardHeader,
    Typography,
} from "@mui/material";
import { getData, getPublicData } from "../../../../services/data.service";
import Loading from "../../../../components/common/loading";
import LoginOrRegister from "../../../../components/login-or-register";

import isAllowedToManageChecker from "../../../../components/hooks/is-allowed-to-manage";
import PublicNoticesForm from "../../../../components/forms/public-notices-form";
import { pascalCaseFormatter } from "../../../../components/hooks/text-formatter";

// Assets URL
const serverUrl = process.env.NEXT_PUBLIC_ASSETS_URL;

export default function PublicNoticePage() {
    const router = useRouter();
    const { query } = router || {};
    const { area = query.area || "all" } = query || {};
    // const area = query.area || "all";
    const { slug } = query || {};
    console.log("PublicNoticePage query.area");
    console.log(query.area);
    console.log(area);
    console.log(slug);

    const [isLoading, setIsLoading] = useState<boolean>(true);

    // - - - - - - - -
    // Check if user logged in and token is valid
    // - - - - - - - -
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        // if (!isToken) {
        //     router.push("/users/login");
        // }
    };

    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
    };

    // - - - - - - -
    // GET APIs Data
    // - - - - - - -
    const [publicNotice, setPublicNotice] = useState<any>({});
    const [isAllowedToManage, setIsAllowedToManage] = useState<boolean>(false);
    const getAllData = async (slug) => {
        const publicNoticesRawData = await getPublicData(
            `public-notices/${slug}`
        );
        setPublicNotice(publicNoticesRawData);

        // Check if user allowed to manage
        if (isAuth) {
            const isAllowed = isAllowedToManageChecker(
                userDetails,
                publicNoticesRawData
            );
            setIsAllowedToManage(isAllowed);
        }
        console.log("isAllowedToManage");
        console.log(isAllowedToManage);

        setIsLoading(false);
    };

    useEffect(() => {
        if (slug && area) {
            (async () => {
                await checkToken();
                await checkUser();
                // if (isAuth) {
                await getAllData(slug);
                // }
            })();
        }
    }, [area, isAuth, slug]);

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    // - - - - - - - - -
    // Image URL adjustment
    // - - - - - - - - -
    let imageUrl = "/assets/images/TribeHood-better-connected-community.webp";
    if (
        publicNotice?.featuredImageUrl &&
        publicNotice?.featuredImageUrl.indexOf("http") > -1
    ) {
        imageUrl = publicNotice?.featuredImageUrl;
    } else if (publicNotice?.featuredImageUrl) {
        imageUrl = serverUrl + publicNotice?.featuredImageUrl;
    }

    console.log("Single publicNotice");
    console.log(publicNotice);

    return (
        <>
            {/* {!isLoading && (
                <HeadShared
                    title={
                        publicNotice.name +
                        " - Publlic Notice in " +
                        publicNotice.address[0].city +
                        ", " +
                        (publicNotice.areas[0].name !== "all"
                            ? pascalCaseFormatter(publicNotice.areas[0].name)
                            : "") +
                        " area"
                    }
                    description={
                        publicNotice.shortDescription +
                        " || Publlic Notice in the " +
                        (publicNotice.areas[0].name !== "all"
                            ? pascalCaseFormatter(publicNotice.areas[0].name) +
                              " "
                            : "") +
                        "area"
                    }
                    image={imageUrl}
                ></HeadShared>
            )} */}

            {isAuth && isAllowedToManage && (
                <FullscreenModal
                    title={modalTitle}
                    showModal={showModal}
                    setShowModal={setShowModal}
                >
                    {!isLoading && (
                        <PublicNoticesForm
                            recordForEdit={recordForEdit}
                            userId={userDetails?.id}
                            userDetails={userDetails}
                        />
                    )}
                </FullscreenModal>
            )}

            <IonContent>
                <HeaderSecondary
                    addNew="publicNotice"
                    item={publicNotice}
                    isBack={true}
                    isSingleView={true}
                    isAuth={isAuth}
                    userDetails={userDetails}
                    image={imageUrl}
                ></HeaderSecondary>

                {!isLoading ? (
                    <Box sx={{ mt: 4, pb: 4 }}>
                        <Container sx={{ mt: 4, pb: 4 }}>
                            <Card sx={{ mb: 4 }}>
                                <CardHeader
                                    title="Details"
                                    // subheader=""
                                    action={
                                        <Box>
                                            {isAllowedToManage && (
                                                <Grid
                                                    item
                                                    sx={{
                                                        display: "flex",
                                                        alignItems: "center",
                                                        justifyContent: "end",
                                                    }}
                                                >
                                                    <Button
                                                        sx={{
                                                            display: "flex",
                                                            mr: 2,
                                                        }}
                                                        variant="contained"
                                                        size="small"
                                                        color="primary"
                                                        onClick={() => {
                                                            setShowModal(true);
                                                            setRecordForEdit(
                                                                publicNotice
                                                            );
                                                            setModalTitle(
                                                                "Edit Public Notice"
                                                            );
                                                        }}
                                                    >
                                                        Edit
                                                    </Button>
                                                    <Button
                                                        sx={{
                                                            display: "flex",
                                                        }}
                                                        variant="contained"
                                                        size="small"
                                                        color="primary"
                                                        onClick={() => {
                                                            setShowModal(true);
                                                            setRecordForEdit(
                                                                null
                                                            );
                                                            setModalTitle(
                                                                "Add New Public Notice"
                                                            );
                                                        }}
                                                    >
                                                        Add New
                                                    </Button>
                                                </Grid>
                                            )}
                                        </Box>
                                    }
                                ></CardHeader>

                                <CardContent sx={{ pt: 0 }}>
                                    {/* {isAuth ? ( */}

                                    <Grid container spacing={2}>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={4}
                                        >
                                            <CardMedia
                                                component="img"
                                                // height="240"
                                                image={imageUrl}
                                                alt={publicNotice?.name}
                                            />
                                        </Grid>
                                        {!isLoading ? (
                                            <Grid
                                                item
                                                xs={12}
                                                sm={12}
                                                md={6}
                                                lg={8}
                                            >
                                                <Grid
                                                    container
                                                    spacing={2}
                                                    sx={{
                                                        width: "100%",
                                                        display: "flex",
                                                        mb: 6,
                                                    }}
                                                >
                                                    <Grid item xs={12} sm={6}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Event Name
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {publicNotice.name}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12} sm={6}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Category
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {publicNotice
                                                                ?.categories
                                                                .length > 0 ? (
                                                                publicNotice.categories.map(
                                                                    (
                                                                        item,
                                                                        i
                                                                    ) => (
                                                                        <span
                                                                            key={
                                                                                i
                                                                            }
                                                                        >
                                                                            {
                                                                                item.name
                                                                            }
                                                                        </span>
                                                                    )
                                                                )
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={12}
                                                        md={12}
                                                        lg={12}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Address:
                                                        </Typography>
                                                        {publicNotice?.address ? (
                                                            <Typography variant="body1">
                                                                {publicNotice
                                                                    ?.address[0]
                                                                    ?.addressLine1 && (
                                                                    <span>
                                                                        {
                                                                            publicNotice
                                                                                ?.address[0]
                                                                                ?.addressLine1
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {publicNotice
                                                                    ?.address[0]
                                                                    ?.addressLine2 && (
                                                                    <span>
                                                                        {
                                                                            publicNotice
                                                                                ?.address[0]
                                                                                ?.addressLine2
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {publicNotice
                                                                    ?.address[0]
                                                                    ?.addressLine3 && (
                                                                    <span>
                                                                        {
                                                                            publicNotice
                                                                                ?.address[0]
                                                                                ?.addressLine3
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {publicNotice
                                                                    ?.address[0]
                                                                    ?.city && (
                                                                    <span>
                                                                        {
                                                                            publicNotice
                                                                                ?.address[0]
                                                                                ?.city
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {publicNotice
                                                                    ?.address[0]
                                                                    ?.postcode && (
                                                                    <span>
                                                                        {
                                                                            publicNotice
                                                                                ?.address[0]
                                                                                ?.postcode
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {publicNotice
                                                                    ?.address[0]
                                                                    ?.county && (
                                                                    <span>
                                                                        {
                                                                            publicNotice
                                                                                ?.address[0]
                                                                                ?.county
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}

                                                                {publicNotice
                                                                    ?.address[0]
                                                                    ?.region && (
                                                                    <span>
                                                                        {
                                                                            publicNotice
                                                                                ?.address[0]
                                                                                ?.region
                                                                        }
                                                                        ,{" "}
                                                                    </span>
                                                                )}
                                                                {publicNotice
                                                                    ?.address[0]
                                                                    ?.country
                                                                    .length >
                                                                    0 && (
                                                                    <span>
                                                                        {
                                                                            publicNotice
                                                                                ?.address[0]
                                                                                ?.country[0]
                                                                                .name
                                                                        }
                                                                    </span>
                                                                )}
                                                            </Typography>
                                                        ) : (
                                                            <span>N/A</span>
                                                        )}
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Short Description
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {publicNotice.shortDescription ? (
                                                                <span>
                                                                    {
                                                                        publicNotice.shortDescription
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Description
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {publicNotice.description ? (
                                                                <span>
                                                                    {
                                                                        publicNotice.description
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={6}
                                                        lg={6}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Created By:
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {
                                                                publicNotice
                                                                    .createdBy
                                                                    ?.firstName
                                                            }{" "}
                                                            {
                                                                publicNotice
                                                                    .createdBy
                                                                    ?.lastName
                                                            }
                                                        </Typography>
                                                    </Grid>

                                                    <Grid
                                                        item
                                                        xs={12}
                                                        sm={6}
                                                        md={6}
                                                        lg={6}
                                                    >
                                                        <Typography
                                                            variant="caption"
                                                            className="txt-label"
                                                        >
                                                            Updated By:
                                                        </Typography>
                                                        <Typography variant="body1">
                                                            {publicNotice
                                                                .updatedBy
                                                                ?.firstName ? (
                                                                <span>
                                                                    {
                                                                        publicNotice
                                                                            .updatedBy
                                                                            ?.firstName
                                                                    }{" "}
                                                                    {
                                                                        publicNotice
                                                                            .updatedBy
                                                                            ?.lastName
                                                                    }
                                                                </span>
                                                            ) : (
                                                                <span>N/A</span>
                                                            )}
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        ) : (
                                            <Loading />
                                        )}
                                    </Grid>
                                    {/* ) : (
                                        <LoginOrRegister />
                                    )} */}
                                </CardContent>
                            </Card>

                            {/* Contact Details */}
                            <Card>
                                <CardHeader title="Contact"></CardHeader>
                                <CardContent sx={{ pt: 0 }}>
                                    <Grid container>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Contact Person:
                                            </Typography>
                                            <Typography variant="body1">
                                                {publicNotice.contactPersonName ? (
                                                    <span>
                                                        {
                                                            publicNotice.contactPersonName
                                                        }
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Email:
                                            </Typography>
                                            <Typography variant="body1">
                                                {publicNotice.contactEmail ? (
                                                    <span>
                                                        {
                                                            publicNotice.contactEmail
                                                        }
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Mobile:
                                            </Typography>
                                            <Typography variant="body1">
                                                {publicNotice.contactMobile ? (
                                                    <span>
                                                        {
                                                            publicNotice.contactMobile
                                                        }
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={6}
                                            md={4}
                                            lg={3}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                Phone:
                                            </Typography>
                                            <Typography variant="body1">
                                                {publicNotice.contactPhone ? (
                                                    <span>
                                                        {
                                                            publicNotice.contactPhone
                                                        }
                                                    </span>
                                                ) : (
                                                    <span>N/A</span>
                                                )}
                                            </Typography>
                                        </Grid>

                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={6}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                URL 1:
                                            </Typography>
                                            <Typography variant="body1"></Typography>
                                            {publicNotice.url1 ? (
                                                <span>{publicNotice.url1}</span>
                                            ) : (
                                                <span>N/A</span>
                                            )}
                                        </Grid>
                                        <Grid
                                            item
                                            xs={12}
                                            sm={12}
                                            md={6}
                                            lg={6}
                                            sx={{ mb: 1 }}
                                        >
                                            <Typography
                                                variant="caption"
                                                className="txt-label"
                                            >
                                                URL 2:
                                            </Typography>
                                            <Typography variant="body1"></Typography>
                                            {publicNotice.url2 ? (
                                                <span>{publicNotice.url2}</span>
                                            ) : (
                                                <span>N/A</span>
                                            )}
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Container>
                    </Box>
                ) : (
                    <Loading />
                )}
            </IonContent>
        </>
    );
}

export const getServerSideProps = async (context) => {
    const { slug, area } = context.params;
    console.log("public-notice getServerSideProps slug");
    console.log(slug);
    console.log(area);

    const apiUrlParams = `public-notices/${slug}`;
    const event = await getPublicData(apiUrlParams);

    console.log("public-notice getServerSideProps data");
    console.log(event);

    const headMetaTitle = `${event.name} - Public Notice in ${
        event.address[0].city
    }, ${
        event.areas[0].name !== "all"
            ? pascalCaseFormatter(event.areas[0].name)
            : ""
    } area`;

    const headMetaDescription = `${
        event.shortDescription
    } || Public Notice in the ${
        event.areas[0].name !== "all"
            ? pascalCaseFormatter(event.areas[0].name)
            : ""
    } area`;
    const headMetaImage = event?.featuredImageUrl;
    const headMetaType = "article";

    return {
        props: {
            // event: event,
            head: {
                title: headMetaTitle,
                description: headMetaDescription,
                image: headMetaImage,
                type: headMetaType,
            },
        },
    };
};
