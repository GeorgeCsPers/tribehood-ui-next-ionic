import { useRouter } from "next/router";
import HeadShared from "../../../../components/head-shared";
import HeaderSecondary from "../../../../components/header/header-secondary";
import FullscreenModal from "../../../../components/modals/modal-fullscreen";
import { useEffect, useState } from "react";
import authService from "../../../../services/auth.service";
import { IonContent, IonItem } from "@ionic/react";
import {
    Container,
    Button,
    Grid,
    Card,
    CardContent,
    Box,
    CardMedia,
} from "@mui/material";
import { getData, getPublicData } from "../../../../services/data.service";
import Loading from "../../../../components/common/loading";
import LoginOrRegister from "../../../../components/login-or-register";
import ProjectsForm from "../../../../components/forms/projects-form";
import isAllowedToManageChecker from "../../../../components/hooks/is-allowed-to-manage";
import { pascalCaseFormatter } from "../../../../components/hooks/text-formatter";

// Assets URL
const serverUrl = process.env.NEXT_PUBLIC_ASSETS_URL;

export default function ProjectPage() {
    const router = useRouter();
    const { query } = router || {};
    const { area = query.area || "all" } = query || {};
    // const area = query.area || "all";
    const { slug } = query || {};
    console.log("ProjectPage query.area");
    console.log(query.area);
    console.log(area);
    console.log(slug);

    const [isLoading, setIsLoading] = useState<boolean>(true);

    // - - - - - - - -
    // Check if user logged in and token is valid
    // - - - - - - - -
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        // if (!isToken) {
        //     router.push("/users/login");
        // }
    };

    // - - - - - - -
    // Get Logged in User Details
    // - - - - - - -
    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
    };

    // - - - - - - -
    // GET APIs Data
    // - - - - - - -
    const [project, setProject] = useState<any>({});
    const [isAllowedToManage, setIsAllowedToManage] = useState<boolean>(false);
    const getAllData = async (slug) => {
        const projectsRawData = await getPublicData(`projects/${slug}`);
        setProject(projectsRawData);

        // Check if user allowed to manage
        if (isAuth) {
            const isAllowed = isAllowedToManageChecker(
                userDetails,
                projectsRawData
            );
            setIsAllowedToManage(isAllowed);
        }
        console.log("isAllowedToManage");
        console.log(isAllowedToManage);

        setIsLoading(false);
    };

    useEffect(() => {
        if (slug && area) {
            (async () => {
                await checkToken();
                await checkUser();
                // if (isAuth) {
                await getAllData(slug);
                // }
            })();
        }
    }, [area, isAuth, slug]);

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    // - - - - - - - - -
    // Image URL adjustment
    // - - - - - - - - -
    let imageUrl = "/assets/images/TribeHood-better-connected-community.webp";
    if (
        project?.featuredImageUrl &&
        project?.featuredImageUrl.indexOf("http") > -1
    ) {
        imageUrl = project?.featuredImageUrl;
    } else if (project?.featuredImageUrl) {
        imageUrl = serverUrl + project?.featuredImageUrl;
    }

    console.log("Single Project");
    console.log(project);

    return (
        <>
            {/* {!isLoading && (
                <HeadShared
                    title={
                        project.name +
                        " - Project in " +
                        project.address[0].city +
                        ", " +
                        (project.areas[0].name !== "all"
                            ? pascalCaseFormatter(project.areas[0].name)
                            : "") +
                        " area"
                    }
                    description={
                        project.shortDescription +
                        " || Project in the " +
                        (project.areas[0].name !== "all"
                            ? pascalCaseFormatter(project.areas[0].name) + " "
                            : "") +
                        "area"
                    }
                    image={imageUrl}
                ></HeadShared>
            )} */}

            {isAuth && isAllowedToManage && (
                <FullscreenModal
                    title={modalTitle}
                    showModal={showModal}
                    setShowModal={setShowModal}
                >
                    {!isLoading && (
                        <ProjectsForm
                            recordForEdit={recordForEdit}
                            userId={userDetails?.id}
                            userDetails={userDetails}
                        />
                    )}
                </FullscreenModal>
            )}

            <IonContent>
                <HeaderSecondary
                    title={project?.name}
                    isBack={true}
                    addNew="project"
                    isAuth={isAuth}
                    userDetails={userDetails}
                ></HeaderSecondary>

                {!isLoading ? (
                    <Box>
                        <Box className="single-view-header">
                            <CardMedia
                                component="img"
                                height="240"
                                image={imageUrl}
                                alt={project?.name}
                            />
                            <Container sx={{ mt: 4, pb: 4 }}>
                                <Box className="single-view-header-content">
                                    {project?.name}
                                </Box>
                            </Container>
                        </Box>

                        <Container sx={{ mt: 4, pb: 4 }}>
                            <Card>
                                <CardContent>
                                    {isAuth ? (
                                        <Grid container spacing={2}>
                                            <Grid
                                                item
                                                xs={12}
                                                sm={12}
                                                md={6}
                                                lg={4}
                                            >
                                                <CardMedia
                                                    component="img"
                                                    // height="240"
                                                    image={imageUrl}
                                                    alt={project?.name}
                                                />
                                            </Grid>
                                            {!isLoading ? (
                                                <Grid
                                                    item
                                                    xs={12}
                                                    sm={12}
                                                    md={6}
                                                    lg={8}
                                                >
                                                    {isAllowedToManage && (
                                                        <Grid
                                                            item
                                                            sx={{
                                                                display: "flex",
                                                                alignItems:
                                                                    "center",
                                                                justifyContent:
                                                                    "end",
                                                            }}
                                                        >
                                                            <Button
                                                                sx={{
                                                                    display:
                                                                        "flex",
                                                                    mb: 4,
                                                                    mr: 2,
                                                                }}
                                                                variant="contained"
                                                                size="small"
                                                                color="primary"
                                                                onClick={() => {
                                                                    setShowModal(
                                                                        true
                                                                    );
                                                                    setRecordForEdit(
                                                                        project
                                                                    );
                                                                    setModalTitle(
                                                                        "Edit Project"
                                                                    );
                                                                }}
                                                            >
                                                                Edit
                                                            </Button>
                                                            <Button
                                                                sx={{
                                                                    display:
                                                                        "flex",
                                                                    mb: 4,
                                                                }}
                                                                variant="contained"
                                                                size="small"
                                                                color="primary"
                                                                onClick={() => {
                                                                    setShowModal(
                                                                        true
                                                                    );
                                                                    setRecordForEdit(
                                                                        null
                                                                    );
                                                                    setModalTitle(
                                                                        "Add Project"
                                                                    );
                                                                }}
                                                            >
                                                                Add New
                                                            </Button>
                                                        </Grid>
                                                    )}

                                                    <Grid
                                                        container
                                                        spacing={2}
                                                        sx={{
                                                            width: "100%",
                                                            display: "flex",
                                                            mb: 6,
                                                        }}
                                                    >
                                                        <Grid item xs={12}>
                                                            {project.name}
                                                        </Grid>
                                                        <Grid item>
                                                            {
                                                                project.description
                                                            }
                                                        </Grid>

                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project.contactEmail
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project.contactMobile
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project.contactPhone
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project.contactPersonName
                                                            }
                                                        </Grid>
                                                        <Grid item xs={12}>
                                                            {project.websiteUrl}
                                                        </Grid>
                                                        <Grid item xs={12}>
                                                            {project.otherUrl}
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project.featuredImageUrl
                                                            }
                                                        </Grid>

                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    ?.address[0]
                                                                    ?.addressLine1
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    ?.address[0]
                                                                    ?.addressLine2
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    ?.address[0]
                                                                    ?.addressLine3
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    ?.address[0]
                                                                    ?.city
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    ?.address[0]
                                                                    ?.county
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    ?.address[0]
                                                                    ?.region
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    ?.address[0]
                                                                    ?.country
                                                                    .name
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project.areas[0]
                                                                    ?.name
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    .categories[0]
                                                                    ?.name
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    .categoryTypes[0]
                                                                    ?.name
                                                            }
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {project.createdAt}
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {project.updatedAt}
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    .createdBy
                                                                    ?.firstName
                                                            }{" "}
                                                            {
                                                                project
                                                                    .createdBy
                                                                    ?.lastName
                                                            }
                                                        </Grid>

                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={6}
                                                            md={4}
                                                            lg={3}
                                                        >
                                                            {
                                                                project
                                                                    .updatedBy
                                                                    ?.firstName
                                                            }{" "}
                                                            {
                                                                project
                                                                    .updatedBy
                                                                    ?.lastName
                                                            }
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            ) : (
                                                <Loading />
                                            )}
                                        </Grid>
                                    ) : (
                                        <LoginOrRegister />
                                    )}
                                </CardContent>
                            </Card>
                        </Container>
                    </Box>
                ) : (
                    <Loading />
                )}
            </IonContent>
        </>
    );
}

export const getServerSideProps = async (context) => {
    const { slug, area } = context.params;
    console.log("project getServerSideProps slug");
    console.log(slug);
    console.log(area);

    const apiUrlParams = `projects/${slug}`;
    const rawData = await getPublicData(apiUrlParams);

    console.log("project getServerSideProps data");
    console.log(rawData);

    const headMetaTitle = `${rawData.name} - Project in ${
        rawData.address[0].city
    }, ${
        rawData.areas[0].name !== "all"
            ? pascalCaseFormatter(rawData.areas[0].name)
            : ""
    } area`;

    const headMetaDescription = `${
        rawData.shortDescription
    } || Project in the ${
        rawData.areas[0].name !== "all"
            ? pascalCaseFormatter(rawData.areas[0].name)
            : ""
    } area`;
    const headMetaImage = rawData?.featuredImageUrl;
    const headMetaType = "article";

    return {
        props: {
            // data: rawData,
            head: {
                title: headMetaTitle,
                description: headMetaDescription,
                image: headMetaImage,
                type: headMetaType,
            },
        },
    };
};
