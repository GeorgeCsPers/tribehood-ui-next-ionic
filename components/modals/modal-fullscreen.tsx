import {
    IonModal,
    IonHeader,
    IonToolbar,
    IonButtons,
    IonButton,
    IonTitle,
    IonContent,
    IonItem,
    IonLabel,
    IonInput,
    IonIcon,
    IonText,
} from "@ionic/react";
import { closeCircleOutline } from "ionicons/icons";
import { useRef, useState } from "react";
import NonSSRWrapper from "../NoSSRWrapper";
import {
    Dialog,
    AppBar,
    Container,
    Toolbar,
    Typography,
    IconButton,
    DialogContent,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

export default function FullscreenModal(props: any) {
    console.log("FullscreenModal");
    console.log(props);

    const { title, children, showModal, setShowModal } = props;

    return (
        <>
            <Dialog fullScreen open={showModal}>
                {/* <Dialog fullScreen open={showModal} TransitionComponent={Transition}> */}
                <AppBar color="inherit" sx={{ position: "relative" }}>
                    <Container>
                        <Toolbar>
                            <Typography
                                sx={{ flex: 1 }}
                                component="h5"
                                variant="h5"
                            >
                                {title}
                            </Typography>

                            <IconButton
                                size="large"
                                color="inherit"
                                onClick={() => {
                                    setShowModal(false);
                                }}
                                aria-label="close"
                            >
                                <CloseIcon />
                            </IconButton>
                        </Toolbar>
                    </Container>
                </AppBar>

                <DialogContent sx={{ mt: 2 }}>
                    <Container>{children}</Container>
                </DialogContent>
            </Dialog>
        </>
    );
}
