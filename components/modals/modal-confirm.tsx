import {
    IonModal,
    IonHeader,
    IonToolbar,
    IonButtons,
    IonButton,
    IonTitle,
    IonContent,
    IonItem,
    IonLabel,
    IonInput,
    IonIcon,
    IonText,
} from "@ionic/react";
import { closeCircleOutline } from "ionicons/icons";
import { useRef, useState } from "react";
import NonSSRWrapper from "../NoSSRWrapper";
import {
    Dialog,
    AppBar,
    Container,
    Toolbar,
    Typography,
    IconButton,
    DialogContent,
    Grid,
    Button,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

export default function ModalConfirm(props: any) {
    console.log("ModalConfirm");
    console.log(props);

    const {
        title,
        children,
        showConfirmModal,
        setShowConfirmModal,
        deleteItem,
    } = props;

    return (
        <>
            <Dialog open={showConfirmModal} sx={{ minWidth: "280px" }}>
                {/* <Dialog fullScreen open={openPopup} TransitionComponent={Transition}> */}
                <AppBar color="inherit" sx={{ position: "relative" }}>
                    <Container>
                        <Toolbar>
                            <Typography
                                sx={{ flex: 1 }}
                                component="h5"
                                variant="h5"
                            >
                                {title}
                            </Typography>
                            {/* <IonText>
                                <h4>{title}</h4>
                            </IonText> */}
                            <IconButton
                                size="large"
                                color="inherit"
                                onClick={() => {
                                    setShowConfirmModal(false);
                                }}
                                aria-label="close"
                            >
                                <CloseIcon />
                            </IconButton>
                        </Toolbar>
                    </Container>
                </AppBar>

                <DialogContent sx={{ mt: 2 }}>
                    <Grid
                        container
                        spacing={2}
                        sx={{
                            width: "100%",
                            display: "flex",
                            mb: 2,
                        }}
                    >
                        <Grid
                            item
                            xs={12}
                            sx={{
                                display: "flex",
                                justifyContent: "center",
                                mt: 2,
                                mb: 4,
                            }}
                        >
                            <Typography variant="h5">
                                {props.recordForEdit.name}
                            </Typography>
                        </Grid>

                        <Grid
                            item
                            xs={12}
                            sm={12}
                            md={12}
                            lg={6}
                            sx={{ display: "flex", justifyContent: "center" }}
                        >
                            <Button
                                onClick={() => props.deleteItem(false)}
                                variant="contained"
                                color="inherit"
                            >
                                No, Cancel
                            </Button>
                        </Grid>

                        <Grid
                            item
                            xs={12}
                            sm={12}
                            md={12}
                            lg={6}
                            sx={{ display: "flex", justifyContent: "center" }}
                        >
                            <Button
                                onClick={() => props.deleteItem(true)}
                                variant="contained"
                                color="error"
                            >
                                Yes, Delete
                            </Button>
                        </Grid>
                    </Grid>
                </DialogContent>
            </Dialog>
        </>
    );
}
