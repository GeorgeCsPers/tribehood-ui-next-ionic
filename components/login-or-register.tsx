import { IonCard, IonCardContent, IonCol, IonRow } from "@ionic/react";
import { Typography } from "@mui/material";
import Link from "next/link";

export default function LoginOrRegister(props: any) {
    return (
        <Typography variant="body1" sx={{ textAlign: "center", mt: 4, mb: 4 }}>
            Please{" "}
            <Link href="/users/login">
                <strong>Login</strong>
            </Link>{" "}
            or{" "}
            <Link href="/users/register">
                <strong>Register</strong>
            </Link>{" "}
            to see this content.
        </Typography>
    );
}
