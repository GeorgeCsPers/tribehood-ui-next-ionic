import { Box, Button, Card, CardContent, Typography } from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import { useEffect } from "react";

export default function CardIdeas(props: any) {
    useEffect(() => {
        console.log("CardIdeas");
        console.log(props);
    }, []);

    return (
        <Card className="card-ideas">
            <CardContent>
                <Typography variant="h5">Having some ideas?</Typography>

                <Box
                    sx={{
                        display: "flex",
                        justifyContent: "center",
                        mt: 4,
                        mb: 4,
                    }}
                >
                    <Image
                        src="/assets/icons/TribeHood-ideas-icon.svg"
                        alt="advertise-icon"
                        width={88}
                        height={88}
                    />
                </Box>

                <Box
                    sx={{
                        display: "flex",
                        justifyContent: "center",
                        mt: 4,
                        mb: 2,
                    }}
                >
                    <Link href="/contact">
                        <Button variant="contained" color="primary">
                            Write us now
                        </Button>
                    </Link>
                </Box>
            </CardContent>
        </Card>
    );
}
