import { Card, CardContent, Typography } from "@mui/material";
import Link from "next/link";
import { useEffect } from "react";

export default function CardFooter(props: any) {
    const currentYear = new Date().getFullYear();

    useEffect(() => {
        console.log("CardFooter");
        console.log(props);
    }, []);

    return (
        <Card className="card-footer">
            <CardContent>
                <Typography
                    variant="body2"
                    sx={{ textAlign: "center", mb: 2 }}
                    className="txt-medium"
                >
                    {currentYear} © TribeHood
                    <br />
                    Better Connected Community
                </Typography>
                <Typography variant="body2" sx={{ textAlign: "center" }}>
                    <Link href="/legal/terms-and-conditions">
                        Terms & Conditions
                    </Link>{" "}
                    | <Link href="/legal/privacy-policies">Privacy Policy</Link>
                </Typography>
            </CardContent>
        </Card>
    );
}
