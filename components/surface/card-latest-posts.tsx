import { Card, CardContent, Typography } from "@mui/material";
import { useEffect } from "react";

export default function CardLatestPosts(props: any) {
    useEffect(() => {
        console.log("CardLatestPosts");
        console.log(props);
    }, []);

    return (
        <Card className="card-advertise">
            <CardContent>
                <Typography variant="h5">Latest</Typography>
            </CardContent>
        </Card>
    );
}
