import { Card, CardContent, Typography } from "@mui/material";
import { useEffect } from "react";

export default function CardFeatured(props: any) {
    useEffect(() => {
        console.log("CardFeatured");
        console.log(props);
    }, []);

    return (
        <Card className="card-featured">
            <CardContent>
                <Typography variant="h5">Featured</Typography>
            </CardContent>
        </Card>
    );
}
