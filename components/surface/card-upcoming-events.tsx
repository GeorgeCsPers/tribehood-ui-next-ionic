import { Card, CardContent, Typography } from "@mui/material";
import { useEffect } from "react";

export default function CardUpcomingEvents(props: any) {
    useEffect(() => {
        console.log("CardUpcomingEvents");
        console.log(props);
    }, []);

    return (
        <Card className="card-upcoming-events">
            <CardContent>
                <Typography variant="h5">Upcoming</Typography>
            </CardContent>
        </Card>
    );
}
