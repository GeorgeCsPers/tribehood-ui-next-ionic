import { Box, Button, Card, CardContent, Typography } from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import { useEffect } from "react";

export default function CardDonate(props: any) {
    useEffect(() => {
        console.log("CardDonate");
        console.log(props);
    }, []);

    return (
        <Card className="card-having-ideas">
            <CardContent>
                <Typography variant="h5">Love what you see?</Typography>

                <Box
                    sx={{
                        display: "flex",
                        justifyContent: "center",
                        mt: 4,
                        mb: 4,
                    }}
                >
                    <Image
                        src="/assets/icons/TribeHood-donate-icon.svg"
                        alt="donate-icon"
                        width={88}
                        height={88}
                    />
                </Box>

                <Typography variant="body2">
                    Any little helps to cover the operational costs better and
                    to improve further.
                </Typography>

                <Box
                    sx={{
                        display: "flex",
                        justifyContent: "center",
                        mt: 4,
                        mb: 2,
                    }}
                >
                    <Link
                        href="https://www.buymeacoffee.com/tribehood"
                        target="_blank"
                    >
                        <Button variant="contained" color="primary">
                            Donate Now
                        </Button>
                    </Link>
                </Box>
            </CardContent>
        </Card>
    );
}
