import { Box, Button, Card, CardContent, Typography } from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import { useEffect } from "react";

export default function CardAdvertise(props: any) {
    useEffect(() => {
        console.log("CardAdvertise");
        console.log(props);
    }, []);

    return (
        <Card className="card-advertise">
            <CardContent>
                <Typography variant="h5">Interested to advertise?</Typography>

                <Box
                    sx={{
                        display: "flex",
                        justifyContent: "center",
                        mt: 4,
                        mb: 4,
                    }}
                >
                    <Image
                        src="/assets/icons/TribeHood-advertise-icon.svg"
                        alt="advertise-icon"
                        width={88}
                        height={88}
                    />
                </Box>

                <Box
                    sx={{
                        display: "flex",
                        justifyContent: "center",
                        mt: 4,
                        mb: 2,
                    }}
                >
                    <Link href="/contact">
                        <Button variant="contained" color="primary">
                            Discuss your Needs
                        </Button>
                    </Link>
                </Box>
            </CardContent>
        </Card>
    );
}
