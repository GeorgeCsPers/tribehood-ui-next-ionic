import {
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardSubtitle,
    IonCardContent,
    IonContent,
} from "@ionic/react";
import {
    Box,
    Button,
    Card,
    CardContent,
    Divider,
    Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import FullscreenModal from "../modals/modal-fullscreen";

import EventsForm from "../forms/events-form";
import ActivitiesForm from "../forms/activities-form";
import PlacesToVisitForm from "../forms/places-to-visit-form";
import ServicesForm from "../forms/service-form";
import ProjectsForm from "../forms/projects-form";
import PlacesForm from "../forms/places-form";
import PublicNoticesForm from "../forms/public-notices-form";

import CategoriesForm from "../forms/categories-form";
import CategoryTypesForm from "../forms/category-types-form";

const postType = [
    {
        id: "events",
        name: "Event",
        icon: "icon-name",
    },
    {
        id: "activities",
        name: "Activity",
        icon: "icon-name",
    },
    {
        id: "services",
        name: "Service",
        icon: "icon-name",
    },
    {
        id: "publicNotices",
        name: "Public Notice",
        icon: "icon-name",
    },
    {
        id: "places",
        name: "Place",
        icon: "icon-name",
    },
    {
        id: "placesToVisit",
        name: "Place to Visit",
        icon: "icon-name",
    },
    {
        id: "projects",
        name: "Project",
        icon: "icon-name",
    },
];
const postCategories = [
    {
        id: "categories",
        name: "Category",
        icon: "icon-name",
    },
    {
        id: "categoryTypes",
        name: "Category Type",
        icon: "icon-name",
    },
];

export default function ChoosePostComponent(props: any) {
    const [postForm, setPostForm] = useState(null);

    console.log("ChoosePostComponent");
    console.log(props);
    console.log(props.userRole[0]);

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    useEffect(() => {
        console.log(postForm);
    }, [postForm, showModal]);
    return (
        <Box>
            <FullscreenModal
                title={modalTitle}
                showModal={showModal}
                setShowModal={setShowModal}
            >
                {postForm === "events" && (
                    <EventsForm userDetails={props.userDetails} />
                )}
                {postForm === "activities" && (
                    <ActivitiesForm userDetails={props.userDetails} />
                )}
                {postForm === "services" && (
                    <ServicesForm userDetails={props.userDetails} />
                )}
                {postForm === "publicNotices" && (
                    <PublicNoticesForm userDetails={props.userDetails} />
                )}
                {postForm === "places" && (
                    <PlacesForm userDetails={props.userDetails} />
                )}
                {postForm === "placesToVisit" && (
                    <PlacesToVisitForm userDetails={props.userDetails} />
                )}
                {postForm === "projects" && (
                    <ProjectsForm userDetails={props.userDetails} />
                )}

                {props.userRole[0] === "admin" && postForm === "categories" && (
                    <CategoriesForm userDetails={props.userDetails} />
                )}
                {props.userRole[0] === "admin" &&
                    postForm === "categoryTypes" && (
                        <CategoryTypesForm userDetails={props.userDetails} />
                    )}
            </FullscreenModal>

            <IonGrid className="container">
                <IonRow>
                    <IonCol size-xs="12">
                        <Typography variant="h6">Post Type</Typography>
                    </IonCol>
                    {postType.map((item, i) => (
                        <IonCol
                            key={i}
                            size-xs="12"
                            size-sm="6"
                            size-md="6"
                            size-lg="3"
                        >
                            <Button
                                fullWidth
                                variant="contained"
                                size="large"
                                sx={{ mb: 2 }}
                                onClick={() => {
                                    setPostForm(item.id);
                                    setShowModal(true);
                                    setModalTitle(`Create ${item.name} Post`);
                                }}
                            >
                                {item.name}
                            </Button>
                            {/* <Card>
                            <CardContent sx={{ p: 4 }}>
                                <Typography variant="h5" textAlign="center">
                                </Typography>
                            </CardContent>
                        </Card> */}
                        </IonCol>
                    ))}
                </IonRow>

                <IonRow>
                    <IonCol size-xs="12">
                        <Divider sx={{ mb: 2 }} />
                    </IonCol>
                </IonRow>

                {props.userRole[0] === "admin" && (
                    <IonRow>
                        <IonCol size-xs="12">
                            <Typography variant="h6">Categories</Typography>
                        </IonCol>

                        {postCategories.map((item, i) => (
                            <IonCol
                                key={i}
                                size-xs="12"
                                size-sm="6"
                                size-md="6"
                                size-lg="3"
                            >
                                <Button
                                    fullWidth
                                    variant="contained"
                                    size="large"
                                    sx={{ mb: 2 }}
                                    onClick={() => {
                                        setPostForm(item.id);
                                        setShowModal(true);
                                        setModalTitle(`Create ${item.name}`);
                                    }}
                                >
                                    {item.name}
                                </Button>
                            </IonCol>
                        ))}
                    </IonRow>
                )}
            </IonGrid>
        </Box>
    );
}
