import * as React from "react";
import { styled } from "@mui/material/styles";
import IconButton, { IconButtonProps } from "@mui/material/IconButton";
import { red } from "@mui/material/colors";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import {
    Tooltip,
    Menu,
    MenuItem,
    Box,
    Grid,
    Button,
    Typography,
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    CardActions,
    Collapse,
    Avatar,
} from "@mui/material";
import Link from "next/link";

// Plugins
// https://day.js.org/docs/en/installation/typescript
// https://codesandbox.io/s/dayjs-example-cgg4f?file=/src/index.js:1329-1336
import dayjs from "dayjs";
import advancedFormat from "dayjs/plugin/advancedFormat";
import localizedFormat from "dayjs/plugin/localizedFormat";
import relativeTime from "dayjs/plugin/relativeTime";
import calendar from "dayjs/plugin/calendar";
import { useEffect, useState } from "react";
import Loading from "../common/loading";
import { useRouter } from "next/router";
import CardMediaComponent from "../common/card-media";

// Load plugins
dayjs.extend(advancedFormat);
dayjs.extend(localizedFormat);
dayjs.extend(relativeTime);
dayjs.extend(calendar);

// Assets URL
const serverUrl = process.env.NEXT_PUBLIC_ASSETS_URL;

export default function CardPlacesComponent(props) {
    console.log("CardPlacesComponent");
    console.log(props);

    const item = props.data;
    const router = useRouter();
    const { query = {} } = router || {};
    const { area = query.area || "all" } = query || {};
    // const area = query.area || "all";

    const [anchorElUser, setAnchorElUser] = useState();
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };
    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isAllowedToManage, setIsAllowedToManage] = useState<boolean>(false);
    useEffect(() => {
        if (area) {
            setIsLoading(false);
            if (
                // (props.isAuth && props.itemUserId === props.currentUserId) ||
                (props.isAuth && item.createdBy?.id === props.currentUserId) ||
                props.role === "sys_admin"
            ) {
                setIsAllowedToManage(true);
            }
        }
    }, [area, props]);

    // - - - - - - - - -
    // Image URL adjustment
    // - - - - - - - - -
    let imageUrl = "/assets/images/TribeHood-better-connected-community.webp";
    if (item?.featuredImageUrl && item?.featuredImageUrl.indexOf("http") > -1) {
        imageUrl = item?.featuredImageUrl;
    } else if (item?.featuredImageUrl) {
        imageUrl = serverUrl + item?.featuredImageUrl;
    }

    return (
        <Box width="100%">
            {!isLoading ? (
                <Card className="post-card">
                    <CardHeader
                        avatar={
                            <Avatar
                                sx={{ bgcolor: red[500] }}
                                aria-label="place"
                            >
                                P
                            </Avatar>
                        }
                        action={
                            <Box sx={{ flexGrow: 0 }}>
                                <Tooltip title="More">
                                    <IconButton
                                        size="large"
                                        onClick={handleOpenUserMenu}
                                        sx={{ p: 0 }}
                                    >
                                        <MoreVertIcon />
                                    </IconButton>
                                </Tooltip>
                                <Menu
                                    sx={{ mt: "40px" }}
                                    id="menu-appbar"
                                    anchorEl={anchorElUser}
                                    anchorOrigin={{
                                        vertical: "top",
                                        horizontal: "right",
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: "top",
                                        horizontal: "right",
                                    }}
                                    open={Boolean(anchorElUser)}
                                    onClose={handleCloseUserMenu}
                                >
                                    {isAllowedToManage && (
                                        <Box>
                                            <MenuItem
                                                onClick={(e) => {
                                                    handleCloseUserMenu();
                                                    props.setShowModal(true);
                                                    props.setRecordForEdit(
                                                        props.data
                                                    );
                                                    props.setModalTitle(
                                                        "Edit Place"
                                                    );
                                                }}
                                            >
                                                <Typography textAlign="center">
                                                    Edit
                                                </Typography>
                                            </MenuItem>
                                            {/* <MenuItem
                                                onClick={(e) => {
                                                    handleCloseUserMenu();
                                                    props.setShowModal(true);
                                                }}
                                            >
                                                <Typography textAlign="center">
                                                    Follow
                                                </Typography>
                                            </MenuItem> */}
                                        </Box>
                                    )}
                                    <Link href={`/${area}/places/${item.slug}`}>
                                        <MenuItem
                                            onClick={(e) => {
                                                handleCloseUserMenu();
                                            }}
                                        >
                                            <Typography textAlign="center">
                                                View Place
                                            </Typography>
                                        </MenuItem>
                                    </Link>
                                </Menu>
                            </Box>
                        }
                        title={item?.name}
                        // subheader={dayjs(item.createdAt).format("LLLL")}
                        subheader={
                            dayjs(item.createdAt).fromNow() +
                            " • by " +
                            item?.createdBy?.firstName +
                            " " +
                            item?.createdBy?.lastName
                        }
                    ></CardHeader>

                    <Link href={`/${area}/places/${item.slug}`}>
                        <CardMediaComponent item={item} imageUrl={imageUrl} />
                    </Link>

                    {item.address[0]?.city && (
                        <Box
                            sx={{
                                display: "flex",
                                justifyContent: "end",
                                alignItems: "center",
                            }}
                            className="info-box"
                        >
                            <Box className="info-box-container" sx={{ ml: 2 }}>
                                {/* {item.eventEndDate} */}
                                <Box className="info-box-header">Location</Box>
                                <Box className="info-box-content city-name">
                                    {item.address[0].city}
                                </Box>
                                <Box className="info-box-footer">
                                    {item.address[0]?.postcode}
                                </Box>
                            </Box>
                        </Box>
                    )}

                    <CardContent>
                        <Grid container>
                            <Grid item xs={12} sm={9}>
                                <Typography
                                    component="p"
                                    sx={{
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",
                                        display: "-webkit-box",
                                        WebkitLineClamp: "2",
                                        WebkitBoxOrient: "vertical",
                                    }}
                                >
                                    {item.shortDescription}{" "}
                                </Typography>
                            </Grid>

                            <Grid
                                item
                                xs={12}
                                sm={3}
                                sx={{
                                    display: "flex",
                                    justifyContent: "end",
                                }}
                            >
                                <Link href={`/${area}/places/${item.slug}`}>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                    >
                                        View More
                                    </Button>
                                </Link>
                            </Grid>
                        </Grid>
                    </CardContent>
                    {/* <CardActions disableSpacing>
                        <IconButton aria-label="add to favorites">
                            <FavoriteIcon />
                        </IconButton>
                        <IconButton aria-label="share">
                            <ShareIcon />
                        </IconButton>
                    </CardActions> */}
                </Card>
            ) : (
                <Loading />
            )}
        </Box>
    );
}
