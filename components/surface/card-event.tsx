import * as React from "react";
import { styled } from "@mui/material/styles";
import IconButton, { IconButtonProps } from "@mui/material/IconButton";
import { red } from "@mui/material/colors";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import {
    Tooltip,
    Menu,
    MenuItem,
    Box,
    Grid,
    Button,
    Typography,
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    CardActions,
    Collapse,
    Avatar,
} from "@mui/material";
import Link from "next/link";
// import Image from "next/image";

// Plugins
// https://day.js.org/docs/en/installation/typescript
// https://codesandbox.io/s/dayjs-example-cgg4f?file=/src/index.js:1329-1336
import dayjs from "dayjs";
import advancedFormat from "dayjs/plugin/advancedFormat";
import localizedFormat from "dayjs/plugin/localizedFormat";
import relativeTime from "dayjs/plugin/relativeTime";
import calendar from "dayjs/plugin/calendar";
import { useEffect, useState } from "react";
import Loading from "../common/loading";
import { useRouter } from "next/router";
import isAllowedToManageChecker from "../hooks/is-allowed-to-manage";
import categoryTypeFormatter from "../hooks/category-type-formatter";
import CardMediaComponent from "../common/card-media";

// Load plugins
dayjs.extend(advancedFormat);
dayjs.extend(localizedFormat);
dayjs.extend(relativeTime);
dayjs.extend(calendar);

interface ExpandMoreProps extends IconButtonProps {
    expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
    }),
}));

// Assets URL
const serverUrl = process.env.NEXT_PUBLIC_ASSETS_URL;

export default function CardEventComponent(props) {
    // console.log("CardEventComponent");
    // console.log(props);

    const item = props.data;
    const router = useRouter();
    const { query = {} } = router || {};
    const { area = query.area || "all" } = query || {};
    // const area = query.area || "all";

    // console.log("CardEventComponent");
    // console.log(props);
    // console.log(item);
    // console.log(area);
    const [expanded, setExpanded] = useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    const [anchorElNav, setAnchorElNav] = useState();
    const [anchorElUser, setAnchorElUser] = useState();
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };
    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isAllowedToManage, setIsAllowedToManage] = useState<boolean>(false);
    useEffect(() => {
        if (area) {
            setIsLoading(false);
            // Check if user allowed to manage
            if (props.isAuth) {
                const isAllowed = isAllowedToManageChecker(
                    props.userDetails,
                    item
                );
                setIsAllowedToManage(isAllowed);
            }
            // console.log("isAllowedToManage");
            // console.log(isAllowedToManage);
            // if (
            //     // (props.isAuth && props.itemUserId === props.currentUserId) ||
            //     (props.isAuth && item.createdBy?.id === props.currentUserId) ||
            //     props.role === "sys_admin"
            // ) {
            //     setIsAllowedToManage(true);
            // }
        }
    }, [area, props]);

    // - - - - - - - - -
    // Check categoryTypes to adjust URL
    // - - - - - - - - -
    let adjustedUrl = "";
    adjustedUrl = categoryTypeFormatter(item.categoryTypes[0]?.name, "slug");

    // - - - - - - - - -
    // Image URL adjustment
    // - - - - - - - - -
    let imageUrl = "/assets/images/TribeHood-better-connected-community.webp";
    if (item?.featuredImageUrl && item?.featuredImageUrl.indexOf("http") > -1) {
        imageUrl = item?.featuredImageUrl;
    } else if (item?.featuredImageUrl) {
        imageUrl = serverUrl + item?.featuredImageUrl;
    }

    const imageLoadDone = (img: any) => {
        console.log(img);
    };

    // - - - - - - - - -
    // Format status of events dates
    // - - - - - - - - -
    const formatStatusOfEvent = (item: any) => {
        const currentDate = dayjs(new Date());

        if (
            currentDate.format("DD/MM/YYYY") ===
            dayjs(item.startDate).format("DD/MM/YYYY")
        ) {
            return "Happening Today 😎";
        } else if (
            currentDate >= dayjs(item.startDate) &&
            currentDate.format("DD/MM/YYYY") <=
                dayjs(item.endDate).format("DD/MM/YYYY") &&
            item.endDate !== null
        ) {
            // if (dayjs(new Date()) <= dayjs(item.endDate)) {
            return "Ongoing 😀";
            // }
        } else if (currentDate < dayjs(item.startDate)) {
            return "Upcoming 🤩";
        } else if (
            currentDate > dayjs(item.startDate) ||
            currentDate.format("DD/MM/YYYY") >
                dayjs(item.endDate).format("DD/MM/YYYY")
        ) {
            // if (item.endDate === null) {
            return "Too late 😔";
            // }
        } else {
            return "";
        }
    };

    return (
        <Box width="100%">
            {!isLoading ? (
                <Card className="post-card">
                    <CardHeader
                        avatar={
                            <Avatar
                                sx={{ bgcolor: red[500] }}
                                aria-label="recipe"
                            >
                                E
                            </Avatar>
                        }
                        action={
                            <Box sx={{ flexGrow: 0 }}>
                                <Tooltip title="More">
                                    <IconButton
                                        size="large"
                                        onClick={handleOpenUserMenu}
                                        sx={{ p: 0 }}
                                    >
                                        <MoreVertIcon />
                                    </IconButton>
                                </Tooltip>
                                <Menu
                                    sx={{ mt: "40px" }}
                                    id="menu-appbar"
                                    anchorEl={anchorElUser}
                                    anchorOrigin={{
                                        vertical: "top",
                                        horizontal: "right",
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: "top",
                                        horizontal: "right",
                                    }}
                                    open={Boolean(anchorElUser)}
                                    onClose={handleCloseUserMenu}
                                >
                                    {isAllowedToManage && (
                                        <Box>
                                            <MenuItem
                                                onClick={(e) => {
                                                    handleCloseUserMenu();
                                                    props.setShowModal(true);
                                                    props.setRecordForEdit(
                                                        props.data
                                                    );
                                                    props.setModalTitle(
                                                        "Edit Event"
                                                    );
                                                }}
                                            >
                                                <Typography textAlign="center">
                                                    Edit
                                                </Typography>
                                            </MenuItem>
                                            {/* <MenuItem
                                                onClick={(e) => {
                                                    handleCloseUserMenu();
                                                    props.setShowModal(true);
                                                }}
                                            >
                                                <Typography textAlign="center">
                                                    Follow
                                                </Typography>
                                            </MenuItem> */}
                                        </Box>
                                    )}
                                    <Link
                                        href={`/${area}/${adjustedUrl}/${item.slug}`}
                                    >
                                        <MenuItem
                                            onClick={(e) => {
                                                handleCloseUserMenu();
                                            }}
                                        >
                                            <Typography textAlign="center">
                                                View Event
                                            </Typography>
                                        </MenuItem>
                                    </Link>
                                </Menu>
                            </Box>
                        }
                        title={item?.name}
                        subheader={
                            <Box
                            // sx={{
                            //     backgroundColor: "red",
                            //     display: "flex",
                            //     width: "auto",
                            // }}
                            >
                                {formatStatusOfEvent(item)}
                            </Box>
                        }
                        // subheader={dayjs(item?.startDate).format("DD MMM YYYY")}
                        // subheader={
                        //     dayjs(item.createdAt).fromNow() +
                        //     " • by " +
                        //     item?.createdBy?.firstName +
                        //     " " +
                        //     item?.createdBy?.lastName
                        // }
                    ></CardHeader>

                    <Link href={`/${area}/${adjustedUrl}/${item.slug}`}>
                        <CardMediaComponent item={item} imageUrl={imageUrl} />
                        {/* <CardMedia
                            component="img"
                            height="480"
                            image={imageUrl}
                            alt={item?.name}
                            sx={{
                                height: { xs: "404px", sm: "480px" },
                            }}
                        /> */}
                        {/* <Image
                            // height={404}
                            // width={404}
                            fill={true}
                            src={imageUrl}
                            alt={item?.name}
                            quality={80}
                            sizes="100vw"
                            // style={{
                            //     width: "100%",
                            //     height: "auto",
                            // }}
                            onLoadingComplete={(img) => imageLoadDone(img)}
                        /> */}
                    </Link>

                    {(item.startDate ||
                        item.endDate ||
                        item.address[0]?.city) && (
                        <Box
                            sx={{
                                display: "flex",
                                justifyContent: "end",
                                alignItems: "center",
                            }}
                            className="info-box"
                        >
                            {item.startDate && (
                                <Box className="info-box-container">
                                    {/* {item.eventStartDate} */}
                                    <Box className="info-box-header">
                                        {dayjs(item.startDate).format("MMM")}
                                    </Box>
                                    <Box className="info-box-content">
                                        {dayjs(item.startDate).format("DD")}
                                    </Box>
                                    <Box className="info-box-footer">
                                        {dayjs(item.startDate).format("ddd")}
                                    </Box>
                                </Box>
                            )}
                            {item.endDate && (
                                <Box
                                    className="info-box-container"
                                    sx={{ ml: 2 }}
                                >
                                    <Box className="date-to">To</Box>

                                    <Box className="info-box-header">
                                        {dayjs(item.endDate).format("MMM")}
                                    </Box>
                                    <Box className="info-box-content">
                                        {dayjs(item.endDate).format("DD")}
                                    </Box>
                                    <Box className="info-box-footer">
                                        {dayjs(item.endDate).format("ddd")}
                                    </Box>
                                </Box>
                            )}
                            {item?.address[0]?.city && (
                                <Box
                                    className="info-box-container"
                                    sx={{ ml: 2 }}
                                >
                                    <Box className="info-box-header">
                                        Location
                                    </Box>
                                    <Box className="info-box-content city-name">
                                        {item.address[0].city}
                                    </Box>
                                    <Box className="info-box-footer">
                                        {item.address[0]?.postcode}
                                    </Box>
                                </Box>
                            )}
                        </Box>
                    )}

                    <CardContent>
                        <Grid container>
                            <Grid item xs={12} sm={9}>
                                <Typography
                                    component="p"
                                    sx={{
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",
                                        display: "-webkit-box",
                                        WebkitLineClamp: "2",
                                        WebkitBoxOrient: "vertical",
                                    }}
                                >
                                    {item.shortDescription}{" "}
                                    {/* <Link href={`/${area}/events/${item.slug}`}>
                                        <strong>Read More...</strong>
                                    </Link> */}
                                </Typography>
                                {/* <Typography
                                    component="span"
                                    className="linking"
                                    sx={{ fontWeight: "fontWeightMedium" }}
                                    onClick={handleExpandClick}
                                >
                                    Read More
                                </Typography> */}

                                {/* <ExpandMore
                    expand={expanded}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </ExpandMore> */}
                                {/* <Collapse
                                    in={expanded}
                                    timeout="auto"
                                    unmountOnExit
                                >
                                    <Typography paragraph>Method:</Typography>
                                    <Typography paragraph>
                                        Heat 1/2 cup of the broth in a pot until
                                        simmering, add saffron and set aside for
                                        10 minutes.
                                    </Typography>
                                    <Typography paragraph>
                                        Heat oil in a (14- to 16-inch) paella
                                        pan or a large, deep skillet over
                                        medium-high heat. Add chicken, shrimp
                                        and chorizo, and cook, stirring
                                        occasionally until lightly browned, 6 to
                                        8 minutes. Transfer shrimp to a large
                                        plate and set aside, leaving chicken and
                                        chorizo in the pan. Add pimentón, bay
                                        leaves, garlic, tomatoes, onion, salt
                                        and pepper, and cook, stirring often
                                        until thickened and fragrant, about 10
                                        minutes. Add saffron broth and remaining
                                        4 1/2 cups chicken broth; bring to a
                                        boil.
                                    </Typography>
                                    <Typography paragraph>
                                        Add rice and stir very gently to
                                        distribute. Top with artichokes and
                                        peppers, and cook without stirring,
                                        until most of the liquid is absorbed, 15
                                        to 18 minutes. Reduce heat to
                                        medium-low, add reserved shrimp and
                                        mussels, tucking them down into the
                                        rice, and cook again without stirring,
                                        until mussels have opened and rice is
                                        just tender, 5 to 7 minutes more.
                                        (Discard any mussels that don&apos;t
                                        open.)
                                    </Typography>
                                    <Typography>
                                        Set aside off of the heat to let rest
                                        for 10 minutes, and then serve.
                                    </Typography>
                                </Collapse> */}
                            </Grid>
                            <Grid
                                item
                                xs={12}
                                sm={3}
                                sx={{
                                    display: "flex",
                                    justifyContent: "end",
                                }}
                            >
                                <Link
                                    href={`/${area}/${adjustedUrl}/${item.slug}`}
                                >
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                    >
                                        View More
                                    </Button>
                                </Link>
                            </Grid>
                        </Grid>
                    </CardContent>
                    {/* <CardActions disableSpacing>
                        <IconButton aria-label="add to favorites">
                            <FavoriteIcon />
                        </IconButton>
                        <IconButton aria-label="share">
                            <ShareIcon />
                        </IconButton>
                    </CardActions> */}
                </Card>
            ) : (
                <Loading />
            )}
        </Box>
    );
}
