// import Head from "next/head";
import { NextPage } from "next";
import { IonFooter, IonToolbar, IonTitle } from "@ionic/react";

export default function FooterMain(props: any) {
    return (
        <IonFooter>
            <IonToolbar>
                <IonTitle>Footer</IonTitle>
            </IonToolbar>
        </IonFooter>
    );
}
