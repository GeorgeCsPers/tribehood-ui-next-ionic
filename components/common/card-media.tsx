import { CardMedia } from "@mui/material";
import Image from "next/image";

export default function CardMediaComponent(props) {
    console.log(props.item);
    console.log(props.imageUrl);
    return (
        <CardMedia
            component="div"
            sx={{
                height: {
                    xs: "404px",
                    sm: "448px",
                },
                display: "flex",
                justifyContent: "center",
                position: "relative",
                // backgroundImage: `url(${props.imageUrl})`,
                // filter: "blur(8px)",
            }}
            children={
                <div
                    dangerouslySetInnerHTML={{
                        __html: `
                            <Image
                                fill=${true}
                                src=${encodeURI(props.imageUrl)}
                                alt=${props.item?.name}
                                quality=${80}
                                style="min-height: 480px;"
                            />
                        `,
                        // objectFit=${"cover"}
                        //   style=${{ objectFit: "cover" }}
                    }}
                ></div>
            }
        />
    );
}
