import { useEffect, useState } from "react";
import {
    Stack,
    Snackbar,
    SnackbarContent,
    Alert,
    AlertTitle,
    Button,
    Typography,
    // Slide,
    IconButton,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

// function TransitionLeft(props) {
//     return <Slide {...props} direction="left" />;
// }
// function TransitionRight(props) {
//     return <Slide {...props} direction="right" />;
// }
// function TransitionUp(props) {
//     return <Slide {...props} direction="up" />;
// }
// function TransitionDown(props) {
//     return <Slide {...props} direction="down" />;
// }

export default function SnackbarComp(props) {
    console.log("SnackbarComp");
    console.log(props);

    const [open, setOpen] = useState(true);

    useEffect(() => {
        (async () => {
            console.log(props);
            checkMessages();
        })();
    }, [props]);

    const actionSnackbar = (
        <Button size="small" onClick={() => setOpen(false)}>
            Close
        </Button>
    );

    const actionClose = (
        <IconButton
            aria-label="close"
            color="inherit"
            size="small"
            onClick={() => {
                setOpen(false);
            }}
        >
            <CloseIcon fontSize="inherit" />
        </IconButton>
    );

    const handleClose = async (e) => {
        console.log("handleClose e");
        console.log(e);
        setOpen(false);
    };

    const [message, setMessage] = useState(props.message);
    const checkMessages = async () => {
        if (message.indexOf("_slug_key") > -1) {
            setMessage("Name already exist, please use another one!");
        }
    };

    return (
        <Stack spacing={2}>
            <Snackbar
                open={open}
                autoHideDuration={6000}
                // onClose={(e) => handleClose(e)}
                // TransitionComponent={props?.transition}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center",
                }}
                sx={{ maxWidth: 600, minWidth: 280 }}
            >
                <Alert
                    severity={props.type} // error, warning, info, success
                    sx={{ width: "100%" }}
                    action={actionClose}
                    variant="filled"
                >
                    <Typography variant="h6">{message}</Typography>
                </Alert>
            </Snackbar>
        </Stack>
    );
}
