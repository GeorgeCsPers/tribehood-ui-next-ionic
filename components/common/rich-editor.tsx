import React, { useState } from "react";
import {
    convertToRaw,
    convertFromRaw,
    convertFromHTML,
    convertToHTML,
    EditorState,
    ContentState,
} from "draft-js";
import { EditorProps } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "draft-js/dist/Draft.css";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import dynamic from "next/dynamic";

const Editor = dynamic<EditorProps>(
    () => import("react-draft-wysiwyg").then((mod) => mod.Editor),
    { ssr: false }
);

export default function RichContentEditor(props) {
    const blocksFromHTML = convertFromHTML(props?.content || "");
    const contentState = ContentState.createFromBlockArray(
        blocksFromHTML.contentBlocks,
        blocksFromHTML.entityMap
    );
    console.log(blocksFromHTML);
    console.log(contentState);

    const state = {
        editorState:
            EditorState.createWithContent(contentState) ||
            EditorState.createEmpty(),
    };

    const [newState, setNewState] = useState(EditorState.createEmpty());

    return (
        <div>
            <Editor
                defaultEditorState={state.editorState}
                onEditorStateChange={(e) => {
                    setNewState(e);
                    props.setNewContent(
                        draftToHtml(convertToRaw(newState.getCurrentContent()))
                    );
                }}
                wrapperClassName="wrapper-class"
                editorClassName="editor-class"
                toolbarClassName="toolbar-class"
            />
            {/* <div className="code-view">
                <p>HTML View </p>
                <textarea
                    className="text-area"
                    disabled
                    value={draftToHtml(
                        convertToRaw(newState.getCurrentContent())
                    )}
                />
            </div> */}
        </div>
    );
}
