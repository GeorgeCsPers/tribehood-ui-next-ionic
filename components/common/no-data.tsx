import { Box, Card, CardContent, Typography } from "@mui/material";

export default function NoData(props) {
    return (
        <Box className="no-data">
            {props.type === "card" && (
                <Box>
                    <Card>
                        <CardContent>
                            <Typography
                                variant="body1"
                                sx={{
                                    mt: 4,
                                    mb: 3,
                                    textAlign: "center",
                                    fontWeight: 500,
                                }}
                            >
                                Currently there is no related data :(
                            </Typography>
                        </CardContent>
                    </Card>
                </Box>
            )}
            {props.type === "text" && (
                <Box>
                    <Typography variant="body1">
                        Currently there is no related data :(
                    </Typography>
                </Box>
            )}
        </Box>
    );
}
