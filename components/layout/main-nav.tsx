import { IonItem, IonMenuToggle } from "@ionic/react";
import {
    Box,
    Card,
    CardContent,
    FormControl,
    InputLabel,
    List,
    ListItem,
    ListItemButton,
    ListItemText,
    MenuItem,
    Select,
} from "@mui/material";
import Link from "next/link";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import authService from "../../services/auth.service";
import Loading from "../common/loading";
import { getPublicData } from "../../services/data.service";
import storageService from "../../services/storage.service";

const publicNav = [
    { name: "Home", url: "/[area]", isArea: true },
    { name: "Events", url: "/[area]/events", isArea: true },
    // { name: "Projects", url: "/[area]/projects", isArea: true },
    { name: "Activities", url: "/[area]/activities", isArea: true },
    { name: "Public Notices", url: "/[area]/public-notices", isArea: true },
    { name: "Places", url: "/[area]/places", isArea: true },
    { name: "Services", url: "/[area]/services", isArea: true },
    { name: "Places to Visit", url: "/[area]/places-to-visit", isArea: true },
];
const privateNav = [
    { name: "Home", url: "/[area]", isArea: true },
    { name: "Events", url: "/[area]/events", isArea: true },
    // { name: "Projects", url: "/[area]/projects", isArea: true },
    { name: "Activities", url: "/[area]/activities", isArea: true },
    { name: "Public Notices", url: "/[area]/public-notices", isArea: true },
    { name: "Places", url: "/[area]/places", isArea: true },
    { name: "Services", url: "/[area]/services", isArea: true },
    { name: "Places to Visit", url: "/[area]/places-to-visit", isArea: true },
];
const adminNav = [
    { name: "Home", url: "/[area]", isArea: true },
    { name: "Events", url: "/[area]/events", isArea: true },
    // { name: "Projects", url: "/[area]/projects", isArea: true },
    { name: "Activities", url: "/[area]/activities", isArea: true },
    { name: "Public Notices", url: "/[area]/public-notices", isArea: true },
    { name: "Places", url: "/[area]/places", isArea: true },
    { name: "Services", url: "/[area]/services", isArea: true },
    { name: "Places to Visit", url: "/[area]/places-to-visit", isArea: true },
    { name: "Users", url: "/users", isArea: false },
    { name: "Categories", url: "/categories", isArea: false },
    { name: "Category Types", url: "/category-types", isArea: false },
    // { name: "Category Types", url: "/category-types" },
];

// const selectedArea = "wallingford";

export default function MainNav(props: any) {
    const router = useRouter();
    const { query = {} } = router || {};
    // const { area = "all" } = query || {};
    let area = query.area;
    // const selectedArea = query.area;
    const selectedArea = query.area || "all";
    // let area = query.area || "all";

    // const { queryArea } = router.query;
    // const queryArea = query;
    console.log("MainNav props");
    console.log(props);
    console.log("area");
    console.log(area);

    let mainNav = publicNav;
    if (props.isAuth) {
        if (props.userRole === "sys_admin" || props.userRole === "admin") {
            mainNav = adminNav;
        } else {
            mainNav = privateNav;
        }
    }

    const [userDetails, setUserDetails] = useState<any>([]);
    const checkUser = useCallback(async () => {
        const user = await authService.getCurrentUser();
        // return user;
        setUserDetails(await user);
        // getSelectionData();
    }, []);

    const [isSelectionLoading, setIsSelectionLoading] = useState(true);
    const [areas, setAreas] = useState([]);
    const getSelectionData = async () => {
        // const getSelectionData = useCallback(async () => {
        // // Check area
        // area = query.area;

        // Get selection data
        const areasRawData = await getPublicData("location/areas/selection");
        // return areasRawData;
        setAreas(await areasRawData);

        console.log("areasRawData");
        console.log(areasRawData);
        console.log("area");
        console.log(area);
        // console.log("newArea");
        // console.log(newArea);

        // userDetails.forEach((item: any) => {
        //     console.log("item");
        //     console.log(item);
        // });

        areasRawData.forEach((item: any) => {
            if (area === item.slug) {
                // setUserAreaProp(item.id);
                setUserAreaProp(item);
                console.log("areasRawData item");
                console.log(item);
            }
            // else {
            //     if (item.id === userDetails?.userAreaId) {
            //         // setUserAreaProp(item.id);
            //         setUserAreaProp(item);
            //         // Update user -> userArea
            //     } else {
            //         // setUserAreaProp(areasRawData[2].id);
            //         setUserAreaProp(areasRawData[2]);
            //         // Update user -> userArea
            //     }
            // }
        });

        // setValue("userArea", { id: areasRawData[0].id });
        // setValue("userAreaId", areasRawData[0].id);

        setIsSelectionLoading(false);
        // }, []);
    };

    useEffect(() => {
        console.log("useEffect area");
        console.log(area);
        if (area) {
            (async () => {
                await checkUser();
                // if (userDetails) {
                // setTimeout(() => {
                await getSelectionData();
                // }, 100);
                // }
            })();
        }

        // let mounted = true;
        // checkUser().then((items) => {
        //     if (mounted) {
        //         setUserDetails(items);
        //     }
        // });
        // getSelectionData().then((items) => {
        //     if (mounted) {
        //         setAreas(items);
        //         // userDetails.forEach((item: any) => {
        //         //     console.log("item");
        //         //     console.log(item);
        //         // });
        //         items.forEach((item: any) => {
        //             if (item.id === userDetails.userAreaId) {
        //                 setUserAreaProp(item.id);
        //             } else {
        //                 setUserAreaProp(items[0].id);
        //             }
        //         });
        //         setIsSelectionLoading(false);
        //     }
        // });
        // return () => (mounted = false);
    }, [area]);

    const [userAreaProp, setUserAreaProp] = useState([]);
    const handleUserAreaSelection = (data: any) => {
        console.log("handleUserAreaSelection");
        console.log(data);
        console.log(data.target.value);

        setUserAreaProp(data.target.value);

        props.handleAreaChange(true);
        // setValue("userArea", { id: data.target.value });
        // setValue("userArea", data.target.value);

        router.push({
            query: { area: data.target.value.slug },
        });
        // Update user -> userArea
        // if (area !== selectedArea) {
        //     // setTimeout(() => {
        //     router.reload();
        //     // }, 100);
        // }
    };

    console.log("userDetails");
    console.log(userDetails);
    console.log("userAreaProp");
    console.log(userAreaProp);
    console.log("areas");
    console.log(areas);

    return (
        <>
            {props.isSmScreen ? (
                !isSelectionLoading ? (
                    <Box>
                        <FormControl fullWidth sx={{ mb: 1, pl: 1, pr: 1 }}>
                            <Select
                                variant="standard"
                                sx={{ p: 1 }}
                                labelId="area-selection-label"
                                onChange={(e) => handleUserAreaSelection(e)}
                                // value={areas[0].id}
                                value={userAreaProp}
                                // value={selectedArea}
                                // defaultValue={areas[0].id}
                                // defaultValue={categoryTypesProp}
                                // renderValue={() => categoryTypesProp[0].id}
                                // inputProps={register(
                                //     "userAreaId",
                                //     {
                                //         required:
                                //             "Please choose",
                                //     }
                                // )}
                                // inputProps={register("userArea", {
                                //     required: "Please choose",
                                // })}
                            >
                                {areas.map((item, i) => (
                                    <MenuItem key={i} value={item}>
                                        {item.name} Areas
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>

                        {mainNav.map((item, i) => (
                            <Link
                                href={{
                                    pathname: item.url,
                                    query: {
                                        area: selectedArea,
                                    },
                                }}
                                key={i}
                            >
                                <IonMenuToggle>
                                    <IonItem>{item.name}</IonItem>
                                </IonMenuToggle>
                            </Link>
                        ))}
                    </Box>
                ) : (
                    <Loading />
                )
            ) : (
                <Card className="main-menu-card" elevation={15}>
                    <CardContent sx={{ pt: 1 }}>
                        {!isSelectionLoading ? (
                            <FormControl fullWidth sx={{ mb: 1 }}>
                                <Select
                                    variant="standard"
                                    sx={{ p: 1 }}
                                    labelId="area-selection-label"
                                    onChange={(e) => handleUserAreaSelection(e)}
                                    // value={areas[0].id}
                                    value={userAreaProp}
                                    // value={selectedArea}
                                    // defaultValue={userAreaProp}
                                    // defaultValue={areas[0].id}
                                    // defaultValue={categoryTypesProp}
                                    // renderValue={() => categoryTypesProp[0].id}
                                    // inputProps={register(
                                    //     "userAreaId",
                                    //     {
                                    //         required:
                                    //             "Please choose",
                                    //     }
                                    // )}
                                    // inputProps={register("userArea", {
                                    //     required: "Please choose",
                                    // })}
                                >
                                    {areas.map((item, i) => (
                                        <MenuItem key={i} value={item}>
                                            {item.name} Areas
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        ) : (
                            <Loading />
                        )}
                        <List>
                            {mainNav.map((item, i) =>
                                item.isArea ? (
                                    <Link
                                        key={i}
                                        // href={item.url}
                                        href={{
                                            pathname: item.url,
                                            query: {
                                                area: selectedArea,
                                            },
                                        }}
                                        className={
                                            router.pathname == item.url
                                                ? "active"
                                                : ""
                                        }
                                    >
                                        <ListItem disablePadding>
                                            <ListItemButton>
                                                {/* <ListItemIcon>
                                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                            </ListItemIcon> */}
                                                <ListItemText
                                                    primary={item.name}

                                                    // onClick={() => setActive(item)}
                                                    // className={`main-menu-item ${
                                                    //     active === item
                                                    //         ? "active"
                                                    //         : ""
                                                    // }`}
                                                />
                                            </ListItemButton>
                                        </ListItem>
                                    </Link>
                                ) : (
                                    <Link
                                        key={i}
                                        href={item.url}
                                        // href={{
                                        //     pathname: item.url,
                                        // }}
                                        className={
                                            router.pathname == item.url
                                                ? "active"
                                                : ""
                                        }
                                    >
                                        <ListItem disablePadding>
                                            <ListItemButton>
                                                <ListItemText
                                                    primary={item.name}
                                                />
                                            </ListItemButton>
                                        </ListItem>
                                    </Link>
                                )
                            )}
                        </List>
                    </CardContent>
                </Card>
            )}
        </>
    );
}
