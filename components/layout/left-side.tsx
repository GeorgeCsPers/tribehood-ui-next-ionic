import MainNav from "./main-nav";
import CardAdvertise from "../surface/card-advertise";
import CardIdeas from "../surface/card-ideas";
import { useEffect, useState } from "react";
import authService from "../../services/auth.service";
import { Box } from "@mui/material";

export default function LeftSide(props: any) {
    // console.log("LeftSide");
    // console.log(props);

    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        checkUser();
    };

    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [userDetails, setUserDetails] = useState<any>();
    const checkUser = async () => {
        const user = await authService.getCurrentUser();
        setUserDetails(user);
        setIsLoading(false);
        // getData(user);
    };

    useEffect(() => {
        checkToken();
        console.log("useEffect LeftSide");
        console.log(props.fixedClass);
        console.log("userDetails");
        console.log(userDetails);
    }, [props]);

    return (
        // <Box component="div" className="left-side">
        <Box component="div" sx={{ width: "100%" }}>
            {!isLoading && (
                <Box
                    component="div"
                    sx={{ mb: 3 }}
                    className={props.fixedClass ? "sticky" : ""}
                >
                    <MainNav
                        isAuth={isAuth}
                        userRole={userDetails?.role[0]}
                        handleAreaChange={props.handleAreaChange}
                    />
                </Box>
            )}
            {/* <Box component="div" sx={{ mb: 3 }}>
                <CardAdvertise />
            </Box> */}
            <Box component="div" sx={{ mb: 3 }}>
                <CardIdeas />
            </Box>
        </Box>
    );
}
