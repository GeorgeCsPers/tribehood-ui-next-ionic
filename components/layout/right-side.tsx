import MainNav from "./main-nav";
import CardAdvertise from "../surface/card-advertise";
import CardDonate from "../surface/card-donate";
import CardLatestPosts from "../surface/card-latest-posts";
import { Box } from "@mui/material";
import { useEffect } from "react";
import CardUpcomingEvents from "../surface/card-upcoming-events";
import CardFeatured from "../surface/card-featured";
import CardFooter from "../surface/card-footer";

export default function RightSide(props: any) {
    useEffect(() => {
        console.log("useEffect RightSide");
        console.log(props);
    }, [props]);

    return (
        // <Box component="div" className="right-side">
        <Box component="div">
            {/* <Box
                component="div"
                sx={{ mb: 3 }}
                className={props.fixedClass ? "sticky" : ""}
            >
                <CardFeatured />
                <CardUpcomingEvents />
                <CardLatestPosts />
            </Box> */}

            <Box component="div" sx={{ mb: 3 }}>
                <CardDonate />
            </Box>

            <Box component="div" sx={{ mb: 3 }}>
                <CardAdvertise />
            </Box>

            <Box component="div" sx={{ mb: 3 }}>
                <CardFooter />
            </Box>
        </Box>
    );
}
