import {
    IonButton,
    IonIcon,
    IonPopover,
    IonContent,
    IonItem,
    IonLabel,
    IonButtons,
} from "@ionic/react";
import { addCircleOutline, personCircleOutline } from "ionicons/icons";
import AddIcon from "@mui/icons-material/Add";
import Link from "next/link";
import { useRef, useState } from "react";
import authService from "../../services/auth.service";
import { Button, IconButton } from "@mui/material";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import EventsForm from "../forms/events-form";
import FullscreenModal from "../modals/modal-fullscreen";
import ChoosePostComponent from "../surface/choose-post";

export default function ProfileMenu(props: any) {
    const popover = useRef<HTMLIonPopoverElement>(null);
    const [popoverOpen, setPopoverOpen] = useState(false);

    // - - - - - - -
    // Profile Menu
    // - - - - - - -
    const openPopover = (e: any) => {
        popover.current!.event = e;
        setPopoverOpen(true);
    };

    // - - - - - - -
    // Create New Post Modal
    // - - - - - - -
    const [showModal, setShowModal] = useState(false);

    return (
        <>
            <FullscreenModal
                title="Create New Post"
                showModal={showModal}
                setShowModal={setShowModal}
            >
                {/* {!isLoading && ( */}
                <ChoosePostComponent
                    recordForEdit={null}
                    // dataCategoryTypes={categoryTypes}
                    // dataCategories={categories}
                    userId={props.userDetails?.id}
                    userRole={props.userDetails?.role}
                    userDetails={props.userDetails}
                />
                {/* )} */}
            </FullscreenModal>

            {/* Create Post - SX up */}
            {/* <IonButtons slot="primary"> */}
            {/* <IonButton color="secondary">
                <IonIcon slot="start" icon={addCircleOutline}></IonIcon> Post
            </IonButton> */}
            <Button
                variant="contained"
                size="small"
                startIcon={<AddCircleOutlineIcon />}
                sx={{ mr: 2, display: { xs: "none", sm: "inline-flex" } }}
                onClick={() => {
                    setShowModal(true);
                }}
            >
                Post
            </Button>

            {/* Create Post - XS */}
            <IconButton
                color="primary"
                size="large"
                sx={{ mr: -1, display: { xs: "inline-flex", sm: "none" } }}
                onClick={() => {
                    setShowModal(true);
                }}
            >
                <AddCircleOutlineIcon />
            </IconButton>
            {/* </IonButtons> */}

            {/* Profile Menu */}
            {/* <IonButton color="dark" onClick={openPopover}>
                <IonIcon slot="icon-only" icon={personCircleOutline}></IonIcon>
            </IonButton> */}
            <IconButton color="inherit" size="large" onClick={openPopover}>
                <AccountCircleIcon />
            </IconButton>
            <IonPopover
                ref={popover}
                isOpen={popoverOpen}
                onDidDismiss={() => setPopoverOpen(false)}
            >
                <IonContent class="ion-padding">
                    <Link href="/users/my-profile">
                        <IonItem button onClick={() => setPopoverOpen(false)}>
                            <IonLabel>My Profile</IonLabel>
                        </IonItem>
                    </Link>
                    <Link href="/users/my-account">
                        <IonItem button onClick={() => setPopoverOpen(false)}>
                            <IonLabel>My Account</IonLabel>
                        </IonItem>
                    </Link>
                    <IonItem
                        button
                        onClick={() => {
                            setPopoverOpen(false), authService.logout();
                        }}
                    >
                        <IonLabel>Logout</IonLabel>
                    </IonItem>
                </IonContent>
            </IonPopover>
        </>
    );
}
