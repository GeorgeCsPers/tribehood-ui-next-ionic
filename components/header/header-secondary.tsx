// import Head from "next/head";
import { NextPage } from "next";
import Link from "next/link";
import { IonHeader, IonToolbar, IonTitle, IonGrid } from "@ionic/react";
import { useRouter } from "next/router";
import {
    Button,
    Grid,
    Container,
    Box,
    CardMedia,
    Divider,
    IconButton,
    Typography,
} from "@mui/material";
import { useState } from "react";
import EventsForm from "../forms/events-form";
import FullscreenModal from "../modals/modal-fullscreen";
import { ArrowBack } from "@mui/icons-material";
import ActivitiesForm from "../forms/activities-form";
import PlacesToVisitForm from "../forms/places-to-visit-form";

export default function HeaderSecondary(props: any) {
    const router = useRouter();

    // - - - - - - - - -
    // Go Back adjustment
    // - - - - - - - - -
    const finalSlashIndex = router.asPath.lastIndexOf(
        "/",
        router.asPath.lastIndexOf("/") - 1
    );
    const previousPath = router.asPath.slice(0, finalSlashIndex);
    console.log("previousPath");
    console.log(previousPath);

    // - - - - - - - - -
    // Show modal and title
    // - - - - - - - - -
    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState("");

    // - - - - - - - - -
    // Add New or Edit
    // - - - - - - - - -
    const [recordForEdit, setRecordForEdit] = useState(null);

    return (
        <>
            {props.isAuth && (
                <FullscreenModal
                    title={modalTitle}
                    showModal={showModal}
                    setShowModal={setShowModal}
                >
                    {props.addNew === "event" && (
                        <EventsForm
                            recordForEdit={recordForEdit}
                            userId={props.userDetails?.id}
                            userDetails={props.userDetails}
                        />
                    )}
                    {props.addNew === "activity" && (
                        <ActivitiesForm
                            recordForEdit={recordForEdit}
                            userId={props.userDetails?.id}
                            userDetails={props.userDetails}
                        />
                    )}
                    {props.addNew === "placesToVisit" && (
                        <PlacesToVisitForm
                            recordForEdit={recordForEdit}
                            userId={props.userDetails?.id}
                            userDetails={props.userDetails}
                        />
                    )}
                    {/* {props.addNew === "service" && (
                        <ServicesForm
                            recordForEdit={recordForEdit}
                            userId={props.userDetails?.id}
                            userDetails={props.userDetails}
                        />
                    )} */}
                </FullscreenModal>
            )}

            {/* <IonHeader> */}
            {props.isToolbar && (
                <IonToolbar color="medium">
                    <Grid
                        container
                        className="container"
                        sx={{ display: "flex" }}
                    >
                        {props.isBack && (
                            <Box>
                                {/* <Link href="../"> */}
                                <Button
                                    startIcon={<ArrowBack />}
                                    sx={{
                                        display: { xs: "none", sm: "flex" },
                                        justifyContent: "start",
                                    }}
                                    onClick={() => router.back()}
                                >
                                    {" "}
                                    Back{" "}
                                </Button>
                                {/* </Link> */}

                                {/* Back Icon Button - XS */}
                                <IconButton
                                    color="primary"
                                    size="large"
                                    sx={{
                                        mr: -1,
                                        display: { xs: "flex", sm: "none" },
                                    }}
                                    onClick={() => router.back()}
                                >
                                    <ArrowBack />
                                </IconButton>
                            </Box>
                        )}

                        {/* <Button
                        variant="contained"
                        size="small"
                        startIcon={<AddCircleOutlineIcon />}
                        sx={{
                            mr: 2,
                            display: { xs: "none", sm: "inline-flex" },
                        }}
                        onClick={() => {
                            setShowModal(true);
                        }}
                    >
                        Post
                    </Button> */}

                        <IonTitle color="light">
                            {/* <Grid container> */}
                            {/* <Grid item xs={12}> */}
                            <Typography
                                variant="h2"
                                sx={{
                                    overflow: "hidden",
                                    textOverflow: "ellipsis",
                                    display: "-webkit-box",
                                    WebkitLineClamp: "2",
                                    WebkitBoxOrient: "vertical",
                                }}
                            >
                                {props.title}
                            </Typography>
                            {/* </Grid> */}
                            {/* </Grid> */}
                        </IonTitle>

                        {/* <Button
                            sx={{ display: "flex", justifyContent: "end" }}
                            variant="contained"
                            size="small"
                            color="primary"
                            onClick={() => {
                                setShowModal(true);
                                setRecordForEdit(null);
                                setModalTitle("Create Event");
                            }}
                        >
                            Add New
                        </Button> */}
                    </Grid>
                </IonToolbar>
            )}
            {/* </IonHeader> */}

            {/* Single View Header */}
            {props.isSingleView && (
                <Box className="single-view-header">
                    <CardMedia
                        component="img"
                        sx={{
                            height: {
                                xs: "120px",
                                md: "240px",
                            },
                        }}
                        image={props.image}
                        alt={props.item?.name}
                    />
                    <Container
                        sx={{
                            mt: 3,
                            pb: 4,
                            display: "flex",
                            justifyContent: "start",
                            alignItems: "center",
                        }}
                        className="single-view-header-content"
                    >
                        {props.isBack && (
                            <Box sx={{ display: "flex", alignItems: "center" }}>
                                {/* <Link scroll={false} href="./"> */}
                                <Button
                                    startIcon={<ArrowBack />}
                                    sx={{
                                        display: {
                                            xs: "none",
                                            sm: "flex",
                                        },
                                        justifyContent: "start",
                                        ml: "-20px",
                                    }}
                                    onClick={() => router.push(previousPath)}
                                    // onClick={() => router.back()}
                                    // onClick={() =>
                                    //     router.push({
                                    //         pathname: "./",
                                    //         // scroll: false,
                                    //     })
                                    // }
                                >
                                    {" "}
                                    Back{" "}
                                </Button>
                                {/* </Link> */}

                                {/* Back Icon Button - XS */}
                                <IconButton
                                    color="primary"
                                    size="large"
                                    sx={{
                                        display: {
                                            xs: "iunline-flex",
                                            sm: "none",
                                        },
                                        ml: "-4px",
                                        mr: 1,
                                    }}
                                    onClick={() => router.push(previousPath)}
                                    // onClick={() => router.back()}
                                >
                                    <ArrowBack />
                                </IconButton>

                                <Divider
                                    orientation="vertical"
                                    sx={{
                                        display: "inline-flex",
                                        height: "56px",
                                        mr: 3,
                                        borderColor: "#fff",
                                        borderWidth: "1px",
                                        width: "0px",
                                        opacity: 0.24,
                                    }}
                                />
                            </Box>
                        )}
                        <Box
                            sx={{
                                display: "block",
                                // justifyContent: "start",
                            }}
                        >
                            <Typography variant="h2" className="txt-truncate-3">
                                {props.item?.name}
                            </Typography>
                            {/* <Typography variant="h6" className="txt-truncate-3">
                                <i>{props.item?.categoryTypes[0]?.name}</i>
                            </Typography> */}
                        </Box>
                    </Container>
                </Box>
            )}
        </>
    );
}
