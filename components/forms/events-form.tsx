import {
    postData,
    putData,
    getData,
    getPublicData,
    postDataFile,
    putDataFile,
    deleteData,
} from "../../services/data.service";
import {
    Button,
    Grid,
    Box,
    TextField,
    FormControl,
    InputLabel,
    Autocomplete,
    Select,
    MenuItem,
    OutlinedInput,
    SelectChangeEvent,
    Checkbox,
    ListItemText,
    Typography,
    Divider,
    CardMedia,
} from "@mui/material";
// https://mui.com/x/react-date-pickers/
// https://mui.com/x/react-date-pickers/adapters-locale/
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";

// https://day.js.org/docs/en/display/format
import dayjs from "dayjs";
// import advancedFormat from "dayjs/plugin/advancedFormat";
// import localizedFormat from "dayjs/plugin/localizedFormat";
// import relativeTime from "dayjs/plugin/relativeTime";
// import calendar from "dayjs/plugin/calendar";
// import utc from "dayjs/plugin/utc";
// import timezone from "dayjs/plugin/timezone";

// Load plugins
// dayjs.extend(advancedFormat);
// dayjs.extend(localizedFormat);
// dayjs.extend(relativeTime);
// dayjs.extend(calendar);
// dayjs.extend(utc);
// dayjs.extend(timezone);

import { useForm, Controller, useFieldArray } from "react-hook-form";
import { useRouter } from "next/router";
import { useRouter as navUseRouter } from "next/navigation";
import { useEffect, useRef, useState } from "react";
import CountrySelection from "../inputs/country";
import fileResizer from "../hooks/file-resizer";
import slugConverter from "../hooks/slug-converter";
import addressFormatter from "../hooks/address-formatter";

// import { useErrorBoundary } from "react-error-boundary";
import SnackbarComp from "../common/snackbars";
import RichContentEditor from "../common/rich-editor";
import ModalConfirm from "../modals/modal-confirm";
import { Router } from "react-router-dom";

// const ITEM_HEIGHT = 48;
// const ITEM_PADDING_TOP = 8;
// const MenuProps = {
//     PaperProps: {
//         style: {
//             maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
//             width: 250,
//         },
//     },
// };

export default function EventsForm(props) {
    const router = useRouter();
    const routerNav = navUseRouter();
    const {
        register,
        handleSubmit,
        control,
        setValue,
        formState: { isSubmitting, isDirty, isValid, errors },
    } = useForm({
        defaultValues: props.recordForEdit,
        mode: "onChange",
    });
    const { fields, append, prepend, remove, swap, move, insert } =
        useFieldArray({
            control, // control props comes from useForm (optional: if you are using FormContext)
            name: "address", // unique name for your Field Array
        });
    // const inputRef = useRef(null);

    // Form messages
    const formMessages = {
        name: "Name is required",
        areas: "Area is required",
        city: "City/Town/Village is required",
        postcode: "Postcode is required",
        startDate: "Start date is required",
        shortDescription: "Short Description is required",
    };
    // const eventCreatedById = props.userId;
    // const eventUpdatedById = props.userId;
    const userId = props.userId;
    const apiUrl = "events";

    // Message handlers
    const [showSnackbar, setShowSnackbar] = useState(false);
    const [snackbarType, setSnackbarType] = useState("");
    const [snackbarMessage, setSnackbarMessage] = useState("");
    // const { showBoundary } = useErrorBoundary();

    console.log("EventsForm props");
    console.log(props);
    console.log("recordForEdit");
    console.log(props.recordForEdit);
    console.log("router");
    console.log(router.query.area);
    console.log(router.query.slug);
    console.log(router.pathname);
    console.log(router.query);
    console.log(router.asPath);
    console.log(router.basePath);
    console.log(router.domainLocales);
    console.log(router.locales);
    console.log(router.query);
    console.log(router.route);

    // - - - - - - - -
    // Submit Edit or New Events
    // - - - - - - - -
    const [isProcessing, setIsProcessing] = useState(false);
    async function onSubmit({
        name,
        categories,
        areas,
        // categoryTypes,
        // price,
        featuredImage,
        featuredImageUrl,
        url1,
        url2,
        contactEmail,
        contactMobile,
        contactPhone,
        contactPersonName,
        startDate,
        endDate,
        startTime,
        endTime,
        // eventStartDate,
        // eventEndDate,
        // eventStartTime,
        // eventEndTime,
        shortDescription,
        description,
        address,
    }) {
        console.log("isSubmitting");
        console.log(isSubmitting);
        console.log("isValid");
        console.log(isValid);
        console.log("isDirty");
        console.log(isDirty);
        console.log("errors");
        console.log(errors);

        // if (name.length === 0) {
        //     setSnackbarType("warning");
        //     setSnackbarMessage(formMessages.name);
        //     setShowSnackbar(true);
        // }
        // Start processing load
        setIsProcessing(true);

        // Address formatter
        console.log("address");
        console.log(address);
        address = address ? addressFormatter(await address) : {};

        // Slug converter
        const slug = await slugConverter(
            // props.recordForEdit.name ? props.recordForEdit.name : name, // safe if page already shared
            name, // more accurate for SEO
            "withDate",
            props.recordForEdit ? props.recordForEdit.createdAt : null
        );

        // Resize file, Upload and set as featured image
        if (featuredImage) {
            console.log("featuredImage");
            console.log(await featuredImage);
            console.log(await featuredImage.size);

            setSnackbarType("info");
            setSnackbarMessage(
                `featuredImage: ${(await featuredImage.size) / 1000} kb`
            );
            setShowSnackbar(true);
        }

        // let resizedImage: any;
        // if (await featuredImage) {
        //     // const file = await featuredImage[0];
        //     // resizedImage = await fileResizer(file, "image");
        //     resizedImage = featuredImage;
        // }
        // console.log("resizedImage");
        // console.log(resizedImage);

        // const dateDefault = new Date("0000-01-01T00:00:00.00Z");
        // if (startDate === null) {
        //     startDate = dateDefault;
        // }
        // if (endDate === null) {
        //     endDate = dateDefault;
        // }
        // if (startTime === null) {
        //     startTime = dateDefault;
        // }
        // if (endTime === null) {
        //     endTime = dateDefault;
        // }

        // Category Type
        const categoryTypes = [{ name: "Event" }];

        const dataBody = {
            // userId,
            name,
            slug,
            categories,
            areas,
            categoryTypes,
            // price,
            // asset,
            featuredImageUrl,
            // featuredImageId,
            url1,
            url2,
            contactEmail,
            contactMobile,
            contactPhone,
            contactPersonName,
            startDate,
            endDate,
            startTime,
            endTime,
            // eventStartDate,
            // eventEndDate,
            // eventStartTime,
            // eventEndTime,
            description,
            shortDescription,
            address,
        };
        console.log("dataBody");
        console.log(dataBody);

        // - - - - -
        // Edit
        // - - - - -
        if (props.recordForEdit) {
            // const updatedBy = props.userId;
            // Object.assign(dataBody, { eventUpdatedById: await userId });

            // if (resizedImage .length > 0) {
            if (await featuredImage) {
                console.log("dataBody Update featuredImage");
                console.log(featuredImage);
                console.log([featuredImage].length);
                // const newAsset = await putDataFile(
                //     "assets",
                //     props.recordForEdit.id,
                //     featuredImage
                // );
                const newAsset = await postDataFile("assets", [
                    await featuredImage,
                ]);
                console.log("fileResponse");
                console.log(newAsset);

                Object.assign(dataBody, {
                    // Object.assign(dataBody, {
                    asset: { id: await newAsset.id },
                    featuredImageId: await newAsset.id,
                    featuredImageUrl: await newAsset.url,
                });
            }

            console.log("dataBody Update");
            console.log(dataBody);

            await putData(apiUrl, props.recordForEdit.id, dataBody).then(
                (response) => {
                    console.log("putData response");
                    console.log(response);

                    setShowSnackbar(false);
                    setSnackbarType("success");
                    setSnackbarMessage(
                        `${response.name} successfully updated!`
                    );
                    setShowSnackbar(true);

                    refreshRouter(response.slug);
                },
                (error) => {
                    console.log("putData error");
                    console.log(error);

                    let message = "";
                    if (props.userDetails.role[0] === "admin") {
                        message = `${error.message} || ${error.response.data.message} (code: ${error.response.data.code}) `;
                    } else {
                        message = error.message;
                    }
                    setShowSnackbar(false);
                    setSnackbarType("error");
                    setSnackbarMessage(message);
                    setShowSnackbar(true);
                }
            );
        }
        // - - - - -
        // Add New
        // - - - - -
        else {
            // if (featuredImage.length > 0) {
            if (await featuredImage) {
                console.log("dataBody Create featuredImage");
                console.log(featuredImage);
                console.log([featuredImage].length);
                const newAsset = await postDataFile("assets", [
                    await featuredImage,
                ]);
                console.log("fileResponse");
                console.log(newAsset);
                // const asset = { id: newAsset.data.id };
                // const featuredImageId = newAsset.data.id;
                // const featuredImageUrl = newAsset.data.url;
                // // console.log(asset);
                // console.log(featuredImageId);
                // console.log(featuredImageUrl);

                Object.assign(dataBody, {
                    asset: { id: await newAsset.id },
                    featuredImageId: await newAsset.id,
                    featuredImageUrl: await newAsset.url,
                });
            }

            // Object.assign(dataBody, { eventCreatedById: await userId });
            console.log("dataBody New");
            console.log(dataBody);

            await postData(apiUrl, dataBody).then(
                (response) => {
                    console.log("postData response");
                    console.log(response);

                    setShowSnackbar(false);
                    setSnackbarType("success");
                    setSnackbarMessage(
                        `${response.name} successfully created!`
                    );
                    setShowSnackbar(true);

                    refreshRouter(response.slug);
                },
                (error) => {
                    console.log("postData error");
                    console.log(error);

                    let message = "";
                    if (props.userDetails.role[0] === "admin") {
                        message = `${error.message} || ${error.response.data.message} (code: ${error.response.data.code}) `;
                    } else {
                        message = error.message;
                    }
                    setShowSnackbar(false);
                    setSnackbarType("error");
                    setSnackbarMessage(message);
                    setShowSnackbar(true);
                }
            );
        }
    }

    // - - - - - -
    // Handle Delete
    // - - - - - -
    const [showConfirmModal, setShowConfirmModal] = useState(false);
    const deleteItem = async (e: boolean) => {
        if (e) {
            setShowConfirmModal(false);

            if (props.recordForEdit) {
                await deleteData(apiUrl, props.recordForEdit.id).then(
                    (response) => {
                        console.log("deleteItem response");
                        console.log(response);

                        setSnackbarType("success");
                        setSnackbarMessage(
                            `${response.name} successfully deleted!`
                        );
                        setShowSnackbar(true);
                        refreshRouter();
                    },
                    (error) => {
                        console.log("deleteItem error");
                        console.log(error);

                        let message = "";
                        if (props.userDetails.role[0] === "admin") {
                            message = `${error.message} || ${error.response.data.message} (code: ${error.response.data.code}) `;
                        } else {
                            message = error.message;
                        }
                        setSnackbarType("error");
                        setSnackbarMessage(message);
                        setShowSnackbar(true);
                    }
                );
            }
        } else {
            setShowConfirmModal(false);
        }
    };

    // - - - - - - - -
    // Refresh router
    // - - - - - - - -
    const refreshRouter = (slug?: string) => {
        let pushUrl: string;
        if (slug) {
            pushUrl = `/${router.query.area}/events/${slug}`;
        } else {
            pushUrl = `/${router.query.area}/events/`;
        }
        router.push(pushUrl).then((resp) => {
            routerNav.refresh();
            setIsProcessing(false);
        });
        // setTimeout(() => {
        //     routerNav.refresh();
        //     setIsProcessing(false);
        // }, 2000);
    };

    // - - - - - - - -
    // Handle file selection and preview
    // - - - - - - - -
    const [previewFile, setPreviewFile] = useState("");
    const handleFileSelection = async (e) => {
        console.log("handleFileSelection");
        console.log(e);
        console.log(e.target.value);
        console.log(e.target.files);

        setShowSnackbar(false);
        // setSnackbarType("info");
        // setSnackbarMessage(
        //     `e.target: ${await e.target.value} and ${JSON.stringify(
        //         e.target.files
        //     )}`
        // );
        // setShowSnackbar(true);

        let resizedImage: any;
        if (await e.target.value) {
            // Set image preview and prepare URL to resize image
            const [previewfile] = await e.target.files;
            const fileURL = URL.createObjectURL(previewfile);
            setPreviewFile(fileURL);
            console.log("fileURL");
            console.log(fileURL);

            // Resize file, Upload and set as featured image
            console.log("e.target.files");
            console.log(e.target.files);
            console.log(e.target.files[0]);

            const file = await e.target.files[0];
            console.log("file");
            console.log(file);
            console.log(file.name);

            // v1 with react file resizer package
            resizedImage = await fileResizer(file, "image");

            // v2 raw
            // await imageResizer(fileURL, 2000, file.name).then(async (resp) => {
            //     resizedImage = await resp;
            // });

            console.log("resizedImage");
            console.log(await resizedImage);
            console.log(await resizedImage.size);

            setSnackbarType("info");
            setSnackbarMessage(
                `resizedImage:
                ${await resizedImage.name}, 
                    ${((await resizedImage.size) / Math.pow(10, 3)).toFixed(
                        2
                    )} kb
                    (${((await resizedImage.size) / Math.pow(10, 6)).toFixed(
                        2
                    )} mb) 
                   
                    || Original: ${(
                        (await file.size) / Math.pow(10, 3)
                    ).toFixed(2)} kb
                    (${((await file.size) / Math.pow(10, 6)).toFixed(2)} mb)`
            );
            setShowSnackbar(true);

            setValue("featuredImage", await resizedImage);
        }
    };

    // - - - - - - - -
    // Handle date and time picker
    // - - - - - - - -
    const [startDate, setStartDate] = useState(
        props.recordForEdit?.startDate !== null &&
            props.recordForEdit?.startDate !== ""
            ? dayjs(props.recordForEdit?.startDate).format("YYYY-MM-DD")
            : null
        // props.recordForEdit?.startDate !== null
        //     ? dayjs(props.recordForEdit?.startDate).format("YYYY-MM-DD")
        //     : dayjs(new Date("0000-01-01")).format("YYYY-MM-DD")
        // props.recordForEdit?.startDate !== null ||
        //     props.recordForEdit?.startDate !== "0000-01-01T00:00:00.000Z"
        //     ? dayjs(props.recordForEdit?.startDate).format("YYYY-MM-DD")
        //     : dayjs(new Date("0000-01-01T00:00:00.00Z")).format("YYYY-MM-DD") // "0000-01-01T00:00:00.00Z" || 0000-00-00
    );
    console.log("startDate");
    console.log(startDate);
    console.log(props.recordForEdit?.startDate);

    async function handleStartDate(e) {
        console.log("handleStartDate");
        console.log(e);
        console.log(new Date(e));

        let dateValue: any;
        if (!isNaN(new Date(e).getTime()) && e !== null) {
            dateValue = new Date(e);
        } else {
            dateValue = null;
            // dateValue = "0000-01-01T00:00:00.000Z";
        }
        console.log(dateValue);

        setValue("startDate", dateValue);

        // const adjDate = e?.format?.("YYYY-MM-DD");
        // console.log(adjDate);
        // setValue("eventStartDate", adjDate);

        // console.log(e?.format?.("MMMM D YYYY"));
        // console.log(e?.format?.("DD/MM/YYYY"));
        // console.log(e?.format?.("YYYY/MM/DD"));
        // console.log(e?.format?.("DD-MM-YYYY"));
        // console.log(e?.format?.("YYYY-MM-DD"));

        // setValue("eventStartDate", e?.format?.("DD-MM-YYYY"));
        // console.log(new Date(e));
        // setValue("eventStartDate", new Date(e).toLocaleDateString());
        // console.log(new Date(e).toISOString());
        // setValue("eventStartDate", new Date(e).toISOString());
        // setValue("eventStartDate", e);
    }

    const [endDate, setEndDate] = useState(
        // dayjs(props.recordForEdit?.endDate).format("YYYY-MM-DD") || null
        props.recordForEdit?.endDate !== null &&
            props.recordForEdit?.endDate !== ""
            ? dayjs(props.recordForEdit?.endDate).format("YYYY-MM-DD")
            : null

        // props.recordForEdit?.endDate !== "0000-01-01T00:00:00.000Z"
        //     ? dayjs(props.recordForEdit?.endDate).format("YYYY-MM-DD")
        //     : dayjs("") // new Date("0000-00-00") // "0000-01-01T00:00:00.00Z"
        // props.recordForEdit?.endDate !== "0000-01-01T00:00:00.000Z"
        //     ? dayjs(props.recordForEdit?.endDate).format("YYYY-MM-DD")
        //     : new Date("0000-01-01T00:00:00.00Z") // "0000-01-01T00:00:00.00Z"
    );

    console.log("endDate");
    console.log(endDate);
    console.log(props.recordForEdit?.endDate);

    async function handleEndDate(e) {
        console.log("handleEndDate");
        console.log(e);
        console.log(new Date(e));

        let dateValue: any;
        if (!isNaN(new Date(e).getTime()) && e !== null) {
            dateValue = new Date(e);
        } else {
            dateValue = null;
            // dateValue = "0000-01-01T00:00:00.000Z";
        }
        console.log(dateValue);

        setValue("endDate", dateValue);

        // const adjDate = e?.format?.("YYYY-MM-DD");
        // console.log(adjDate);
        // setValue("eventEndDate", adjDate);

        // console.log(e?.format?.("DD-MM-YYYY"));
        // setValue("eventEndDate", e?.format?.("DD-MM-YYYY"));
        // console.log(new Date(e));
        // setValue("eventEndDate", new Date(e).toLocaleDateString());
        // console.log(new Date(e).toISOString());
        // setValue("eventEndDate", new Date(e).toISOString());
        // setValue("eventEndDate", new Date(e));
        // setValue("eventEndDate", e);
    }

    const [startTime, setStartTime] = useState(
        props.recordForEdit?.startTime !== null &&
            props.recordForEdit?.startTime !== ""
            ? dayjs(props.recordForEdit?.startTime).format("YYYY-MM-DDTHH:mm")
            : null
        // props.recordForEdit?.startTime !== "0000-01-01T00:00:00.000Z"
        //     ? dayjs(props.recordForEdit?.startTime).format("YYYY-MM-DDTHH:mm")
        //     : new Date("0000-00-00T00:00:00") // "0000-01-01T00:00:00.00Z"
        // dayjs(props.recordForEdit?.startTime).format("YYYY-MM-DDTHH:mm") || null
    );

    console.log("startTime");
    console.log(startTime);
    console.log(props.recordForEdit?.startTime);

    async function handleStartTime(e) {
        console.log("handleStartTime");
        console.log(e);
        console.log(new Date(e));

        let dateValue: any;
        if (!isNaN(new Date(e).getTime()) && e !== null) {
            dateValue = new Date(e);
        } else {
            dateValue = null;
            // dateValue = "0000-01-01T00:00:00.000Z";
        }
        console.log(dateValue);

        setValue("startTime", dateValue);

        // console.log(e?.format("hh:mm A"));
        // setValue("eventStartTime", e?.format("hh:mm A"));

        // console.log(newDate.toLocaleString());
        // console.log(newDate.toLocaleTimeString());
        // setValue("eventStartTime", newDate.toLocaleTimeString());
        // console.log(new Date(e).toISOString());
        // setValue("eventStartTime", new Date(e).toISOString());
    }

    const [endTime, setEndTime] = useState(
        props.recordForEdit?.endTime !== null &&
            props.recordForEdit?.endTime !== ""
            ? dayjs(props.recordForEdit?.endTime).format("YYYY-MM-DDTHH:mm")
            : null
        // props.recordForEdit?.endTime !== "0000-01-01T00:00:00.000Z"
        //     ? dayjs(props.recordForEdit?.endTime).format("YYYY-MM-DDTHH:mm")
        //     : new Date("0000-00-00T00:00:00") // "0000-01-01T00:00:00.00Z"
        // dayjs(props.recordForEdit?.endTime).format("YYYY-MM-DDTHH:mm") || null
    );

    console.log("endTime");
    console.log(endTime);
    console.log(props.recordForEdit?.endTime);

    async function handleEndTime(e) {
        console.log("handleEndTime");
        console.log(e);
        console.log(new Date(e));

        let dateValue: any;
        if (!isNaN(new Date(e).getTime()) && e !== null) {
            dateValue = new Date(e);
        } else {
            dateValue = null;
            // dateValue = "0000-01-01T00:00:00.000Z";
        }
        console.log(dateValue);

        setValue("endTime", dateValue);

        // console.log(e?.format("hh:mm A"));
        // setValue("eventEndTime", e?.format("hh:mm A"));

        // console.log(newDate.toLocaleString());
        // console.log(newDate.toLocaleTimeString());
        // setValue("eventEndTime", newDate.toLocaleTimeString());
        // console.log(new Date(e).toISOString());
        // setValue("eventEndTime", new Date(e).toISOString());
    }

    // - - - - - - -
    // Get All selections data
    // - - - - - - -
    // const [dataCategoryTypes, setDataCategoryTypes] = useState([]);
    const [dataCategories, setDataCategories] = useState([]);
    const [dataEventAreas, setDataEventAreas] = useState([]);
    const getAll = async () => {
        // const categoryTypesRawData = await getPublicData(
        //     "category-types/selection"
        // );
        // setDataCategoryTypes(categoryTypesRawData);

        const categoriesRawData = await getPublicData("categories/selection");
        setDataCategories(categoriesRawData);

        const areasRawData = await getPublicData("location/areas/selection");
        setDataEventAreas(areasRawData);

        console.log("dataEventAreas");
        console.log(dataEventAreas);
    };

    // - - - - - -
    // Event Category Types
    // - - - - - -
    // // Search autocomplete input
    // const [categoryTypesProp, setCategoryTypesProp] = useState(
    //     props.recordForEdit?.categoryTypes[0]?.id || ""
    //     // props.recordForEdit?.categoryTypes || []
    // );
    // // https://mui.com/material-ui/react-select/
    // const handleCategoryTypeSelection = (data: any) => {
    //     console.log("handleCategoryTypeSelection");
    //     console.log(data);
    //     console.log(data.target.value);

    //     setCategoryTypesProp(data.target.value);
    //     setValue("categoryTypes", [{ id: data.target.value }]);
    // };

    // - - - - - -
    // Event Categories
    // - - - - - -
    const [categoriesProp, setCategoriesProp] = useState(
        props.recordForEdit?.categories || []
    );
    // const handleCategoriesChange = (event: SelectChangeEvent) => {
    const handleCategoriesSelection = (data: any) => {
        // const {
        //     target: { value },
        // } = event;
        // setCategoriesProp(
        //     // On autofill we get a stringified value.
        //     // typeof value === "string" ? value.split(",") : value
        //     { name: value.name, id: value.id }
        // );
        // setValue("categories", data);

        console.log("handleCategoriesSelection");
        console.log(data);

        setValue("categories", selectionNameRemoval(data));
    };

    // - - - - - -
    // Event Areas
    // - - - - - -
    const [areasProp, setEventAreasProp] = useState(
        props.recordForEdit?.areas || []
        // props.recordForEdit?.areas || dataEventAreas[2]
    );
    const handleEventAreasSelection = (data: any) => {
        console.log("handleEventAreasSelection");
        console.log(data);

        setValue("areas", selectionNameRemoval(data));
    };

    const selectionNameRemoval = (data: any) => {
        let newObj = [];
        data.forEach((item) => {
            const newObjTemp = Object.keys(item)
                .filter((key) => key != "name")
                .filter((key) => key != "slug")
                .reduce((acc, key) => {
                    acc[key] = item[key];
                    return acc;
                }, {});
            newObj.push(newObjTemp);
        });

        console.log(newObj);
        return newObj;
    };

    // - - - - - - -
    // Country selection
    // - - - - - - -
    const [selectedCountry, setSelectedCountry] = useState(
        props.recordForEdit?.address[0]?.country || []
    );
    const handleSelectedCountry = (data: any) => {
        console.log("handleSelectedCountry data");
        console.log(data);
        setSelectedCountry([{ id: data?.id }]);
        setValue("address[0].country[0].id", data?.id);
    };

    // - - - - - -
    // Description
    // - - - - - -
    const handleDescription = (data: any) => {
        console.log("handleDescription");
        console.log(data);

        setValue("description", data);
    };

    useEffect(() => {
        (async () => {
            getAll();
        })();
        // setCategoryTypesProp(props.recordForEdit?.categoryTypes[0]?.id ?? "");
        // setCategoriesProp(props.recordForEdit?.categories ?? []);
        if (props.recordForEdit) {
            // setValue("categoryTypes", [
            //     { id: props.recordForEdit?.categoryTypes[0]?.id },
            // ]);
            setValue(
                "categories",
                selectionNameRemoval(props.recordForEdit?.categories)
            );
            setValue("areas", selectionNameRemoval(props.recordForEdit?.areas));
        }
    }, [props]);

    console.log("categoriesProp");
    console.log(categoriesProp);

    return (
        <>
            {showSnackbar && (
                <SnackbarComp
                    type={snackbarType}
                    message={snackbarMessage}
                    // transition="TransitionUp"
                />
            )}

            {showConfirmModal && (
                <ModalConfirm
                    title="Are you sure to Delete"
                    showConfirmModal={showConfirmModal}
                    setShowConfirmModal={setShowConfirmModal}
                    recordForEdit={props.recordForEdit}
                    deleteItem={(e: boolean) => deleteItem(e)}
                />
            )}

            <Box
                component="form"
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit(onSubmit)}
            >
                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 6,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">Event Details</Typography>
                    </Grid>

                    <Grid item xs={12} sm={12} md={12} lg={6}>
                        <TextField
                            fullWidth
                            label="Name"
                            type="text"
                            {...register("name", { required: true })}
                            color={errors.name ? "error" : "primary"}
                            required
                        />
                        {errors.name && (
                            <Typography variant="body2" color="error">
                                {formMessages.name}
                            </Typography>
                        )}
                    </Grid>
                    <Grid
                        item
                        xs={12}
                        sm={12}
                        md={6}
                        lg={4}
                        sx={{ display: "flex", justifyContent: "start" }}
                    >
                        <TextField
                            fullWidth
                            label="Featured Image URL"
                            type="text"
                            {...register("featuredImageUrl")}
                        />
                    </Grid>
                    <Grid
                        item
                        xs={12}
                        sm={12}
                        md={6}
                        lg={2}
                        sx={{
                            display: "flex",
                            justifyContent: "start",
                            // mt: -2,
                            mb: 1,
                        }}
                    >
                        {previewFile && (
                            <CardMedia
                                component="img"
                                image={previewFile}
                                sx={{
                                    maxWidth: "56px",
                                    maxHeight: "56px",
                                    mr: 1,
                                    mb: -2,
                                }}
                            />
                        )}
                        <Button
                            variant="contained"
                            component="label"
                            fullWidth
                            style={{ minWidth: "140px" }}
                        >
                            Choose File
                            <input
                                type="file"
                                name="Featured Image"
                                // accept="image/*"
                                hidden
                                onChange={handleFileSelection}
                                // onChange={(e) => handleFileSelection(e)}
                                // onInput={(e) => {
                                //     handleFileSelection(e);
                                // }}
                                // {...register("featuredImage")}
                            />
                        </Button>
                    </Grid>

                    {/* <Grid item xs={12} sm={12} md={4}>
                        <FormControl fullWidth>
                            <InputLabel id="type-selection-label">
                                Type
                            </InputLabel>
                            <Select
                                labelId="type-selection-label"
                                onChange={(e) => handleCategoryTypeSelection(e)}
                                value={categoryTypesProp}
                                // defaultValue={categoryTypesProp}
                                // renderValue={() => categoryTypesProp[0].id}
                                // inputProps={register("categoryTypes", {
                                //     required: "Please choose",
                                // })}
                            >
                                {dataCategoryTypes.map((item, i) => (
                                    <MenuItem key={i} value={item.id}>
                                        {item.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid> */}

                    <Grid item xs={12} sm={12} md={4}>
                        <Controller
                            control={control}
                            name="Categories"
                            defaultValue={categoriesProp}
                            render={({
                                field: { ref, onChange, ...field },
                            }) => (
                                <Autocomplete
                                    multiple
                                    options={dataCategories}
                                    defaultValue={categoriesProp}
                                    getOptionLabel={(option) => option.name}
                                    onChange={(_, data) => {
                                        onChange(data);
                                        handleCategoriesSelection(data);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...field}
                                            {...params}
                                            fullWidth
                                            // inputRef={ref}
                                            // variant="filled"
                                            label="Categories"
                                            // inputProps={register("categories", {
                                            //     required: "Please choose",
                                            // })}
                                        />
                                    )}
                                />
                            )}
                            // {...register("categories")}
                        />
                    </Grid>

                    <Grid item xs={12} sm={12} md={4}>
                        <Controller
                            control={control}
                            name="Event Areas"
                            defaultValue={areasProp}
                            render={({
                                field: { ref, onChange, ...field },
                            }) => (
                                <Autocomplete
                                    multiple
                                    options={dataEventAreas}
                                    defaultValue={areasProp}
                                    getOptionLabel={(option) => option.name}
                                    onChange={(_, data) => {
                                        onChange(data);
                                        handleEventAreasSelection(data);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...field}
                                            {...params}
                                            fullWidth
                                            label="Event Areas"
                                            // inputRef={ref}
                                            // variant="filled"
                                            // inputProps={register("areas", {
                                            //     required: true,
                                            // })}
                                            required
                                            // {...register("areas", {
                                            //     required: true,
                                            // })}
                                            error={!!errors["Event Areas"]}
                                            helperText={
                                                errors["Event Areas"]
                                                    ? formMessages.areas
                                                    : ""
                                            }
                                        />
                                    )}
                                />
                            )}
                            // {...register("areas")}
                        />
                        {/* <TextField
                            fullWidth
                            label="Name"
                            type="text"
                            {...register("name", { required: true })}
                            color={errors.name ? "error" : "primary"}
                            required
                        /> */}
                        {/* {errors.areas && (
                            <Typography variant="body2" color="error">
                                {formMessages.areas}
                            </Typography>
                        )} */}
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 6,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">Event Place</Typography>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Address Line 1"
                            type="text"
                            // onChange={(e) =>
                            //     handleAddress(e.target.value, "addressLine1")
                            // }
                            {...register("address[0].addressLine1")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Address Line 2"
                            type="text"
                            // onChange={(e) =>
                            //     handleAddress(e.target.value, "addressLine2")
                            // }
                            {...register("address[0].addressLine2")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Address Line 3"
                            type="text"
                            {...register("address[0].addressLine3")}
                        />
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="City/Town/Village"
                            type="text"
                            // {...register("address[0].city")}
                            {...register("address[0].city", { required: true })}
                            color={errors.address ? "error" : "primary"}
                            required
                        />
                        {/* {errors.address && (
                            <Typography variant="body2" color="error">
                                {formMessages.city}
                            </Typography>
                        )} */}
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Postcode"
                            type="text"
                            {...register("address[0].postcode", {
                                required: true,
                            })}
                            color={errors.address ? "error" : "primary"}
                            required
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="County"
                            type="text"
                            {...register("address[0].county")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Region"
                            type="text"
                            {...register("address[0].region")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <CountrySelection
                            handleSelectedCountry={(e) =>
                                handleSelectedCountry(e)
                            }
                            selectedCountry={selectedCountry}
                        />
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 6,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">
                            Event Date and Time
                        </Typography>
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DatePicker
                                label="Start Date"
                                slotProps={{
                                    textField: { fullWidth: true },
                                }}
                                format="DD-MM-YYYY"
                                // value={startDate}
                                value={
                                    startDate
                                        ? dayjs(startDate?.toString())
                                        : null
                                }
                                onChange={(e) => handleStartDate(e)}
                                slots={{
                                    textField: (textFieldProps) => (
                                        <TextField
                                            {...textFieldProps}
                                            onChange={() =>
                                                handleStartDate(null)
                                            }
                                        />
                                    ),
                                }}
                                // renderInput={(params: any) => (
                                //     <TextField
                                //         {...params}
                                //         // {...register("eventStartDate")}
                                //     />
                                // )}
                            />
                        </LocalizationProvider>
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <TimePicker
                                label="Start Time"
                                slotProps={{ textField: { fullWidth: true } }}
                                value={
                                    startTime
                                        ? dayjs(startTime.toString())
                                        : null
                                }
                                onChange={(e) => handleStartTime(e)}
                                // {...register("eventStartTime")}
                            />
                        </LocalizationProvider>
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        {/* {endDate} */}
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DatePicker
                                label="End Date"
                                slotProps={{ textField: { fullWidth: true } }}
                                format="DD-MM-YYYY"
                                // value={endDate}
                                defaultValue={
                                    endDate ? dayjs(endDate?.toString()) : null
                                }
                                onChange={(e) => handleEndDate(e)}
                                // {...register("eventEndDate")}
                                slots={{
                                    textField: (textFieldProps) => (
                                        <TextField
                                            {...textFieldProps}
                                            onChange={() => handleEndDate(null)}
                                        />
                                    ),
                                }}
                            />
                        </LocalizationProvider>
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <TimePicker
                                label="End Time"
                                slotProps={{ textField: { fullWidth: true } }}
                                value={
                                    endTime ? dayjs(endTime.toString()) : null
                                }
                                onChange={(e) => handleEndTime(e)}
                                // {...register("eventEndTime")}
                            />
                        </LocalizationProvider>
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 6,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">
                            Event Contact Details
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Contact Email"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("contactEmail")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Contact Mobile"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("contactMobile")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Contact Phone"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("contactPhone")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Contact Person Name"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("contactPersonName")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={6}>
                        <TextField
                            fullWidth
                            label="URL 1"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("url1")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={6}>
                        <TextField
                            fullWidth
                            label="URL 2"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("url2")}
                        />
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 4,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">
                            Event Short Description
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            label="Short Description"
                            multiline
                            rows={4}
                            {...register("shortDescription", {
                                required: true,
                            })}
                            color={
                                errors.shortDescription ? "error" : "primary"
                            }
                            required
                        />
                        {errors.shortDescription && (
                            <Typography variant="body2" color="error">
                                {formMessages.shortDescription}
                            </Typography>
                        )}
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="h5">Event Description</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <RichContentEditor
                            content={props.recordForEdit?.description}
                            setNewContent={(e: any) => handleDescription(e)}
                        />
                        {/* <TextField
                            fullWidth
                            label="Description"
                            multiline
                            rows={4}
                            {...register("description")}
                        /> */}
                    </Grid>

                    <Grid
                        item
                        xs={6}
                        sx={{
                            display: "flex",
                            alignItems: "end",
                            justifyContent: "start",
                        }}
                    >
                        {props.userDetails.role[0] === "admin" &&
                            props.recordForEdit && (
                                <Button
                                    size="large"
                                    variant="outlined"
                                    color="error"
                                    onClick={(e) => setShowConfirmModal(true)}
                                >
                                    Delete
                                </Button>
                            )}
                    </Grid>

                    <Grid
                        item
                        xs={6}
                        sx={{
                            display: "flex",
                            alignItems: "end",
                            justifyContent: "end",
                        }}
                    >
                        <Button
                            type="submit"
                            size="large"
                            variant="contained"
                            color="primary"
                            // disabled={formState.isSubmitting}
                            disabled={!isDirty && !isValid}
                        >
                            {isProcessing &&
                            showSnackbar &&
                            snackbarType !== "error" ? (
                                <Box
                                    sx={{
                                        display: "flex",
                                        width: "80px",
                                        justifyContent: "center",
                                    }}
                                >
                                    <img
                                        width={64}
                                        height={32}
                                        // src="/assets/icons/loading-pulse.svg"
                                        src="/assets/icons/loading-dots-muted.svg"
                                        // src="/assets/icons/loading-ripple-muted.svg"
                                        // src="/assets/icons/loading-ripple.svg"
                                    />
                                </Box>
                            ) : props.recordForEdit ? (
                                <Box
                                    sx={{
                                        display: "flex",
                                        width: "80px",
                                        justifyContent: "center",
                                    }}
                                >
                                    Update
                                </Box>
                            ) : (
                                <Box
                                    sx={{
                                        display: "flex",
                                        width: "80px",
                                        justifyContent: "center",
                                    }}
                                >
                                    Save
                                </Box>
                            )}
                        </Button>
                    </Grid>
                </Grid>
            </Box>
        </>
    );
}
