import { postData, putData, postDataFile } from "../../services/data.service";
import { Button, Grid, Box, TextField, Typography } from "@mui/material";
// https://mui.com/x/react-date-pickers/
import { useForm, Controller, useFieldArray } from "react-hook-form";
import { useRouter } from "next/navigation";
import { useEffect, useRef, useState } from "react";

import SnackbarComp from "../common/snackbars";
import slugConverter from "../hooks/slug-converter";

export default function CategoryTypesForm(props) {
    const router = useRouter();
    const {
        register,
        handleSubmit,
        control,
        formState,
        setValue,
        formState: { isSubmitting, isDirty, isValid, errors },
    } = useForm({
        defaultValues: props.recordForEdit,
    });
    // const inputRef = useRef(null);

    // Form messages
    const formMessages = {
        name: "Name is required",
    };

    const apiUrl = "category-types";

    // Message handlers
    const [showSnackbar, setShowSnackbar] = useState(false);
    const [snackbarType, setSnackbarType] = useState("");
    const [snackbarMessage, setSnackbarMessage] = useState("");
    // const { showBoundary } = useErrorBoundary();

    console.log("CategoryTypesForm props");
    console.log(props);
    console.log("recordForEdit");
    console.log(props.recordForEdit);

    // - - - - - - - -
    // Submit Edit or New Events
    // - - - - - - - -
    async function onSubmit({
        name,
        featuredImage,
        featuredImageUrl,
        shortDescription,
        description,
    }) {
        // Slug converter
        const slug = await slugConverter(name, "withoutDate");

        // Upload assets and set as featured image for event
        if (featuredImage) {
            console.log("featuredImage");
            console.log(await featuredImage);
            console.log(await featuredImage.size);
            // Object.assign(featuredImage[0].file.name, name);
            // const fileResponse = await postDataFile("assets", featuredImage);
        }

        const dataBody = {
            // userId,
            name,
            slug,
            featuredImageUrl,
            description,
            shortDescription,
        };
        console.log("dataBody");
        console.log(dataBody);

        // - - - - -
        // Edit
        // - - - - -
        if (props.recordForEdit) {
            if (featuredImage) {
                console.log("dataBody Update featuredImage");
                console.log(featuredImage);
                console.log(featuredImage.length);

                const newAsset = await postDataFile("assets", featuredImage);
                console.log("fileResponse");
                console.log(newAsset);

                Object.assign(dataBody, {
                    asset: { id: await newAsset.data.id },
                    featuredImageId: await newAsset.data.id,
                    featuredImageUrl: await newAsset.data.url,
                });
            }

            console.log("dataBody Update");
            console.log(dataBody);

            await putData(apiUrl, props.recordForEdit.id, dataBody).then(
                (response) => {
                    console.log("putData response");
                    console.log(response);

                    setSnackbarType("success");
                    setSnackbarMessage(
                        `${response.name} successfully updated!`
                    );
                    setShowSnackbar(true);
                    refreshRouter();
                },
                (error) => {
                    console.log("putData error");
                    console.log(error);

                    let message = "";
                    if (props.userDetails.role[0] === "admin") {
                        message = `${error.message} || ${error.response.data.message} (code: ${error.response.data.code}) `;
                    } else {
                        message = error.message;
                    }
                    setSnackbarType("error");
                    setSnackbarMessage(message);
                    setShowSnackbar(true);
                }
            );
        }
        // - - - - -
        // Add New
        // - - - - -
        else {
            if (featuredImage) {
                console.log("dataBody Create featuredImage");
                console.log(featuredImage);
                console.log(featuredImage.length);

                const newAsset = await postDataFile("assets", featuredImage);
                console.log("fileResponse");
                console.log(newAsset);

                Object.assign(dataBody, {
                    asset: { id: await newAsset.data.id },
                    featuredImageId: await newAsset.data.id,
                    featuredImageUrl: await newAsset.data.url,
                });
            }

            console.log("dataBody New");
            console.log(dataBody);

            await postData(apiUrl, dataBody).then(
                (response) => {
                    console.log("postData response");
                    console.log(response);

                    setSnackbarType("success");
                    setSnackbarMessage(
                        `${response.name} successfully created!`
                    );
                    setShowSnackbar(true);
                    refreshRouter();
                },
                (error) => {
                    console.log("postData error");
                    console.log(error);

                    let message = "";
                    if (props.userDetails.role[0] === "admin") {
                        message = `${error.message} || ${error.response.data.message} (code: ${error.response.data.code}) `;
                    } else {
                        message = error.message;
                    }
                    setSnackbarType("error");
                    setSnackbarMessage(message);
                    setShowSnackbar(true);
                }
            );
        }
    }

    // - - - - - - - -
    // Refresh router
    // - - - - - - - -
    const refreshRouter = () => {
        setTimeout(() => {
            // setIsProcessing(false);
            router.refresh();
        }, 2000);
    };

    // - - - - - - -
    // Get All selections data
    // - - - - - - -
    // const [dataCategoryTypes, setDataCategoryTypes] = useState([]);
    // const getAll = async () => {
    //     const categoryTypesRawData = await getPublicData(
    //         "category-types/selection"
    //     );
    //     setDataCategoryTypes(categoryTypesRawData);
    // };

    // - - - - - -
    // Event Category Types
    // - - - - - -
    // const [categoryTypesProp, setCategoryTypesProp] = useState(
    //     props.recordForEdit?.categoryTypes[0]?.id || ""
    //     // props.recordForEdit?.categoryTypes || []
    // );
    // const handleCategoryTypeSelection = (data: any) => {
    //     console.log("handleCategoryTypeSelection");
    //     console.log(data);
    //     console.log(data.target.value);

    //     setCategoryTypesProp(data.target.value);
    //     setValue("categoryTypes", [{ id: data.target.value }]);
    // };

    useEffect(() => {
        (async () => {
            console.log(props);
        })();
    }, [props]);

    return (
        <>
            {showSnackbar && (
                <SnackbarComp
                    type={snackbarType}
                    message={snackbarMessage}
                    // transition="TransitionUp"
                />
            )}

            <Box
                component="form"
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit(onSubmit)}
            >
                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 6,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">Details</Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={6}>
                        <TextField
                            fullWidth
                            label="Name"
                            type="text"
                            {...register("name")}
                        />
                    </Grid>
                    <Grid
                        item
                        xs={12}
                        sm={12}
                        md={6}
                        lg={4}
                        sx={{ display: "flex", justifyContent: "start" }}
                    >
                        <TextField
                            fullWidth
                            label="Featured Image URL"
                            type="text"
                            {...register("featuredImageUrl")}
                        />
                    </Grid>
                    <Grid
                        item
                        xs={12}
                        sm={12}
                        md={6}
                        lg={2}
                        sx={{
                            display: "flex",
                            justifyContent: "start",
                            // mt: -2,
                            mb: 1,
                        }}
                    >
                        <Button
                            variant="contained"
                            component="label"
                            fullWidth
                            style={{ minWidth: "140px" }}
                        >
                            Choose File
                            <input
                                type="file"
                                name="Test Image"
                                hidden
                                {...register("featuredImage")}
                            />
                        </Button>
                    </Grid>

                    {/* <Grid item xs={12} sm={12} md={4}>
                        <FormControl fullWidth>
                            <InputLabel id="type-selection-label">
                                Type
                            </InputLabel>
                            <Select
                                labelId="type-selection-label"
                                onChange={(e) => handleCategoryTypeSelection(e)}
                                value={categoryTypesProp}
                            >
                                {dataCategoryTypes.map((item, i) => (
                                    <MenuItem key={i} value={item.id}>
                                        {item.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid> */}
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 4,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">Description</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            label="Description"
                            multiline
                            rows={4}
                            {...register("description")}
                        />
                    </Grid>

                    <Grid
                        item
                        xs={12}
                        sx={{
                            display: "flex",
                            alignItems: "end",
                            justifyContent: "end",
                        }}
                    >
                        <Button
                            type="submit"
                            size="large"
                            variant="contained"
                            color="primary"
                            disabled={!isDirty && !isValid}
                        >
                            {isSubmitting && (
                                <span className="spinner-border spinner-border-sm mr-1"></span>
                            )}
                            {props.recordForEdit ? (
                                <span>Update</span>
                            ) : (
                                <span>Save</span>
                            )}
                        </Button>
                    </Grid>
                </Grid>
            </Box>
        </>
    );
}
