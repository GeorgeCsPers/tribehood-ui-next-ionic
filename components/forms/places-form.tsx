import {
    postData,
    putData,
    getData,
    getPublicData,
    postDataFile,
    putDataFile,
    deleteData,
} from "../../services/data.service";
import {
    Button,
    Grid,
    Box,
    TextField,
    FormControl,
    InputLabel,
    Autocomplete,
    Select,
    MenuItem,
    OutlinedInput,
    Checkbox,
    ListItemText,
    Typography,
    Divider,
    CardMedia,
} from "@mui/material";
import { useForm, Controller, useFieldArray } from "react-hook-form";
import { useRouter } from "next/router";
import { useRouter as navUseRouter } from "next/navigation";
import { useEffect, useRef, useState } from "react";
import CountrySelection from "../inputs/country";
import fileResizer from "../hooks/file-resizer";
import slugConverter from "../hooks/slug-converter";
import addressFormatter from "../hooks/address-formatter";
// import { useErrorBoundary } from "react-error-boundary";
import SnackbarComp from "../common/snackbars";
import RichContentEditor from "../common/rich-editor";
import ModalConfirm from "../modals/modal-confirm";

export default function PlacesForm(props) {
    const router = useRouter();
    const routerNav = navUseRouter();

    const {
        register,
        handleSubmit,
        control,
        formState,
        setValue,
        formState: { isSubmitting, isDirty, isValid, errors },
    } = useForm({
        defaultValues: props.recordForEdit,
    });
    const { fields, append, prepend, remove, swap, move, insert } =
        useFieldArray({
            control, // control props comes from useForm (optional: if you are using FormContext)
            name: "address", // unique name for your Field Array
        });
    // const inputRef = useRef(null);

    // Form messages
    const formMessages = {
        name: "Name is required",
        areas: "Area is required",
        city: "City/Town/Village is required",
        postcode: "Postcode is required",
        shortDescription: "Short Description is required",
    };

    const apiUrl = "places";

    // Message handlers
    const [showSnackbar, setShowSnackbar] = useState(false);
    const [snackbarType, setSnackbarType] = useState("");
    const [snackbarMessage, setSnackbarMessage] = useState("");
    // const { showBoundary } = useErrorBoundary();

    console.log("PlacesForm props");
    console.log(props);
    console.log("recordForEdit");
    console.log(props.recordForEdit);

    // - - - - - - - -
    // Submit Edit or New
    // - - - - - - - -
    const [isProcessing, setIsProcessing] = useState(false);
    async function onSubmit({
        name,
        categories,
        areas,
        // categoryTypes,
        featuredImage,
        featuredImageUrl,
        websiteUrl,
        otherUrl,
        contactEmail,
        contactMobile,
        contactPhone,
        contactPersonName,
        shortDescription,
        description,
        // placeName,
        address,
    }) {
        // Start processing load
        setIsProcessing(true);

        // Address formatter
        address = address ? addressFormatter(await address) : {};

        // Slug converter
        const slug = await slugConverter(name, "withoutDate");

        // Resize file, Upload and set as featured image
        if (featuredImage) {
            console.log("featuredImage");
            console.log(await featuredImage);
            console.log(await featuredImage.size);
        }

        // Category Type
        const categoryTypes = [{ name: "Place" }];

        const dataBody = {
            name,
            slug,
            categories,
            areas,
            categoryTypes,
            featuredImageUrl,
            websiteUrl,
            otherUrl,
            contactEmail,
            contactMobile,
            contactPhone,
            contactPersonName,
            description,
            shortDescription,
            // placeName,
            address,
        };
        console.log("dataBody");
        console.log(dataBody);

        // - - - - -
        // Edit
        // - - - - -
        if (props.recordForEdit) {
            if (featuredImage) {
                console.log("dataBody Update featuredImage");
                console.log(featuredImage);
                console.log([featuredImage].length);

                const newAsset = await postDataFile("assets", [featuredImage]);
                console.log("fileResponse");
                console.log(newAsset);

                Object.assign(dataBody, {
                    asset: { id: await newAsset.id },
                    featuredImageId: await newAsset.id,
                    featuredImageUrl: await newAsset.url,
                });
            }

            console.log("dataBody Update");
            console.log(dataBody);

            await putData(apiUrl, props.recordForEdit.id, dataBody).then(
                (response) => {
                    console.log("putData response");
                    console.log(response);

                    setSnackbarType("success");
                    setSnackbarMessage(
                        `${response.name} successfully updated!`
                    );
                    setShowSnackbar(true);
                    refreshRouter(response.slug);
                },
                (error) => {
                    console.log("putData error");
                    console.log(error);

                    let message = "";
                    if (props.userDetails.role[0] === "admin") {
                        message = `${error.message} || ${error.response.data.message} (code: ${error.response.data.code}) `;
                    } else {
                        message = error.message;
                    }
                    setSnackbarType("error");
                    setSnackbarMessage(message);
                    setShowSnackbar(true);
                }
            );
        }
        // - - - - -
        // Add New
        // - - - - -
        else {
            if (featuredImage) {
                console.log("dataBody Create featuredImage");
                console.log(featuredImage);
                console.log([featuredImage].length);

                const newAsset = await postDataFile("assets", [featuredImage]);
                console.log("fileResponse");
                console.log(newAsset);

                Object.assign(dataBody, {
                    asset: { id: await newAsset.id },
                    featuredImageId: await newAsset.id,
                    featuredImageUrl: await newAsset.url,
                });
            }

            console.log("dataBody New");
            console.log(dataBody);

            await postData(apiUrl, dataBody).then(
                (response) => {
                    console.log("postData response");
                    console.log(response);

                    setSnackbarType("success");
                    setSnackbarMessage(
                        `${response.name} successfully created!`
                    );
                    setShowSnackbar(true);
                    refreshRouter(response.slug);
                },
                (error) => {
                    console.log("postData error");
                    console.log(error);

                    let message = "";
                    if (props.userDetails.role[0] === "admin") {
                        message = `${error.message} || ${error.response.data.message} (code: ${error.response.data.code}) `;
                    } else {
                        message = error.message;
                    }
                    setSnackbarType("error");
                    setSnackbarMessage(message);
                    setShowSnackbar(true);
                }
            );
        }
    }

    // - - - - - -
    // Handle Delete
    // - - - - - -
    const [showConfirmModal, setShowConfirmModal] = useState(false);
    const deleteItem = async (e: boolean) => {
        if (e) {
            setShowConfirmModal(false);

            if (props.recordForEdit) {
                await deleteData(apiUrl, props.recordForEdit.id).then(
                    (response) => {
                        // Set data, check for errors and refresh router
                        console.log("deleteItem response");
                        console.log(response);

                        setSnackbarType("success");
                        setSnackbarMessage(
                            `${response.name} successfully deleted!`
                        );
                        setShowSnackbar(true);
                        refreshRouter();
                    },
                    (error) => {
                        console.log("deleteItem error");
                        console.log(error);

                        let message = "";
                        if (props.userDetails.role[0] === "admin") {
                            message = `${error.message} || ${error.response.data.message} (code: ${error.response.data.code}) `;
                        } else {
                            message = error.message;
                        }
                        setSnackbarType("error");
                        setSnackbarMessage(message);
                        setShowSnackbar(true);
                    }
                );
            }
        } else {
            setShowConfirmModal(false);
        }
    };

    // - - - - - - - -
    // Refresh router
    // - - - - - - - -
    const refreshRouter = (slug?: string) => {
        let pushUrl: string;
        if (slug) {
            pushUrl = `/${router.query.area}/places/${slug}`;
        } else {
            pushUrl = `/${router.query.area}/places/`;
        }
        router.push(pushUrl).then((resp) => {
            routerNav.refresh();
            setIsProcessing(false);
        });
    };

    // - - - - - - - -
    // Handle file selection and preview
    // - - - - - - - -
    const [previewFile, setPreviewFile] = useState("");
    const handleFileSelection = async (e) => {
        console.log("handleFileSelection");
        console.log(e);
        console.log(e.target.value);
        console.log(e.target.files);

        let resizedImage: any;
        if ((await e.target.files.length) > 0) {
            // Set image preview and prepare URL to resize image
            const [previewfile] = e.target.files;
            const fileURL = URL.createObjectURL(previewfile);
            setPreviewFile(fileURL);

            // Resize file, Upload and set as featured image
            console.log("e.target.files");
            console.log(e.target.files);
            console.log(e.target.files[0]);

            const file = await e.target.files[0];
            // v1 with react file resizer package
            resizedImage = await fileResizer(file, "image");

            // v2 raw
            // await imageResizer(fileURL, 2000, file.name).then(async (resp) => {
            //     resizedImage = await resp;
            // });

            console.log("resizedImage");
            console.log(await resizedImage);
            console.log(await resizedImage.size);
        }

        setSnackbarType("info");
        setSnackbarMessage(
            `resizedImage: 
            ${((await resizedImage.size) / Math.pow(10, 3)).toFixed(2)} kb 
            (${((await resizedImage.size) / Math.pow(10, 6)).toFixed(2)} mb)`
        );
        setShowSnackbar(true);

        setValue("featuredImage", await resizedImage);
    };

    // - - - - - - -
    // Get All selections data
    // - - - - - - -
    // const [dataCategoryTypes, setDataCategoryTypes] = useState([]);
    const [dataCategories, setDataCategories] = useState([]);
    const [dataPlaceAreas, setDataPlaceAreas] = useState([]);
    const getAll = async () => {
        // const categoryTypesRawData = await getPublicData(
        //     "category-types/selection"
        // );
        // setDataCategoryTypes(categoryTypesRawData);

        const categoriesRawData = await getPublicData("categories/selection");
        setDataCategories(categoriesRawData);

        const areasRawData = await getPublicData("location/areas/selection");
        setDataPlaceAreas(areasRawData);
    };

    // - - - - - -
    // Category Types
    // - - - - - -
    // const [categoryTypesProp, setCategoryTypesProp] = useState(
    //     props.recordForEdit?.categoryTypes[0]?.id || ""
    // );

    // const handleCategoryTypeSelection = (data: any) => {
    //     console.log("handleCategoryTypeSelection");
    //     console.log(data);
    //     console.log(data.target.value);

    //     setCategoryTypesProp(data.target.value);
    //     setValue("categoryTypes", [{ id: data.target.value }]);
    // };

    // - - - - - -
    // Categories
    // - - - - - -
    const [categoriesProp, setCategoriesProp] = useState(
        props.recordForEdit?.categories || []
    );
    const handleCategoriesSelection = (data: any) => {
        console.log("handleCategoriesSelection");
        console.log(data);

        setValue("categories", selectionNameRemoval(data));
    };

    // - - - - - -
    // Areas
    // - - - - - -
    const [areasProp, setPlaceProp] = useState(
        props.recordForEdit?.areas || []
        // props.recordForEdit?.areas || dataPlaceAreas[2]
    );
    const handlePlaceAreasSelection = (data: any) => {
        console.log("handlePlaceAreasSelection");
        console.log(data);

        setValue("areas", selectionNameRemoval(data));
    };

    const selectionNameRemoval = (data: any) => {
        let newObj = [];
        data.forEach((item) => {
            const newObjTemp = Object.keys(item)
                .filter((key) => key != "name")
                .filter((key) => key != "slug")
                .reduce((acc, key) => {
                    acc[key] = item[key];
                    return acc;
                }, {});
            newObj.push(newObjTemp);
        });

        console.log(newObj);
        return newObj;
    };

    // - - - - - - -
    // Country selection
    // - - - - - - -
    const [selectedCountry, setSelectedCountry] = useState(
        props.recordForEdit?.address[0]?.country || []
    );
    const handleSelectedCountry = (data: any) => {
        console.log("handleSelectedCountry data");
        console.log(data);
        setSelectedCountry([{ id: data?.id }]);
        setValue("address[0].country[0].id", data?.id);
    };

    // - - - - - -
    // Description
    // - - - - - -
    const handleDescription = (data: any) => {
        console.log("handleDescription");
        console.log(data);

        setValue("description", data);
    };

    useEffect(() => {
        (async () => {
            getAll();
        })();

        if (props.recordForEdit) {
            // setValue("categoryTypes", [
            //     { id: props.recordForEdit?.categoryTypes[0]?.id },
            // ]);
            setValue(
                "categories",
                selectionNameRemoval(props.recordForEdit?.categories)
            );
            setValue("areas", selectionNameRemoval(props.recordForEdit?.areas));
        }
    }, [props]);

    // console.log("categoryTypesProp");
    // console.log(categoryTypesProp);
    console.log("categoriesProp");
    console.log(categoriesProp);

    return (
        <>
            {showSnackbar && (
                <SnackbarComp
                    type={snackbarType}
                    message={snackbarMessage}
                    // transition="TransitionUp"
                />
            )}

            {showConfirmModal && (
                <ModalConfirm
                    title="Are you sure to Delete"
                    showConfirmModal={showConfirmModal}
                    setShowConfirmModal={setShowConfirmModal}
                    recordForEdit={props.recordForEdit}
                    deleteItem={(e: boolean) => deleteItem(e)}
                />
            )}

            <Box
                component="form"
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit(onSubmit)}
            >
                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 6,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5"> Details</Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={6}>
                        <TextField
                            fullWidth
                            label="Name"
                            type="text"
                            {...register("name", { required: true })}
                            color={errors.name ? "error" : "primary"}
                            required
                        />
                        {errors.name && (
                            <Typography variant="body2" color="error">
                                {formMessages.name}
                            </Typography>
                        )}
                    </Grid>
                    <Grid
                        item
                        xs={12}
                        sm={12}
                        md={6}
                        lg={4}
                        sx={{ display: "flex", justifyContent: "start" }}
                    >
                        <TextField
                            fullWidth
                            label="Featured Image URL"
                            type="text"
                            {...register("featuredImageUrl")}
                        />
                    </Grid>
                    <Grid
                        item
                        xs={12}
                        sm={12}
                        md={6}
                        lg={2}
                        sx={{
                            display: "flex",
                            justifyContent: "start",
                            // mt: -2,
                            mb: 1,
                        }}
                    >
                        {previewFile && (
                            <CardMedia
                                component="img"
                                image={previewFile}
                                sx={{
                                    maxWidth: "56px",
                                    maxHeight: "56px",
                                    mr: 1,
                                    mb: -2,
                                }}
                            />
                        )}
                        <Button
                            variant="contained"
                            component="label"
                            fullWidth
                            style={{ minWidth: "140px" }}
                        >
                            Choose File
                            <input
                                type="file"
                                name="Featured Image"
                                // accept="image/*"
                                hidden
                                onChange={handleFileSelection}
                            />
                        </Button>
                    </Grid>

                    {/* <Grid item xs={12} sm={12} md={4}>
                        <FormControl fullWidth>
                            <InputLabel id="type-selection-label">
                                Type
                            </InputLabel>
                            <Select
                                labelId="type-selection-label"
                                onChange={(e) => handleCategoryTypeSelection(e)}
                                value={categoryTypesProp}
                            >
                                {dataCategoryTypes.map((item, i) => (
                                    <MenuItem key={i} value={item.id}>
                                        {item.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid> */}

                    <Grid item xs={12} sm={12} md={4}>
                        <Controller
                            control={control}
                            name="Categories"
                            defaultValue={categoriesProp}
                            render={({
                                field: { ref, onChange, ...field },
                            }) => (
                                <Autocomplete
                                    multiple
                                    options={dataCategories}
                                    defaultValue={categoriesProp}
                                    getOptionLabel={(option) => option.name}
                                    onChange={(_, data) => {
                                        onChange(data);
                                        handleCategoriesSelection(data);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...field}
                                            {...params}
                                            fullWidth
                                            label="Categories"
                                        />
                                    )}
                                />
                            )}
                        />
                    </Grid>

                    <Grid item xs={12} sm={12} md={4}>
                        <Controller
                            control={control}
                            name="Place Areas"
                            defaultValue={areasProp}
                            render={({
                                field: { ref, onChange, ...field },
                            }) => (
                                <Autocomplete
                                    multiple
                                    options={dataPlaceAreas}
                                    defaultValue={areasProp}
                                    getOptionLabel={(option) => option.name}
                                    onChange={(_, data) => {
                                        onChange(data);
                                        handlePlaceAreasSelection(data);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...field}
                                            {...params}
                                            fullWidth
                                            label="Place Areas"
                                        />
                                    )}
                                />
                            )}
                        />
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 6,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5"> Place</Typography>
                    </Grid>
                    {/* <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Place Name"
                            type="text"
                            {...register("placeName")}
                        />
                    </Grid> */}
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Address Line 1"
                            type="text"
                            {...register("address[0].addressLine1")}
                        />
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Address Line 2"
                            type="text"
                            {...register("address[0].addressLine2")}
                        />
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Address Line 3"
                            type="text"
                            {...register("address[0].addressLine3")}
                        />
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="City/Town/Village"
                            type="text"
                            {...register("address[0].city", { required: true })}
                            color={errors.address ? "error" : "primary"}
                            required
                        />
                        {errors.address && (
                            <Typography variant="body2" color="error">
                                {formMessages.city}
                            </Typography>
                        )}
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Postcode"
                            type="text"
                            {...register("address[0].postcode", {
                                required: true,
                            })}
                            color={errors.address ? "error" : "primary"}
                            required
                        />
                        {errors.address && (
                            <Typography variant="body2" color="error">
                                {formMessages.postcode}
                            </Typography>
                        )}
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="County"
                            type="text"
                            {...register("address[0].county")}
                        />
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Region"
                            type="text"
                            {...register("address[0].region")}
                        />
                    </Grid>

                    <Grid item xs={12} sm={6} md={3}>
                        <CountrySelection
                            handleSelectedCountry={(e) =>
                                handleSelectedCountry(e)
                            }
                            selectedCountry={selectedCountry}
                        />
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 6,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">Contact Details</Typography>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Contact Email"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("contactEmail")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Contact Mobile"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("contactMobile")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Contact Phone"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("contactPhone")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            fullWidth
                            label="Contact Person Name"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("contactPersonName")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={6}>
                        <TextField
                            fullWidth
                            label="URL 1"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("url1")}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={6}>
                        <TextField
                            fullWidth
                            label="URL 2"
                            type="text"
                            sx={{ sm: 12, md: 6 }}
                            {...register("url2")}
                        />
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 4,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">Short Description</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            label="Short Description"
                            multiline
                            rows={4}
                            {...register("shortDescription", {
                                required: true,
                            })}
                            color={
                                errors.shortDescription ? "error" : "primary"
                            }
                            required
                        />
                        {errors.shortDescription && (
                            <Typography variant="body2" color="error">
                                {formMessages.shortDescription}
                            </Typography>
                        )}
                    </Grid>

                    <Grid item xs={12}>
                        <Typography variant="h5">Description</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <RichContentEditor
                            content={props.recordForEdit?.description}
                            setNewContent={(e: any) => handleDescription(e)}
                        />
                        {/* <TextField
                            fullWidth
                            label="Description"
                            multiline
                            rows={4}
                            {...register("description")}
                        /> */}
                    </Grid>

                    <Grid
                        item
                        xs={6}
                        sx={{
                            display: "flex",
                            alignItems: "end",
                            justifyContent: "start",
                        }}
                    >
                        {props.userDetails.role[0] === "admin" &&
                            props.recordForEdit && (
                                <Button
                                    size="large"
                                    variant="outlined"
                                    color="error"
                                    onClick={(e) => setShowConfirmModal(true)}
                                >
                                    Delete
                                </Button>
                            )}
                    </Grid>

                    <Grid
                        item
                        xs={6}
                        sx={{
                            display: "flex",
                            alignItems: "end",
                            justifyContent: "end",
                        }}
                    >
                        <Button
                            type="submit"
                            size="large"
                            variant="contained"
                            color="primary"
                            // disabled={formState.isSubmitting}
                            disabled={!isDirty && !isValid}
                        >
                            {isProcessing &&
                            showSnackbar &&
                            snackbarType !== "error" ? (
                                <Box
                                    sx={{
                                        display: "flex",
                                        width: "80px",
                                        justifyContent: "center",
                                    }}
                                >
                                    <img
                                        width={64}
                                        height={32}
                                        src="/assets/icons/loading-dots-muted.svg"
                                    />
                                </Box>
                            ) : props.recordForEdit ? (
                                <Box
                                    sx={{
                                        display: "flex",
                                        width: "80px",
                                        justifyContent: "center",
                                    }}
                                >
                                    Update
                                </Box>
                            ) : (
                                <Box
                                    sx={{
                                        display: "flex",
                                        width: "80px",
                                        justifyContent: "center",
                                    }}
                                >
                                    Save
                                </Box>
                            )}
                        </Button>
                    </Grid>
                </Grid>
            </Box>
        </>
    );
}
