import {
    postData,
    putData,
    getData,
    getPublicData,
    postDataFile,
    putDataFile,
} from "../../services/data.service";
import {
    Button,
    Grid,
    Box,
    TextField,
    FormControl,
    InputLabel,
    Autocomplete,
    Select,
    MenuItem,
    OutlinedInput,
    SelectChangeEvent,
    Checkbox,
    ListItemText,
    Typography,
    Divider,
} from "@mui/material";
// https://mui.com/x/react-date-pickers/
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
import { useForm, Controller, useFieldArray } from "react-hook-form";
import { useRouter } from "next/navigation";
import { useEffect, useRef, useState } from "react";
import CountrySelection from "../inputs/country";
// import { useErrorBoundary } from "react-error-boundary";
import SnackbarComp from "../common/snackbars";
import fileResizer from "../hooks/file-resizer";
import slugConverter from "../hooks/slug-converter";

export default function CategoriesForm(props) {
    const router = useRouter();
    const {
        register,
        handleSubmit,
        control,
        formState,
        setValue,
        formState: { isSubmitting, isDirty, isValid, errors },
    } = useForm({
        defaultValues: props.recordForEdit,
    });

    // Form messages
    const formMessages = {
        name: "Name is required",
        categoryType: "Category type is required",
    };

    const apiUrl = "categories";

    // Message handlers
    const [showSnackbar, setShowSnackbar] = useState(false);
    const [snackbarType, setSnackbarType] = useState("");
    const [snackbarMessage, setSnackbarMessage] = useState("");

    console.log("CategoriesForm props");
    console.log(props);
    console.log("recordForEdit");
    console.log(props.recordForEdit);

    // - - - - - - - -
    // Submit Edit or New Events
    // - - - - - - - -
    async function onSubmit({
        name,
        categoryTypes,
        featuredImage,
        featuredImageUrl,
        shortDescription,
        description,
    }) {
        // Slug converter
        const slug = await slugConverter(name, "withoutDate");

        // Resize file, Upload and set as featured image
        if (featuredImage) {
            console.log("featuredImage");
            console.log(await featuredImage);
            console.log(await featuredImage.size);

            // setSnackbarType("info");
            // setSnackbarMessage(
            //     `featuredImage: ${(await featuredImage.size) / 1000} kb`
            // );
            // setShowSnackbar(true);
        }

        const dataBody = {
            // userId,
            name,
            slug,
            categoryTypes,
            featuredImageUrl,
            description,
            shortDescription,
        };
        console.log("dataBody");
        console.log(dataBody);

        // - - - - -
        // Edit
        // - - - - -
        if (props.recordForEdit) {
            if (featuredImage) {
                console.log("dataBody Update featuredImage");
                console.log(featuredImage);
                console.log([featuredImage].length);

                const newAsset = await postDataFile("assets", [
                    await featuredImage,
                ]);
                console.log("fileResponse");
                console.log(newAsset);

                Object.assign(dataBody, {
                    asset: { id: await newAsset.id },
                    featuredImageId: await newAsset.id,
                    featuredImageUrl: await newAsset.url,
                });
            }

            console.log("dataBody Update");
            console.log(dataBody);

            await putData(apiUrl, props.recordForEdit.id, dataBody).then(
                (response) => {
                    console.log("putData response");
                    console.log(response);

                    setSnackbarType("success");
                    setSnackbarMessage(
                        `${response.name} successfully updated!`
                    );
                    setShowSnackbar(true);
                    refreshRouter();
                },
                (error) => {
                    console.log("putData error");
                    console.log(error);

                    let message = "";
                    if (props.userDetails.role[0] === "admin") {
                        message = `${error.message} || ${error.response.data.message} (code: ${error.response.data.code}) `;
                    } else {
                        message = error.message;
                    }
                    setSnackbarType("error");
                    setSnackbarMessage(message);
                    setShowSnackbar(true);
                }
            );
        }
        // - - - - -
        // Add New
        // - - - - -
        else {
            if (featuredImage) {
                console.log("dataBody Create featuredImage");
                console.log(featuredImage);
                console.log([featuredImage].length);

                const newAsset = await postDataFile("assets", [
                    await featuredImage,
                ]);
                console.log("fileResponse");
                console.log(newAsset);

                Object.assign(dataBody, {
                    asset: { id: await newAsset.id },
                    featuredImageId: await newAsset.id,
                    featuredImageUrl: await newAsset.url,
                });
            }

            console.log("dataBody New");
            console.log(dataBody);

            await postData(apiUrl, dataBody).then(
                (response) => {
                    console.log("postData response");
                    console.log(response);

                    setSnackbarType("success");
                    setSnackbarMessage(
                        `${response.name} successfully created!`
                    );
                    setShowSnackbar(true);
                    refreshRouter();
                },
                (error) => {
                    console.log("postData error");
                    console.log(error);

                    let message = "";
                    if (props.userDetails.role[0] === "admin") {
                        message = `${error.message} || ${error.response.data.message} (code: ${error.response.data.code}) `;
                    } else {
                        message = error.message;
                    }
                    setSnackbarType("error");
                    setSnackbarMessage(message);
                    setShowSnackbar(true);
                }
            );
        }
    }

    // - - - - - - - -
    // Refresh router
    // - - - - - - - -
    const refreshRouter = () => {
        setTimeout(() => {
            // setIsProcessing(false);
            router.refresh();
        }, 2000);
    };

    // - - - - - - -
    // Get All selections data
    // - - - - - - -
    const [dataCategoryTypes, setDataCategoryTypes] = useState([]);
    const getAll = async () => {
        const categoryTypesRawData = await getPublicData(
            "category-types/selection"
        );
        setDataCategoryTypes(categoryTypesRawData);
    };

    // - - - - - -
    // Category Types
    // - - - - - -
    const [categoryTypesProp, setCategoryTypesProp] = useState(
        // props.recordForEdit?.categoryTypes[0]?.id || "" // used for single selection
        props.recordForEdit?.categoryTypes || []
    );
    const handleCategoryTypeSelection = (data: any) => {
        console.log("handleCategoryTypeSelection");
        console.log(data);

        // Single selection
        // console.log(data.target.value);
        // setCategoryTypesProp(data.target.value);
        // setValue("categoryTypes", [{ id: data.target.value }]);

        setValue("categoryTypes", selectionNameRemoval(data));
    };

    const selectionNameRemoval = (data: any) => {
        let newObj = [];
        data.forEach((item) => {
            const newObjTemp = Object.keys(item)
                .filter((key) => key != "name")
                .filter((key) => key != "slug")
                .reduce((acc, key) => {
                    acc[key] = item[key];
                    return acc;
                }, {});
            newObj.push(newObjTemp);
        });

        console.log(newObj);
        return newObj;
    };

    useEffect(() => {
        (async () => {
            getAll();
        })();
        if (props.recordForEdit) {
            setValue("categoryTypes", [
                { id: props.recordForEdit?.categoryTypes[0]?.id },
            ]);
        }
    }, [props]);

    console.log("categoryTypesProp");
    console.log(categoryTypesProp);

    // - - - - - - - -
    // Handle file selection and preview
    // - - - - - - - -
    const [previewFile, setPreviewFile] = useState("");
    const handleFileSelection = async (e) => {
        console.log("handleFileSelection");
        console.log(e);
        console.log(e.target.value);
        console.log(e.target.files);

        setShowSnackbar(false);
        // setSnackbarType("info");
        // setSnackbarMessage(
        //     `e.target: ${await e.target.value} and ${JSON.stringify(
        //         e.target.files
        //     )}`
        // );
        // setShowSnackbar(true);

        let resizedImage: any;
        if (await e.target.value) {
            // Set image preview and prepare URL to resize image
            const [previewfile] = await e.target.files;
            const fileURL = URL.createObjectURL(previewfile);
            setPreviewFile(fileURL);
            console.log("fileURL");
            console.log(fileURL);

            // Resize file, Upload and set as featured image
            console.log("e.target.files");
            console.log(e.target.files);
            console.log(e.target.files[0]);

            const file = await e.target.files[0];
            console.log("file");
            console.log(file);
            console.log(file.name);

            // v1 with react file resizer package
            resizedImage = await fileResizer(file, "image");

            // v2 raw
            // await imageResizer(fileURL, 2000, file.name).then(async (resp) => {
            //     resizedImage = await resp;
            // });

            console.log("resizedImage");
            console.log(await resizedImage);
            console.log(await resizedImage.size);

            setSnackbarType("info");
            setSnackbarMessage(
                `resizedImage:
                ${await resizedImage.name}, 
                    ${((await resizedImage.size) / Math.pow(10, 3)).toFixed(
                        2
                    )} kb
                    (${((await resizedImage.size) / Math.pow(10, 6)).toFixed(
                        2
                    )} mb) 
                   
                    || Original: ${(
                        (await file.size) / Math.pow(10, 3)
                    ).toFixed(2)} kb
                    (${((await file.size) / Math.pow(10, 6)).toFixed(2)} mb)`
            );
            setShowSnackbar(true);

            setValue("featuredImage", await resizedImage);
        }
    };

    return (
        <>
            {showSnackbar && (
                <SnackbarComp
                    type={snackbarType}
                    message={snackbarMessage}
                    // transition="TransitionUp"
                />
            )}

            <Box
                component="form"
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit(onSubmit)}
            >
                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 6,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">Details</Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={6}>
                        <TextField
                            fullWidth
                            label="Name"
                            type="text"
                            {...register("name")}
                        />
                    </Grid>

                    <Grid
                        item
                        xs={12}
                        sm={12}
                        md={6}
                        lg={4}
                        sx={{ display: "flex", justifyContent: "start" }}
                    >
                        <TextField
                            fullWidth
                            label="Featured Image URL"
                            type="text"
                            {...register("featuredImageUrl")}
                        />
                    </Grid>

                    <Grid
                        item
                        xs={12}
                        sm={12}
                        md={6}
                        lg={2}
                        sx={{
                            display: "flex",
                            justifyContent: "start",
                            // mt: -2,
                            mb: 1,
                        }}
                    >
                        <Button
                            variant="contained"
                            component="label"
                            fullWidth
                            style={{ minWidth: "140px" }}
                        >
                            Choose File
                            <input
                                type="file"
                                name="Featured Image"
                                hidden
                                onChange={handleFileSelection}
                                // {...register("featuredImage")}
                            />
                        </Button>
                    </Grid>

                    <Grid item xs={12} sm={12} md={4}>
                        {/* <FormControl fullWidth>
                            <InputLabel id="type-selection-label">
                                Type
                            </InputLabel>
                            <Select
                                labelId="type-selection-label"
                                onChange={(e) => handleCategoryTypeSelection(e)}
                                value={categoryTypesProp}
                            >
                                {dataCategoryTypes.map((item, i) => (
                                    <MenuItem key={i} value={item.id}>
                                        {item.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl> */}

                        <Controller
                            control={control}
                            name="Categories"
                            defaultValue={categoryTypesProp}
                            render={({
                                field: { ref, onChange, ...field },
                            }) => (
                                <Autocomplete
                                    multiple
                                    options={dataCategoryTypes}
                                    defaultValue={categoryTypesProp}
                                    getOptionLabel={(option) => option.name}
                                    onChange={(_, data) => {
                                        onChange(data);
                                        handleCategoryTypeSelection(data);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...field}
                                            {...params}
                                            fullWidth
                                            // inputRef={ref}
                                            // variant="filled"
                                            label="Category Types"
                                            // inputProps={register("categories", {
                                            //     required: "Please choose",
                                            // })}
                                        />
                                    )}
                                />
                            )}
                            // {...register("categories")}
                        />
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        width: "100%",
                        display: "flex",
                        mb: 4,
                    }}
                >
                    <Grid item xs={12}>
                        <Typography variant="h5">Description</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            label="Description"
                            multiline
                            rows={4}
                            {...register("description")}
                        />
                    </Grid>

                    <Grid
                        item
                        xs={12}
                        sx={{
                            display: "flex",
                            alignItems: "end",
                            justifyContent: "end",
                        }}
                    >
                        <Button
                            type="submit"
                            size="large"
                            variant="contained"
                            color="primary"
                            disabled={!isDirty && !isValid}
                        >
                            {isSubmitting && (
                                <span className="spinner-border spinner-border-sm mr-1"></span>
                            )}
                            {props.recordForEdit ? (
                                <span>Update</span>
                            ) : (
                                <span>Save</span>
                            )}
                        </Button>
                    </Grid>
                </Grid>
            </Box>
        </>
    );
}
