import { useRouter } from "next/router";
import { useRouter as navUseRouter } from "next/navigation";

export default async function refreshRouterComponent(
    slug: string,
    pagePath: string
) {
    const router = useRouter();
    const routerNav = navUseRouter();

    let pushUrl: string;
    if (slug) {
        pushUrl = `/${router.query.area}/${pagePath}/${slug}`;
    } else {
        pushUrl = `/${router.query.area}/${pagePath}/`;
    }
    await router.push(pushUrl).then((resp) => {
        routerNav.refresh();
    });
}
