export function camelCaseFormatter(str: any) {
    // const updatedText = str
    //     .replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
    //         return index === 0 ? word.toLowerCase() : word.toUpperCase();
    //     })
    //     .replace(/\s+/g, "");

    const updatedText = str.replace(
        /(?:^\w|[A-Z]|\b\w|\s+)/g,
        function (match, index) {
            if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
            return index === 0 ? match.toLowerCase() : match.toUpperCase();
        }
    );

    console.log("camelCaseFormatter");
    console.log(updatedText);
    return updatedText;
}

export function pascalCaseFormatter(str: any) {
    const updatedText = (" " + str)
        .toLowerCase()
        .replace(/[^a-zA-Z0-9]+(.)/g, function (match, chr) {
            return chr.toUpperCase();
        });

    console.log("pascalCaseFormatter");
    console.log(updatedText);
    return updatedText;
}
