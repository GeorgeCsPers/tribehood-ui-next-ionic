export default function addressFormatter(address: any) {
    console.log("addressFormatter address");
    console.log(address);

    let countryValue = [];
    if (address[0].country?.length > 0) {
        countryValue = [{ id: address[0].country[0].id }];
        // countryValue = { country: [{ id: address[0].country[0]?.id }] };
    }
    console.log("countryValue");
    console.log(countryValue);

    const newObj = [
        {
            addressLine1: address[0].addressLine1,
            addressLine2: address[0].addressLine2,
            addressLine3: address[0].addressLine3,
            city: address[0].city,
            postcode: address[0].postcode,
            county: address[0].county,
            region: address[0].region,
            // countryValue,
            // country: [{ id: address[0].country[0].id }] || [],
            country: countryValue,
        },
    ];
    return newObj;
}
