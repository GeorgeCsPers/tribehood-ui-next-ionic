export default function slugConverter(
    inputText: any,
    type: string,
    existingDate?: Date
) {
    if (type === "withDate") {
        let slugDate: string;
        if (existingDate !== null) {
            slugDate = new Date(existingDate).toISOString().split("T")[0];
        } else {
            slugDate = new Date().toISOString().split("T")[0];
        }
        // const textFormat = inputText.replace(/^a-zA-Z0-9 ]/g, "");
        const noSpecialChar = inputText.replace(/[^\w\s]/gi, "");
        console.log("noSpecialChar");
        console.log(noSpecialChar);
        const slug =
            noSpecialChar.replace(/ /g, "-").toLowerCase() + "-" + slugDate;
        console.log("slug");
        console.log(slug);
        return slug;
    }
    if (type === "withoutDate") {
        const noSpecialChar = inputText.replace(/[^\w\s]/gi, "");
        return noSpecialChar.replace(/ /g, "-").toLowerCase();
        // const slug =
        //     (await name.replace(/ /g, "-").toLowerCase()) +
        //     "-" +
        //     new Date().toISOString().split("T")[0];
        // const slug = await name.replaceAll("[^a-zA-Z0-9]+", "").toLowerCase();
        // const slug = name.replace(/-|\s/g, "").replace(/ /g, "-").toLowerCase();
        // const slug = name.replace(/-|\s/g, "").replace(/ /g, "-").toLowerCase();
    }
}
