export default function categoryTypeFormatter(inputText: any, type: string) {
    if (type === "slug") {
        // // const textFormat = inputText.replace(/^a-zA-Z0-9 ]/g, "");
        // const noSpecialChar = inputText.replace(/[^\w\s]/gi, "");
        // console.log("noSpecialChar");
        // console.log(noSpecialChar);

        // const slug = noSpecialChar.replace(/ /g, "-").toLowerCase();
        // console.log("slug");
        // console.log(slug);
        let slug: string;
        if (inputText === "Event") {
            slug = "events";
        }
        if (inputText === "Activity") {
            slug = "activities";
        }
        if (inputText === "Public Notice") {
            slug = "public-notices";
        }
        if (inputText === "Place") {
            slug = "places";
        }
        if (inputText === "Service") {
            slug = "services";
        }
        if (inputText === "Place to Visit") {
            slug = "places-to-visit";
        }

        return slug;
    }
}
