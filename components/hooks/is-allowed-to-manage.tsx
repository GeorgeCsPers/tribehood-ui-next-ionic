export default function isAllowedToManageChecker(userDetails: any, data: any) {
    // console.log("userDetails");
    // console.log(userDetails);
    // console.log("data");
    // console.log(data);
    if (userDetails) {
        if (
            data.createdBy.id === userDetails.id ||
            userDetails.role[0] === "sys_admin" ||
            userDetails.role[0] === "admin"
        ) {
            return true;
        }
    }
}
