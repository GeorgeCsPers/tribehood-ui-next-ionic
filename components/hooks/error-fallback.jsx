// https://www.youtube.com/watch?v=1_dLaSjzOMY
// https://www.npmjs.com/package/react-error-boundary
// https://github.com/gitdagray/react-suspense/blob/main/suspense-complete/src/App.jsx
import SnackbarComp from "../common/snackbars";

const ErrorFallback = ({ error, resetErrorBoundary }) => {
    console.log("ErrorFallback");
    console.log(error);
    // console.log(resetErrorBoundary);

    return (
        <SnackbarComp
            type="error"
            message={error?.message ? error.message : error}
            // transition="TransitionUp"
        />
    );
};
export default ErrorFallback;
