// import { useEffect, useState } from "react";
import Resizer from "react-image-file-resizer";

// v1
// https://www.npmjs.com/package/react-image-file-resizer
export default async function fileResizer(fileData: any, type: any) {
    // console.log("fileResizer fileData");
    // console.log(fileData);
    // console.log("window.innerWidth");
    // console.log(window.innerWidth);

    if (type === "image") {
        // Fix for iOS
        let fileType: string;
        if (window.innerWidth < 1200) {
            fileType = "jpeg";
        } else {
            fileType = "webp";
        }

        const resizeImageFile = async (file: any) =>
            new Promise(async (resolve) => {
                Resizer.imageFileResizer(
                    await file,
                    2000,
                    2000,
                    fileType,
                    80,
                    0,
                    (uri) => {
                        resolve(uri);
                    },
                    "file"
                );
            });

        console.log("fileData");
        console.log(await fileData);
        console.log(await resizeImageFile(await fileData));

        return await resizeImageFile(await fileData);
    }
}

// v2
// export async function imageResizer(
//     imageURL: any,
//     maxSize: number,
//     fileName?: string
// ) {
//     console.log("resizeImage imageURL");
//     console.log(imageURL);

//     return new Promise((resolve) => {
//         const image = new Image();

//         image.onload = function async() {
//             const canvas = document.createElement("canvas");

//             let width = image.width;
//             let height = image.height;
//             if (width > height) {
//                 if (width > maxSize) {
//                     height *= maxSize / width;
//                     width = maxSize;
//                 }
//             } else {
//                 if (height > maxSize) {
//                     width *= maxSize / height;
//                     height = maxSize;
//                 }
//             }
//             canvas.width = width;
//             canvas.height = height;
//             canvas.getContext("2d").drawImage(image, 0, 0, width, height);
//             let dataUrl = canvas.toDataURL("image/webp", 0.8);
//             // return dataURItoBlob(dataUrl);
//             // console.log("dataUrl");
//             // console.log(dataUrl);

//             const file = dataURLtoFile(dataUrl, fileName);

//             // const base64Data = dataUrl.replace(
//             //     /^data:image\/[a-z]+;base64,/,
//             //     ""
//             // );
//             // const base64Data = dataUrl.replace("data:", "").replace(/^.+,/, "");
//             // console.log("base64Data");
//             // console.log(base64Data);
//             // const file = window.atob(base64Data);
//             // const file = btoa(pureDataURL);

//             console.log("file");
//             console.log(file);

//             resolve(file);
//         };

//         image.src = imageURL;

//         console.log("image");
//         console.log(image);
//         console.log("image.src");
//         console.log(image.src);
//     });
// }

// function dataURLtoFile(dataUrl: string, fileName: string) {
//     var arr = dataUrl.split(","),
//         mime = arr[0].match(/:(.*?);/)[1],
//         bstr = atob(arr[arr.length - 1]),
//         n = bstr.length,
//         u8arr = new Uint8Array(n);
//     while (n--) {
//         u8arr[n] = bstr.charCodeAt(n);
//     }
//     // remove . file extensions
//     // fileName.replace(/\.[^/.]+$/, ".webp")
//     // fileName.substring(0, fileName.lastIndexOf("."))
//     return new File([u8arr], fileName.replace(/\.[^/.]+$/, ".webp"), {
//         type: mime,
//     });
// }

// Just test
// const blobConverter = async (dataUrl: any) => {
//     const base64Response = await fetch(`${dataUrl}`);
//     const blob = await base64Response.blob();

//     console.log("blob");
//     console.log(blob);
// };

// v0 Init test
// export default function resizeFiles(fileData: any, type: string) {
//     console.log("resizeFiles");
//     const [fileValue, setFileValue] = useState<any>();

//     const resizeImageFile = (file: any) =>
//         new Promise(async (resolve) => {
//             Resizer.imageFileResizer(
//                 await file,
//                 2000,
//                 2000,
//                 "webp",
//                 80,
//                 0,
//                 (uri) => {
//                     resolve(uri);
//                 },
//                 "file"
//             );
//         });
//     // }).then((item) => {
//     //     setFileValue(item);
//     // });

//     useEffect(() => {
//         (async () => {
//             console.log(fileData);
//             // Image resizer
//             if (type === "image") {
//                 console.log("useEffect resizeFiles");
//                 const file = await resizeImageFile(fileData);
//                 setFileValue(file);
//             }
//         })();
//     }, [fileData, type]);

//     return fileValue;
// }
