import React from "react";

export default class ErrorBoundary extends React.Component {
    state = { hasError: false };

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    componentDidCatch(error, info) {
        console.log(error, info);
    }

    render() {
        if (this.state.hasError) {
            // return this.props.fallback;
            return (
                <Snackbar
                    open={open}
                    autoHideDuration={6000}
                    onClose={handleClose}
                    message="Note archived"
                    action={action}
                >
                    <Alert
                        onClose={handleClose}
                        severity="error" // error, warning, info, success
                        sx={{ width: "100%" }}
                    >
                        {this.props.fallback}
                    </Alert>
                </Snackbar>
            );
        }
        return this.props.children;
    }
}

// export default ErrorBoundary;
