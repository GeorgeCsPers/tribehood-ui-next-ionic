import { EventJsonLd, NextSeo } from "next-seo";
import Head from "next/head";

// Assets URL
const serverUrl = process.env.NEXT_PUBLIC_ASSETS_URL;

export default function HeadShared(props: any) {
    // - - - - - - - - -
    //   Current URL
    // - - - - - - - - -
    // const currentUrl = window.location.href;
    // console.log("window.location.href");
    // console.log(window.location.href);

    let currentUrl = "";
    if (typeof window !== "undefined") {
        console.log("window.location.href");
        console.log(window.location.href);
        currentUrl = window.location.href;
    }

    // - - - - - - - - -
    // Image URL adjustment
    // - - - - - - - - -
    let imageUrl = "/assets/images/TribeHood-better-connected-community.webp";
    if (props.image && props.image.indexOf("http") > -1) {
        imageUrl = props.image;
    } else if (props.image) {
        imageUrl = serverUrl + props.image;
    }

    return (
        <>
            {props.type === "event" ? (
                <>
                    <h1>Event JSON-LD</h1>
                    {/* <EventJsonLd
                        name={props.title}
                        startDate={props.starDate}
                        endDate={props.endDate}
                        location={{
                            name: "My Place",
                            sameAs: "https://example.com/my-place",
                            address: {
                                streetAddress: "1600 Saratoga Ave",
                                addressLocality: "San Jose",
                                addressRegion: "CA",
                                postalCode: "95129",
                                addressCountry: "US",
                            },
                        }}
                        url="https://example.com/my-event"
                        images={["https://example.com/photos/photo.jpg"]}
                        description="My event @ my place"
                        offers={[
                            {
                                price: "119.99",
                                priceCurrency: "USD",
                                priceValidUntil: "2020-11-05",
                                itemCondition:
                                    "https://schema.org/UsedCondition",
                                availability: "https://schema.org/InStock",
                                url: "https://www.example.com/executive-anvil",
                                seller: {
                                    name: "John Doe",
                                },
                                validFrom: "2020-11-01T00:00:00.000Z",
                            },
                            {
                                price: "139.99",
                                priceCurrency: "CAD",
                                priceValidUntil: "2020-09-05",
                                itemCondition:
                                    "https://schema.org/UsedCondition",
                                availability: "https://schema.org/InStock",
                                url: "https://www.example.ca/executive-anvil",
                                seller: {
                                    name: "John Doe Sr.",
                                },
                                validFrom: "2020-08-05T00:00:00.000Z",
                            },
                        ]}
                        performers={[
                            {
                                name: "Adele",
                            },
                            {
                                name: "Kira and Morrison",
                            },
                        ]}
                        organizer={{
                            type: "Organization",
                            name: "Unnamed organization",
                            url: "https://www.unnamed.com",
                        }}
                        eventStatus="EventScheduled"
                        eventAttendanceMode="OfflineEventAttendanceMode"
                    /> */}
                </>
            ) : (
                <NextSeo
                    title={props.title}
                    description={props.description}
                    canonical={currentUrl}
                    openGraph={{
                        url: currentUrl,
                        title: props.title,
                        description: props.description,
                        images: [
                            {
                                url: imageUrl,
                                width: 800,
                                height: 600,
                                alt: props.title,
                                type: "image/webp",
                            },
                            {
                                url: imageUrl,
                                width: 900,
                                height: 800,
                                alt: props.title,
                                type: "image/webp",
                            },
                            // { url: "https://www.example.com/og-image03.jpg" },
                            // { url: "https://www.example.com/og-image04.jpg" },
                        ],
                        site_name: `TribeHood || ${props.title}`,
                    }}
                    twitter={{
                        handle: "@handle",
                        site: "@site",
                        cardType: "summary_large_image",
                    }}
                    additionalLinkTags={[
                        {
                            rel: "icon",
                            href: "/favicon.ico",
                        },
                        {
                            rel: "apple-touch-icon",
                            href: "/favicon.ico",
                            sizes: "76x76",
                        },
                        // {
                        //     rel: "manifest",
                        //     href: "/manifest.json",
                        // },
                        // {
                        //     rel: "preload",
                        //     href: "https://fonts.googleapis.com/css?family=Jost:300,400,500,600,700&display=swap",
                        //     as: "font",
                        //     type: "font/woff2",
                        //     crossOrigin: "google",
                        // },
                        // {
                        //     rel: "stylesheet",
                        //     href: "https://fonts.googleapis.com/css?family=Jost:300,400,500,600,700&display=swap",
                        // },
                    ]}
                />
            )}
        </>
    );
}
{
    /* <Head>
    <meta charSet="UTF-8" />
    <title>{props.title}</title>
    <meta name="description" content={props.description} />
    <meta name="keywords" content={props.keywords} />
            <meta name="robots" content="INDEX,FOLLOW" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta property="og:locale" content="en_UK" />
    <meta property="og:type" content={props.type} key="type" />
    <meta property="og:title" content={props.title} key="title" />
    <meta property="og:image" content={imageUrl} key="image" />
    <meta
        property="og:description"
        content={props.description}
        key="description"
    />
    <meta property="og:url" content={currentUrl} key="url" />

    <link rel="icon" type="image/ico+svg+xml" href="/favicon.ico" />
    <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Jost:300,400,500,600,700&display=swap"
    />
</Head>; */
}
