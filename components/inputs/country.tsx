import { Autocomplete, Box, TextField } from "@mui/material";
import { getPublicData } from "../../services/data.service";
import { useEffect, useState } from "react";
import authService from "../../services/auth.service";
import { useRouter } from "next/router";
import Loading from "../common/loading";

export default function CountrySelection(props) {
    console.log("CountrySelection");
    console.log(props);
    const router = useRouter();

    const [isLoading, setIsLoading] = useState(true);
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const checkToken = async () => {
        const isToken = await authService.checkToken();
        setIsAuth(isToken);
        if (!isToken) {
            router.push("/users/login");
        }
    };

    const [countries, setCountries] = useState([]);
    const getAll = async () => {
        const countriesRawData = await getPublicData("location/countries");
        // const rawData = await getData("events");
        setCountries(countriesRawData);
        setIsLoading(false);
    };

    useEffect(() => {
        checkToken();
        getAll();
    }, [props]);

    // const [country, setCountry] = useState<string | null>(countries[0]);
    // const [country, setCountry] = useState<string | null>(null);
    const [country, setCountry] = useState(props.selectedCountry[0] || null);
    const [inputValue, setInputValue] = useState("");
    const handleCountrySelection = (data: any) => {
        console.log("handleCountrySelection");
        console.log(data);
        props.handleSelectedCountry(data);
        // setCountry(data);
        // setInputValue(data);
    };

    console.log("isLoading");
    console.log(isLoading);
    console.log("isAuth");
    console.log(isAuth);
    console.log("countries");
    console.log(countries);

    return (
        <>
            {!isLoading ? (
                <Autocomplete
                    id="country-selection"
                    sx={{ width: "100%" }}
                    autoHighlight
                    getOptionLabel={(option) => option.name}
                    defaultValue={country}
                    value={country}
                    onChange={(event: any, newValue: string | null) => {
                        setCountry(newValue);
                        handleCountrySelection(newValue);
                    }}
                    inputValue={inputValue}
                    onInputChange={(event, newInputValue) => {
                        setInputValue(newInputValue);
                        // handleCountrySelection(newInputValue);
                    }}
                    options={countries}
                    // onChange={(data) => handleCountrySelection(data)}
                    // onChange={(_, data) => onChange(data); handleCountrySelection(data)}
                    renderOption={(props, option) => (
                        <Box
                            component="li"
                            sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                            {...props}
                        >
                            <img
                                loading="lazy"
                                width="20"
                                src={option.image}
                                alt=""
                            />
                            {option.name} ({option.code}) +{option.phone}
                        </Box>
                    )}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            label="Choose a country"
                            inputProps={{
                                ...params.inputProps,
                                autoComplete: "new-password", // disable autocomplete and autofill
                            }}
                        />
                    )}
                />
            ) : (
                <Loading />
            )}
        </>
    );
}
