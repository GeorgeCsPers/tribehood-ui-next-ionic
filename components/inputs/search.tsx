import { Box, TextField } from "@mui/material";

export default function SearchComponent(props: any) {
    return (
        <Box sx={{ display: "flex", alignItems: "start", width: "100%" }}>
            {props.toolbarSearch ? (
                <Box
                    sx={{ display: "flex", alignItems: "start", width: "100%" }}
                >
                    <TextField
                        sx={{ display: "flex" }}
                        fullWidth
                        // label="Search..."
                        placeholder="Search..."
                        type="text"
                        size="small"
                        className="narrow-input light-input"
                        onChange={(e) => props.handleSearch(e.target.value)}
                    />
                </Box>
            ) : (
                <Box
                    sx={{ display: "flex", alignItems: "start", width: "100%" }}
                    className="general-input"
                >
                    <TextField
                        sx={{ display: "flex" }}
                        fullWidth
                        label="Search..."
                        type="text"
                        onChange={(e) => props.handleSearch(e.target.value)}
                    />
                </Box>
            )}
        </Box>
    );
}
