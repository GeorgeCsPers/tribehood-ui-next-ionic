import { createTheme } from "@mui/material/styles";

const mainFont = "'Jost', sans-serif"; //"Jost", "Helvetica", "Arial", sans-serif

export const theme = createTheme({
    // status: {
    //     danger: "#e53e3e",
    // },

    typography: {
        // fontFamily: `"Nunito", "Helvetica", "Arial", sans-serif`,
        // fontFamily: `"Nunito Sans", "Helvetica", "Arial", sans-serif`,
        // fontFamily: `"Didact Gothic", "Helvetica", "Arial", sans-serif`,
        fontFamily: mainFont,
        fontSize: 14,
        fontWeightLight: 400,
        fontWeightRegular: 500,
        fontWeightMedium: 600,
    },
    // breakpoints: {
    //     values: {
    //         xs: 0,
    //         sm: 575,
    //         md: 767,
    //         lg: 991,
    //         xl: 1200,
    //     },
    // },
    palette: {
        primary: {
            light: "#e1ff4b", // will be calculated from palette.primary.main,
            main: "#cff800", // #24b478
            // dark: will be calculated from palette.primary.main,
            contrastText: "#206a5d", // will be calculated to contrast with palette.primary.main
        },
        secondary: {
            light: "#2b8a7a",
            main: "#206a5d", // #e8a523
            // dark: will be calculated from palette.secondary.main,
            contrastText: "#cff800",
        },
        // Provide every color token (light, main, dark, and contrastText) when using
        // custom colors for props in Material UI's components.
        // Then you will be able to use it like this: `<Button color="custom">`
        // (For TypeScript, you need to add module augmentation for the `custom` value)
        // custom: {
        //     light: "#ffa726",
        //     main: "#f57c00",
        //     dark: "#ef6c00",
        //     contrastText: "rgba(0, 0, 0, 0.87)",
        // },
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        // contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        // tonalOffset: 0.2,
    },
    components: {
        MuiOutlinedInput: {
            styleOverrides: {
                root: {
                    borderRadius: 16,
                    // backgroundColor: "#fff",
                    // border: '1px solid pink',
                    // fontSize: 16,
                    // padding: '10px 12px',
                    // width: 'calc(100% - 24px)',
                },
            },
        },
        MuiContainer: {
            styleOverrides: {
                maxWidthXs: {
                    maxWidth: 1380,
                },
                maxWidthSm: {
                    maxWidth: 1380,
                },
                maxWidthMd: {
                    maxWidth: 1380,
                },
                maxWidthLg: {
                    maxWidth: 1380,
                },
                maxWidthXl: {
                    maxWidth: 1380,
                },
            },
        },
        MuiButton: {
            styleOverrides: {
                containedPrimary: {
                    "&:hover": {
                        backgroundColor: "#e1ff4b",
                    },
                },
                containedSecondary: {
                    "&:hover": {
                        backgroundColor: "#2b8a7a",
                    },
                },
                // root: {
                //         // contained: {
                //         "&:hover": {
                //             backgroundColor: "#e1ff4b",
                //         },
                //         // contained: {
                //         "&:hover": {
                //             backgroundColor: "#2b8a7a",
                //         },
                //     // },
                // },
            },
        },
    },
});

theme.typography.h1 = {
    fontFamily: mainFont,
    // fontFamily: "Didact Gothic",
    // fontFamily: "Nunito",
    // fontFamily: "Nunito Sans",
    fontSize: "3rem",
    fontWeight: 400,
    // "@media (min-width:600px)": {
    //     fontSize: "1.5rem",
    // },
    // [theme.breakpoints.up("md")]: {
    //     fontSize: "2.4rem",
    // },
};
theme.typography.h2 = {
    fontFamily: mainFont,
    // fontFamily: "Didact Gothic",
    // fontFamily: "Nunito",
    // fontFamily: "Nunito Sans",
    fontSize: "1.8rem",
    fontWeight: 400,
};
theme.typography.h3 = {
    fontFamily: mainFont,
    // fontFamily: "Didact Gothic",
    // fontFamily: "Nunito",
    // fontFamily: "Nunito Sans",
    fontSize: "1.6rem",
    fontWeight: 600,
};
theme.typography.h4 = {
    fontFamily: mainFont,
    // fontFamily: "Didact Gothic",
    // fontFamily: "Nunito",
    // fontFamily: "Nunito Sans",
    fontSize: "1.4rem",
    fontWeight: 600,
};
theme.typography.h5 = {
    fontFamily: mainFont,
    // fontFamily: "Didact Gothic",
    // fontFamily: "Nunito",
    // fontFamily: "Nunito Sans",
    fontSize: "1.2rem",
    fontWeight: 600,
};
theme.typography.h6 = {
    fontFamily: mainFont,
    // fontFamily: "Didact Gothic",
    // fontFamily: "Nunito",
    // fontFamily: "Nunito Sans",
    fontSize: "1rem",
    fontWeight: 600,
};
theme.typography.body1 = {
    fontFamily: mainFont,
    // fontFamily: "Didact Gothic",
    // fontFamily: "Nunito",
    // fontFamily: "Nunito Sans",
    fontSize: "1rem",
    letterSpacing: "0.02rem",
    fontWeight: 400,
};
theme.typography.body2 = {
    fontFamily: mainFont,
    // fontFamily: "Didact Gothic",
    // fontFamily: "Nunito",
    // fontFamily: "Nunito Sans",
    fontSize: "0.95rem",
    letterSpacing: "0.02rem",
    fontWeight: 400,
};
