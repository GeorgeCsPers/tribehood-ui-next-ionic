const withTM = require("next-transpile-modules")([
    "@ionic/react",
    "@ionic/core",
    "@stencil/core",
    "ionicons",
]);

// require("dotenv").config();
// const isProd = process.env.NODE_ENV === "production";

const nextConfig = {
    reactStrictMode: false,
    // output: "export",
    // distDir: "build",
    swcMinify: true,
    trailingSlash: true,
    // env: {
    //     SENDINBLUE_API_KEY: process.env.SENDINBLUE_API_KEY,
    //     NEXT_PUBLIC_APP_NAME: process.env.NEXT_PUBLIC_APP_NAME,
    //     SECRET: process.env.SECRET,
    //     NEXT_PUBLIC_URL: process.env.NEXT_PUBLIC_URL,
    //     NEXT_PUBLIC_API_URL: process.env.NEXT_PUBLIC_API_URL,
    // },
    // experimental: {
    scrollRestoration: true,
    images: {
        // unoptimized: true,
        // remotePatterns: [
        //     {
        //         protocol: "https" || "http",
        //         hostname: "**" || "192.168.0.175" || "assets.goldilab.com",
        //         // port: "",
        //         // pathname: "/tribehood/**",
        //     },
        // ],
        domains: [
            "192.168.0.175",
            "assets.goldilab.com",
            "192.168.0.175:10001",
        ],
        // formats: ["image/jpeg", "image/jpg", "image/png", "image/webp"],
    },
    // },
    compiler: {
        removeConsole: process.env.NODE_ENV === "production",
    },

    // async headers() {
    //     return [
    //         {
    //             source: "/sitemapv2.xml",
    //             headers: [
    //                 {
    //                     key: "Content-Type",
    //                     value: "text/xml",
    //                 },
    //             ],
    //         },
    //     ];
    // },

    // rewrites: async () => [
    //     {
    //         source: "/server-sitemap.xml",
    //         destination: "/server-sitemap.xml",
    //     },
    //     {
    //         source: "/dynamic-sitemap-:area.xml",
    //         destination: "/dynamic-sitemap/:area",
    //     },
    // ],
    // async rewrites() {
    // return {
    // beforeFiles: [
    //     // These rewrites are checked after headers/redirects
    //     // and before all files including _next/public files which
    //     // allows overriding page files
    //     {
    //         source: "/",
    //         destination: "/:area",
    //         has: [{ type: "query", key: "area" }],
    //     },
    //     // {
    //     //     source: "/events",
    //     //     destination: "/:area/events",
    //     // },
    // ],
    // };
    // return [
    //     {
    //         source: "/",
    //         destination: "/all",
    //         has: [{ type: "query", key: "area" }],
    //     },
    // ];
    // },
    //     // return [
    //     //     {
    //     //         source: "/",
    //     //         destination: "/:area",
    //     //     },
    //     //     {
    //     //         source: "/events",
    //     //         destination: "/:area/events",
    //     //     },
    //     //     {
    //     //         source: "/projects",
    //     //         destination: "/:area/projects",
    //     //     },
    //     //     {
    //     //         source: "/places",
    //     //         destination: "/:area/places",
    //     //     },
    //     // ];
    // },
};

module.exports = withTM(nextConfig);

// const path = require("path");
// const CopyPlugin = require("copy-webpack-plugin");
// // import path from "path";
// // import CopyPlugin from "copy-webpack-plugin";

// /** @type {import('next').NextConfig} */
// module.exports = {
//     webpack: (config) => {
//         config.plugins.push(
//             new CopyPlugin({
//                 patterns: [
//                     {
//                         from: path.join(
//                             __dirname,
//                             "node_modules/ionicons/dist/ionicons/svg"
//                         ),
//                         to: path.join(__dirname, "public/svg"),
//                     },
//                 ],
//             })
//         );
//         return config;
//     },
// };
